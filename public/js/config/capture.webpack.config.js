var path = require("path");
const { VueLoaderPlugin } = require("vue-loader");

module.exports = {
    mode: "development",
    devtool: "source-map",
    entry: "./src/capture.js",
    output: {
        path: path.resolve(__dirname, "../dist"),
        filename: "capture.bundle.js"
    },
    resolve: {
        alias: {
            vue: "vue/dist/vue.js"
        },
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: "vue-loader",
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
};
