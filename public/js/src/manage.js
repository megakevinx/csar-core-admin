import $ from "jquery";

import Vue from "vue";
import ManageVehicleCores from "./viewmodels/ManageVehicleCores.vue";

import AlertsManager from "./helpers/AlertsManager";
import ApiClient from "./helpers/ApiClient";
import VehicleCoreValidatorFactory from "./validators/VehicleCoreValidatorFactory";

$(document).ready(function() {

    let alertsManager = new AlertsManager();
    let apiClient = new ApiClient();
    let validatorFactory = new VehicleCoreValidatorFactory();

    new Vue({
        el: "#mount_point",

        components: {
            ManageVehicleCores
        },

        provide: {
            alertsManager: alertsManager,
            apiClient: apiClient,
            validatorFactory: validatorFactory,

            onInit: () => {
                $("#overlay").fadeOut();
            }
        }
    });
});
