import $ from "jquery";

import Vue from "vue";
import CaptureDealVehicleCores from "./viewmodels/CaptureDealVehicleCores.vue";

import AlertsManager from "./helpers/AlertsManager";
import DealVehicleCoresDataValidator from "./validators/DealVehicleCoresDataValidator";
import ApiClient from "./helpers/ApiClient";
import UrlHelper from "./helpers/UrlHelper";

$(document).ready(function() {

    let alertsManager = new AlertsManager();
    let apiClient = new ApiClient();
    let urlHelper = new UrlHelper();
    let validator = new DealVehicleCoresDataValidator("form#cores_form", "div#validation_error_container");

    new Vue({
        el: "#mount_point",

        components: {
            CaptureDealVehicleCores
        },

        provide: {
            alertsManager: alertsManager,
            apiClient: apiClient,
            validator: validator,
            urlHelper: urlHelper,

            onInit: () => {
                $("#overlay").fadeOut();
            }
        }
    });
});
