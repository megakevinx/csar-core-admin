import VehicleCoreDataValidator from "./VehicleCoreDataValidator";

export default class VehicleCoreValidatorFactory {
    constructor() {

    }

    makeValidator(coreCode, coreDescription, formSelector, errorMessagesContainerSelector) {
        let validator = new VehicleCoreDataValidator(formSelector, errorMessagesContainerSelector);
        validator.addValidationFor(coreCode, coreDescription);

        return validator;
    }
}
