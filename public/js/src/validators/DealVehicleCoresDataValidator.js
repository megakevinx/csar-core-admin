/**
 * Validates customer data
 */
import $ from "jquery";
import "jquery-validation";
import "jquery-validation/dist/additional-methods.js";

// function coreIsExtracted(coreCode) {
//     return $(`[name='${coreCode}_is_extracted']:checked`).val() == "yes";
// }

export default class DealVehicleCoresDataValidator {

    constructor(formSelector, errorMessagesContainerSelector) {
        this.formSelector = formSelector;
        this.errorMessagesContainerSelector = errorMessagesContainerSelector;

        this.validationOptions = {
            rules: {
                // [this.coreCode + "_is_extracted"]: {
                //     required: true
                // },
                // [this.coreCode + "_quantity"]: {
                //     // required: {
                //     //     depends: function () {
                //     //         return coreIsExtracted(coreCode);
                //     //     }
                //     // },
                //     required: true,
                //     min: 0,
                //     digits: true
                // },
                // [this.coreCode + "_price_per_unit"]: {
                //     // required: {
                //     //     depends: function () {
                //     //         return coreIsExtracted(coreCode);
                //     //     }
                //     // },
                //     required: true,
                //     min: 0,
                //     currency: ["$", false]
                // },
                // [this.coreCode + "_total_price"]: {
                //     // required: {
                //     //     depends: function () {
                //     //         return coreIsExtracted(coreCode);
                //     //     }
                //     // },
                //     required: true,
                //     min: 0
                // }
            },

            messages: {
                // [this.coreCode + "_is_extracted"]: {
                //     required: `Please specify whether the ${this.coreDescription} was extracted`
                // },
                // [this.coreCode + "_quantity"]: {
                //     required: `Please specify the ${this.coreDescription} quantity`,
                //     min: `The ${this.coreDescription} quantity must be zero or higher`,
                //     digits: `The ${this.coreDescription} quantity is invalid`
                // },
                // [this.coreCode + "_price_per_unit"]: {
                //     required: `Please specify the value of each ${this.coreDescription}`,
                //     min: `The ${this.coreDescription} price per unit must be zero or higher`,
                //     currency: `Please specify a valid ${this.coreDescription} price per unit.`
                // },
                // [this.coreCode + "_total_price"]: {
                //     required: `Please specify the total price for all ${this.coreDescription}`,
                //     min: `The ${this.coreDescription} total price must be zero or higher`,
                // }
            },

            errorElement : "div",
            errorLabelContainer: this.errorMessagesContainerSelector
        };
    }

    // isExtracted(coreCode) {
    //     return $(`[name='${coreCode}_is_extracted']:checked`).val() == "yes";
    // }

    addValidationFor(coreCode, coreDescription) {

        let isExtracted = () => {
            return $(`[name='${coreCode}_is_extracted']:checked`).val() == "yes";
        };

        this.validationOptions.rules[coreCode + "_is_extracted"] = {
            required: true
        };

        this.validationOptions.rules[coreCode + "_quantity"] = {
            required: {
                depends: isExtracted
            },
            // required: true,
            min: 0,
            digits: true
        };

        this.validationOptions.rules[coreCode + "_price_per_unit"] = {
            required: {
                depends: isExtracted
            },
            //required: true,
            min: 0,
            currency: ["$", false]
        };

        this.validationOptions.rules[coreCode + "_total_price"] = {
            required: {
                depends: isExtracted
            },
            //required: true,
            min: 0
        };

        this.validationOptions.messages[coreCode + "_is_extracted"] = {
            required: `Please specify whether the ${coreDescription} was extracted`
        };

        this.validationOptions.messages[coreCode + "_quantity"] = {
            required: `Please specify the ${coreDescription} quantity`,
            min: `The ${coreDescription} quantity must be zero or higher`,
            digits: `The ${coreDescription} quantity is invalid`
        };

        this.validationOptions.messages[coreCode + "_price_per_unit"] = {
            required: `Please specify the value of each ${coreDescription}`,
            min: `The ${coreDescription} price per unit must be zero or higher`,
            currency: `Please specify a valid ${coreDescription} price per unit.`
        };

        this.validationOptions.messages[coreCode + "_total_price"] = {
            required: `Please specify the total price for all ${coreDescription}`,
            min: `The ${coreDescription} total price must be zero or higher`,
        };
    }

    isValid() {
        var form = $(this.formSelector);
        form.validate(this.validationOptions);
        var isValid = form.valid();

        return isValid;
    }
}
