/**
 * Validates customer data
 */
import $ from "jquery";
import "jquery-validation";
import "jquery-validation/dist/additional-methods.js";

export default class VehicleCoresDataValidator {

    constructor(formSelector, errorMessagesContainerSelector) {
        this.formSelector = formSelector;
        this.errorMessagesContainerSelector = errorMessagesContainerSelector;

        this.validationOptions = {
            rules: { },
            messages: { },

            errorElement : "div",
            errorLabelContainer: this.errorMessagesContainerSelector
        };
    }

    addValidationFor(coreCode, coreDescription) {

        this.validationOptions.rules[coreCode + "_quantity"] = {
            required: true,
            min: 0,
            digits: true
        };

        this.validationOptions.rules[coreCode + "_price_per_unit"] = {
            required: true,
            min: 0,
            currency: ["$", false]
        };

        this.validationOptions.messages[coreCode + "_quantity"] = {
            required: `Please specify the default quantity for ${coreDescription}`,
            min: `The default quantity for ${coreDescription} must be zero or higher`,
            digits: `The default quantity for ${coreDescription} is invalid`
        };

        this.validationOptions.messages[coreCode + "_price_per_unit"] = {
            required: `Please specify the default price per unit for ${coreDescription}`,
            min: `The $default price per unit for ${coreDescription} must be zero or higher`,
            currency: `The default price per unit for ${coreDescription} is invalid.`
        };
    }

    isValid() {
        var form = $(this.formSelector);
        form.validate(this.validationOptions);
        var isValid = form.valid();

        return isValid;
    }

    destroy() {
        var validator = $(this.formSelector).validate();
        validator.destroy();
    }
}
