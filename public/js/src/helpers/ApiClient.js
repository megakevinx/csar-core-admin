export default class ApiClient {
    constructor() {

    }

    handleResponse(response) {
        const INTERNAL_SERVER_ERROR = 500;

        if (response.status >= INTERNAL_SERVER_ERROR) {
            throw new Error(response.type + " " + response.statusText + " " + response.status.toString() + " " + response.url);
        }
        else {
            return response.json();
        }
    }

    getConfigurationData() {
        return fetch("api/configuration/enter-cores", {
            method: "get",
            credentials: "include"
        }).then(this.handleResponse);
    }

    getDealVehicleCores(dealId) {
        return fetch(`api/deal-vehicle-core/${ dealId }`, {
            method: "get",
            credentials: "include"
        }).then(this.handleResponse);
    }

    submitDealVehicleCores(dealVehicleCoresData) {
        return fetch("api/deal-vehicle-core", {
            method: "post",
            credentials: "include",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(dealVehicleCoresData)
        }).then(this.handleResponse);
    }

    updateVehicleCore(vehicleCoreData) {
        return fetch(`api/vehicle-core/${ vehicleCoreData.id }`, {
            method: "put",
            credentials: "include",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify(vehicleCoreData)
        }).then(this.handleResponse);
    }
}
