/**
 * Helps with alerts for user feedback.
 */
//import $ from "jquery";

export default class AlertsManager {

    constructor() {
        this.alertTypes = {
            success: "success",
            info: "info",
            warning: "warning",
            danger: "danger"
        };

        this.alertPattern = 
            '<div id="{ALERT_ID}" class="alert alert-{ALERT_TYPE} alert-dismissable centered-container">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + 
                "{ALERT_MESSAGE}" +
            '</div>';
    }

    show(alertContainer, alertType, alertMessage, alertId) {
        alertId = alertId || "";

        alertContainer.append(
            this.alertPattern
                .replace("{ALERT_TYPE}", alertType)
                .replace("{ALERT_MESSAGE}", alertMessage)
                .replace("{ALERT_ID}", alertId)
        );
    }

    showSuccess(alertContainer, alertMessage) {
        this.show(alertContainer, this.alertTypes.success, alertMessage);
    }

    showInfo(alertContainer, alertMessage) {
        this.show(alertContainer, this.alertTypes.info, alertMessage);
    }

    showWarning(alertContainer, alertMessage, alertId) {

        if (alertId) {
            this.dismiss(alertId);
        }

        this.show(alertContainer, this.alertTypes.warning, alertMessage, alertId);
    }

    showDanger(alertContainer, alertMessage, alertId) {

        if (alertId) {
            this.dismiss(alertId);
        }

        this.show(alertContainer, this.alertTypes.danger, alertMessage, alertId);
    }

    dismiss(alertId) {
        $('#' + alertId).alert("close");
    }
}
