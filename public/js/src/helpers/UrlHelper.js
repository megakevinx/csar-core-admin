export default class UrlHelper {
    getQueryStringParam(paramKey) {
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.has(paramKey)) {
            return searchParams.get(paramKey);
        }
        else return null;
    }
}
