require("jsdom-global")();

// var mocha = require("mocha");
// var chai = require("chai");

// global.describe = mocha.describe;
// global.it = mocha.it;

// global.expect = chai.expect;
// global.assert = chai.assert;

require("./specs/VehicleCore.spec");
require("./specs/EditVehicleCore.spec");
require("./specs/ManageVehicleCores.spec");
require("./specs/CaptureDealVehicleCores.spec");
