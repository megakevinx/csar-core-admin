var path = require("path");
const { VueLoaderPlugin } = require("vue-loader");
const nodeExternals = require('webpack-node-externals');

module.exports = {
    mode: "development",
    stats: "minimal",
    devtool: "inline-cheap-module-source-map",
    entry: "./test/index.js",
    output: {
        path: path.resolve(__dirname, "."),
        filename: "test.bundle.js",
        devtoolModuleFilenameTemplate: "[absolute-resource-path]",
        devtoolFallbackModuleFilenameTemplate: "[absolute-resource-path]?[hash]"
    },
    resolve: {
        alias: {
            vue: "vue/dist/vue.js"
        },
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: "vue-loader",
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    node: {
        fs: "empty",
        child_process: "empty",
        tls: "empty",
        net: "empty",
    },
    externals: [nodeExternals()]
};
