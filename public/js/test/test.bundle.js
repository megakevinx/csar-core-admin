/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./test/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = __webpack_require__(/*! jquery */ "jquery");

var _jquery2 = _interopRequireDefault(_jquery);

var _VehicleCore = __webpack_require__(/*! ./VehicleCore.vue */ "./src/viewmodels/VehicleCore.vue");

var _VehicleCore2 = _interopRequireDefault(_VehicleCore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {

    components: {
        VehicleCore: _VehicleCore2.default
    },

    inject: ["alertsManager", "apiClient", "validator", "urlHelper", "onInit"],

    data: function data() {
        return {
            isInitError: false,
            initErrorMessage: "",
            isLoadingRequest: false,
            dealId: null,
            cores: []
        };
    },

    created: function created() {
        var _this = this;

        this.dealId = this.urlHelper.getQueryStringParam("dealId");

        if (!this.dealId) {
            this.isInitError = true;
            this.initErrorMessage = "No deal number specified.";

            this.onInit();
            return;
        }

        this.apiClient.getDealVehicleCores(this.dealId).then(function (response) {
            if (response.status == 200) {
                var dealVehicleCores = response.dealVehicleCores;

                _this.apiClient.getConfigurationData().then(function (response) {

                    if (response.status == 200) {
                        var defaultCores = response.cores;

                        _this.cores = defaultCores.map(function (defaultCore) {
                            var actualCore = dealVehicleCores.find(function (c) {
                                return c.vehicleCoreId == defaultCore.id;
                            });

                            return {
                                vehicleCoreId: defaultCore.id,
                                code: defaultCore.code.toLowerCase(),
                                description: defaultCore.description,
                                isExtracted: actualCore ? true : false,
                                quantity: actualCore ? actualCore.quantity : defaultCore.defaultQuantity,
                                pricePerUnit: actualCore ? actualCore.pricePerUnit : defaultCore.defaultPricePerUnit,
                                totalPrice: actualCore ? actualCore.totalPrice : 0.00
                            };
                        });

                        defaultCores.forEach(function (c) {
                            _this.validator.addValidationFor(c.code.toLowerCase(), c.description);
                        });
                    } else {
                        _this.isInitError = true;
                        _this.initErrorMessage = "Could not retrieve configuration data. Please try again later.";
                    }

                    _this.onInit();
                }).catch(function (error) {
                    console.log(error);

                    _this.isInitError = true;
                    _this.initErrorMessage = "There has been a network or server error. Please try again later. " + error;

                    _this.onInit();
                });
            } else if (response.status == 400) {

                _this.isInitError = true;
                _this.initErrorMessage = "The specified deal does not exist.";

                _this.onInit();
            } else if (response.status == 401) {

                _this.isInitError = true;
                _this.initErrorMessage = "Your session has expired. Please close this window and try to open it again from the Spreadsheet.";

                _this.onInit();
            }
        }).catch(function (error) {
            console.log(error);

            _this.isInitError = true;
            _this.initErrorMessage = "There has been a network or server error. Please try again later. " + error;

            _this.onInit();
        });
    },

    mounted: function mounted() {
        (0, _jquery2.default)("div.validation-error-alert a.close").click(function (eventObject) {
            (0, _jquery2.default)(eventObject.target.parentElement).hide();
        });
    },

    computed: {
        totalValue: function totalValue() {
            var coresTotalValue = 0.00;
            var extractedCores = this.cores.filter(function (c) {
                return c.isExtracted;
            });

            if (extractedCores.length > 0) {
                coresTotalValue = extractedCores.map(function (c) {
                    return c.totalPrice;
                }).reduce(function (previous, current) {
                    return previous + current;
                });
            }

            return Number(coresTotalValue).toFixed(2);
        }
    },

    methods: {
        submit: function submit() /*event*/{
            var _this2 = this;

            if (!this.validator.isValid()) {
                return;
            }

            var vehicleCoresData = {
                dealId: this.dealId,
                cores: this.cores
            };

            this.isLoadingRequest = true;

            this.apiClient.submitDealVehicleCores(vehicleCoresData).then(function (response) {
                console.log(response);

                if (response.status == 200) {
                    _this2.alertsManager.showSuccess((0, _jquery2.default)("div#alerts_container"), "The cores information has been saved successfully. Please refresh your Spreadsheet to see the changes.");
                } else if (response.status == 400) {
                    _this2.alertsManager.showDanger((0, _jquery2.default)("div#alerts_container"), "There has been a validation error. Make sure the input data is valid.", "VALIDATION_ERROR");
                } else if (response.status == 401) {
                    _this2.alertsManager.showDanger((0, _jquery2.default)("div#alerts_container"), "Your session has expired. Please close this window and try to open it again from the Spreadsheet.", "SESSION_EXPIRED");
                }

                _this2.isLoadingRequest = false;
            }).catch(function (error) {
                console.log(error);
                _this2.alertsManager.showDanger((0, _jquery2.default)("div#alerts_container"), "There has been a network or server error. Please try again later. " + error, "NETWORK_ERROR");
                _this2.isLoadingRequest = false;
            });
        }
    }
};

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/EditVehicleCore.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/viewmodels/EditVehicleCore.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = __webpack_require__(/*! jquery */ "jquery");

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {

    inject: ["alertsManager", "apiClient", "validatorFactory"],

    props: {
        coreToEdit: Object
    },

    data: function data() {
        return {
            isLoadingRequest: false,
            isValid: true,
            isServerError: false,

            core: {
                id: this.coreToEdit.id,
                code: this.coreToEdit.code.toLowerCase(),
                description: this.coreToEdit.description,
                defaultQuantity: this.coreToEdit.defaultQuantity,
                defaultPricePerUnit: this.coreToEdit.defaultPricePerUnit
            }
        };
    },

    methods: {
        save: function save() /*event*/{
            var _this = this;

            var validator = this.validatorFactory.makeValidator(this.core.code, this.core.description, "form#" + this.core.code + "_form", "div#" + this.core.code + "_validation_error_container");

            this.isValid = validator.isValid();

            if (!this.isValid) {
                return;
            }

            var coreData = this.core;

            this.isLoadingRequest = true;

            this.apiClient.updateVehicleCore(coreData).then(function (response) {
                console.log(response);

                if (response.status == 200) {
                    _this.alertsManager.showSuccess((0, _jquery2.default)("div#" + _this.core.code + "_alerts_container"), "The core's information has been saved successfully.");
                } else if (response.status == 400) {
                    _this.alertsManager.showDanger((0, _jquery2.default)("div#" + _this.core.code + "_alerts_container"), "There has been a validation error. Make sure the input data is valid.", "VALIDATION_ERROR");
                } else if (response.status == 401) {
                    _this.alertsManager.showDanger((0, _jquery2.default)("div#" + _this.core.code + "_alerts_container"), "Your session has expired. Please close this window and try to open it again from the Account Manager page.", "SESSION_EXPIRED");
                }

                _this.isLoadingRequest = false;
            }).catch(function (error) {
                console.log(error);
                _this.alertsManager.showDanger((0, _jquery2.default)("div#" + _this.core.code + "_alerts_container"), "There has been a network or server error. Please try again later. " + error, "NETWORK_ERROR");
                _this.isLoadingRequest = false;
            });
        }
    }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/ManageVehicleCores.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/viewmodels/ManageVehicleCores.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = __webpack_require__(/*! jquery */ "jquery");

var _jquery2 = _interopRequireDefault(_jquery);

var _EditVehicleCore = __webpack_require__(/*! ./EditVehicleCore.vue */ "./src/viewmodels/EditVehicleCore.vue");

var _EditVehicleCore2 = _interopRequireDefault(_EditVehicleCore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {

    components: {
        EditVehicleCore: _EditVehicleCore2.default
    },

    inject: ["apiClient", "onInit"],

    data: function data() {
        return {
            isInitError: false,
            initErrorMessage: "",
            isLoadingRequest: false,
            cores: []
        };
    },

    created: function created() {
        var _this = this;

        this.apiClient.getConfigurationData().then(function (response) {

            if (response.status == 200) {
                _this.cores = response.cores;
            } else {
                _this.isInitError = true;
                _this.initErrorMessage = "Could not retrieve configuration data. Please try again later.";
            }

            _this.onInit();
        }).catch(function (error) {
            console.log(error);

            _this.isInitError = true;
            _this.initErrorMessage = "There has been a network or server error. Please try again later. " + error;

            _this.onInit();
        });
    },

    mounted: function mounted() {
        (0, _jquery2.default)("div.validation-error-alert a.close").click(function (eventObject) {
            (0, _jquery2.default)(eventObject.target.parentElement).hide();
        });
    }
};

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/VehicleCore.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib??vue-loader-options!./src/viewmodels/VehicleCore.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {

    props: {
        core: { type: Object }
    },

    data: function data() {
        return {
            isExtracted: this.core.isExtracted ? "yes" : "no"
        };
    },

    computed: {
        totalPrice: function totalPrice() {
            var newTotalPrice = 0.00;

            if (this.core.quantity && this.core.pricePerUnit) {
                newTotalPrice = this.core.quantity * this.core.pricePerUnit;
            }

            this.$emit("computed-total-price", newTotalPrice);
            return newTotalPrice;
        },

        totalPriceDisplay: function totalPriceDisplay() {
            if (this.totalPrice) {
                return Number(this.totalPrice).toFixed(2);
            }

            return "0.00";
        }
    },

    methods: {
        isExtractedYesOnClick: function isExtractedYesOnClick() {
            this.$emit("change-is-extracted", true);
        },
        isExtractedNoOnClick: function isExtractedNoOnClick() {
            this.$emit("change-is-extracted", false);
        },
        quantityOnInput: function quantityOnInput(event) {
            this.$emit("input-quantity", parseInt(event.target.value));
        },
        pricePerUnitOnInput: function pricePerUnitOnInput(event) {
            this.$emit("input-price-per-unit", parseFloat(event.target.value));
        }
    }
};

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=template&id=c29972da&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=template&id=c29972da& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.isInitError
        ? [
            _c("div", { attrs: { id: "init_alerts_container" } }, [
              _c(
                "div",
                {
                  staticClass:
                    "alert alert-danger alert-dismissable centered-container"
                },
                [
                  _vm._v(
                    "\n                " +
                      _vm._s(_vm.initErrorMessage) +
                      "\n            "
                  )
                ]
              )
            ])
          ]
        : [
            _c("h2", [
              _vm._v(
                "Enter the vehicle's extracted cores for deal " +
                  _vm._s(_vm.dealId)
              )
            ]),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c("div", { staticClass: "total-core-value-container" }, [
              _vm._v("\n            Total Value: "),
              _c("span", { staticClass: "label label-success" }, [
                _vm._v("$" + _vm._s(_vm.totalValue))
              ])
            ]),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c(
              "form",
              { attrs: { id: "cores_form" } },
              [
                _vm._l(_vm.cores, function(core) {
                  return [
                    _c("VehicleCore", {
                      key: core.code,
                      attrs: { core: core },
                      on: {
                        "change-is-extracted": function($event) {
                          core.isExtracted = $event
                        },
                        "input-price-per-unit": function($event) {
                          core.pricePerUnit = $event
                        },
                        "input-quantity": function($event) {
                          core.quantity = $event
                        },
                        "computed-total-price": function($event) {
                          core.totalPrice = $event
                        }
                      }
                    })
                  ]
                })
              ],
              2
            ),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c("div", { attrs: { id: "alerts_container" } }),
            _vm._v(" "),
            _vm._m(0),
            _vm._v(" "),
            _c("div", [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary btn-lg",
                  attrs: { disabled: _vm.isLoadingRequest },
                  on: { click: _vm.submit }
                },
                [_vm._v("Save")]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.isLoadingRequest,
                      expression: "isLoadingRequest"
                    }
                  ],
                  staticClass: "submit-button-loading-container"
                },
                [
                  _c("img", {
                    attrs: { src: "img/loading_small.gif", alt: "Loading..." }
                  })
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "total-core-value-container pull-right" },
                [
                  _vm._v("\n                Total Value: "),
                  _c("span", { staticClass: "label label-success" }, [
                    _vm._v("$" + _vm._s(_vm.totalValue))
                  ])
                ]
              )
            ])
          ]
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "validation-error-alert alert alert-danger alert-dismissable fade in",
        attrs: { id: "validation_error_container" }
      },
      [
        _c("a", { staticClass: "close", attrs: { "aria-label": "close" } }, [
          _vm._v("×")
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/EditVehicleCore.vue?vue&type=template&id=67a1c9ba&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/viewmodels/EditVehicleCore.vue?vue&type=template&id=67a1c9ba& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "row manage-cores-row" }, [
      _c("form", { attrs: { id: _vm.core.code + "_form" } }, [
        _c("div", { staticClass: "col-md-4" }, [
          _vm._v(_vm._s(_vm.core.description))
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-3" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.core.defaultQuantity,
                expression: "core.defaultQuantity"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "number",
              placeholder: "How many parts?",
              min: "0",
              name: _vm.core.code + "_quantity"
            },
            domProps: { value: _vm.core.defaultQuantity },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.core, "defaultQuantity", $event.target.value)
              }
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-3" }, [
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.core.defaultPricePerUnit,
                expression: "core.defaultPricePerUnit"
              }
            ],
            staticClass: "form-control",
            attrs: {
              type: "number",
              placeholder: "How much for each?",
              min: "0",
              name: _vm.core.code + "_price_per_unit"
            },
            domProps: { value: _vm.core.defaultPricePerUnit },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.core, "defaultPricePerUnit", $event.target.value)
              }
            }
          })
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-2" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-primary",
            attrs: { disabled: _vm.isLoadingRequest },
            on: { click: _vm.save }
          },
          [_vm._v("Save")]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            directives: [
              {
                name: "show",
                rawName: "v-show",
                value: _vm.isLoadingRequest,
                expression: "isLoadingRequest"
              }
            ],
            staticClass: "submit-button-loading-container"
          },
          [
            _c("img", {
              attrs: { src: "img/loading_small.gif", alt: "Loading..." }
            })
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c(
      "div",
      {
        directives: [
          {
            name: "show",
            rawName: "v-show",
            value: !_vm.isValid,
            expression: "!isValid"
          }
        ],
        staticClass: "row manage-core-row-error-container"
      },
      [
        _c("div", {
          staticClass: "alert alert-danger",
          attrs: { id: _vm.core.code + "_validation_error_container" }
        })
      ]
    ),
    _vm._v(" "),
    _c("div", {
      staticClass: "row manage-core-row-error-container",
      attrs: { id: _vm.core.code + "_alerts_container" }
    })
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/ManageVehicleCores.vue?vue&type=template&id=fc9c3778&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/viewmodels/ManageVehicleCores.vue?vue&type=template&id=fc9c3778& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.isInitError
        ? [
            _c("div", { attrs: { id: "init_alerts_container" } }, [
              _c(
                "div",
                {
                  staticClass:
                    "alert alert-danger alert-dismissable centered-container"
                },
                [
                  _vm._v(
                    "\n                " +
                      _vm._s(_vm.initErrorMessage) +
                      "\n            "
                  )
                ]
              )
            ])
          ]
        : [
            _c("h2", [_vm._v("Manage Vehicle Cores")]),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c(
              "div",
              [
                _vm._m(0),
                _vm._v(" "),
                _vm._l(_vm.cores, function(core) {
                  return [
                    _c("EditVehicleCore", {
                      key: core.code,
                      attrs: { coreToEdit: core }
                    })
                  ]
                })
              ],
              2
            ),
            _vm._v(" "),
            _c("hr")
          ]
    ],
    2
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row manage-cores-header" }, [
      _c("div", { staticClass: "col-md-4" }, [_vm._v("Core")]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-3" }, [_vm._v("Default Quantity")]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-3" }, [
        _vm._v("Default Price Per Unit")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-md-2" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/VehicleCore.vue?vue&type=template&id=7b164da4&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./src/viewmodels/VehicleCore.vue?vue&type=template&id=7b164da4& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("h4", [_vm._v(_vm._s(_vm.core.description))]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "row capture-deal-vehicle-core-container" },
      [
        _c("div", { staticClass: "col-sm-3" }, [
          _c("div", { staticClass: "form-group" }, [
            _c("label", { attrs: { for: _vm.core.code + "_is_extracted" } }, [
              _vm._v("Extracted?")
            ]),
            _vm._v(" "),
            _c("div", [
              _c("label", { staticClass: "checkbox-inline" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.isExtracted,
                      expression: "isExtracted"
                    }
                  ],
                  attrs: {
                    type: "radio",
                    value: "yes",
                    name: _vm.core.code + "_is_extracted"
                  },
                  domProps: { checked: _vm._q(_vm.isExtracted, "yes") },
                  on: {
                    click: _vm.isExtractedYesOnClick,
                    change: function($event) {
                      _vm.isExtracted = "yes"
                    }
                  }
                }),
                _vm._v(" Yes\n                    ")
              ]),
              _vm._v(" "),
              _c("label", { staticClass: "checkbox-inline" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.isExtracted,
                      expression: "isExtracted"
                    }
                  ],
                  attrs: {
                    type: "radio",
                    value: "no",
                    name: _vm.core.code + "_is_extracted"
                  },
                  domProps: { checked: _vm._q(_vm.isExtracted, "no") },
                  on: {
                    click: _vm.isExtractedNoOnClick,
                    change: function($event) {
                      _vm.isExtracted = "no"
                    }
                  }
                }),
                _vm._v(" No\n                    ")
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("transition", { attrs: { name: "slide-fade" } }, [
          _c(
            "div",
            {
              directives: [
                {
                  name: "show",
                  rawName: "v-show",
                  value: _vm.isExtracted == "yes",
                  expression: "isExtracted == 'yes'"
                }
              ]
            },
            [
              _c("div", { staticClass: "col-sm-3" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: _vm.core.code + "_quantity" } }, [
                    _vm._v("Quantity")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    staticClass: "form-control",
                    attrs: {
                      type: "number",
                      placeholder: "How many parts?",
                      min: "0",
                      name: _vm.core.code + "_quantity"
                    },
                    domProps: { value: _vm.core.quantity },
                    on: { input: _vm.quantityOnInput }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-sm-3" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c(
                    "label",
                    { attrs: { for: _vm.core.code + "_price_per_unit" } },
                    [_vm._v("Price per unit")]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "input-group" }, [
                    _c("span", { staticClass: "input-group-addon" }, [
                      _vm._v("$")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass: "form-control",
                      attrs: {
                        type: "number",
                        placeholder: "How much for each?",
                        min: "0",
                        name: _vm.core.code + "_price_per_unit"
                      },
                      domProps: { value: _vm.core.pricePerUnit },
                      on: { input: _vm.pricePerUnitOnInput }
                    })
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-sm-3" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c(
                    "label",
                    { attrs: { for: _vm.core.code + "_total_price" } },
                    [_vm._v("Total")]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "input-group" }, [
                    _c("span", { staticClass: "input-group-addon" }, [
                      _vm._v("$")
                    ]),
                    _vm._v(" "),
                    _c("input", {
                      staticClass: "form-control",
                      attrs: {
                        readonly: "",
                        type: "text",
                        placeholder: "Total price",
                        name: _vm.core.code + "_total_price"
                      },
                      domProps: { value: _vm.totalPriceDisplay }
                    })
                  ])
                ])
              ])
            ]
          )
        ])
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./src/viewmodels/CaptureDealVehicleCores.vue":
/*!****************************************************!*\
  !*** ./src/viewmodels/CaptureDealVehicleCores.vue ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CaptureDealVehicleCores_vue_vue_type_template_id_c29972da___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CaptureDealVehicleCores.vue?vue&type=template&id=c29972da& */ "./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=template&id=c29972da&");
/* harmony import */ var _CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CaptureDealVehicleCores.vue?vue&type=script&lang=js& */ "./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CaptureDealVehicleCores_vue_vue_type_template_id_c29972da___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CaptureDealVehicleCores_vue_vue_type_template_id_c29972da___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src\\viewmodels\\CaptureDealVehicleCores.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/babel-loader/lib!../../node_modules/vue-loader/lib??vue-loader-options!./CaptureDealVehicleCores.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_CaptureDealVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=template&id=c29972da&":
/*!***********************************************************************************!*\
  !*** ./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=template&id=c29972da& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CaptureDealVehicleCores_vue_vue_type_template_id_c29972da___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/vue-loader/lib??vue-loader-options!./CaptureDealVehicleCores.vue?vue&type=template&id=c29972da& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/CaptureDealVehicleCores.vue?vue&type=template&id=c29972da&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CaptureDealVehicleCores_vue_vue_type_template_id_c29972da___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CaptureDealVehicleCores_vue_vue_type_template_id_c29972da___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/viewmodels/EditVehicleCore.vue":
/*!********************************************!*\
  !*** ./src/viewmodels/EditVehicleCore.vue ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditVehicleCore_vue_vue_type_template_id_67a1c9ba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditVehicleCore.vue?vue&type=template&id=67a1c9ba& */ "./src/viewmodels/EditVehicleCore.vue?vue&type=template&id=67a1c9ba&");
/* harmony import */ var _EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditVehicleCore.vue?vue&type=script&lang=js& */ "./src/viewmodels/EditVehicleCore.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditVehicleCore_vue_vue_type_template_id_67a1c9ba___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditVehicleCore_vue_vue_type_template_id_67a1c9ba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src\\viewmodels\\EditVehicleCore.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/viewmodels/EditVehicleCore.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./src/viewmodels/EditVehicleCore.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/babel-loader/lib!../../node_modules/vue-loader/lib??vue-loader-options!./EditVehicleCore.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/EditVehicleCore.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_EditVehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/viewmodels/EditVehicleCore.vue?vue&type=template&id=67a1c9ba&":
/*!***************************************************************************!*\
  !*** ./src/viewmodels/EditVehicleCore.vue?vue&type=template&id=67a1c9ba& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditVehicleCore_vue_vue_type_template_id_67a1c9ba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/vue-loader/lib??vue-loader-options!./EditVehicleCore.vue?vue&type=template&id=67a1c9ba& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/EditVehicleCore.vue?vue&type=template&id=67a1c9ba&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditVehicleCore_vue_vue_type_template_id_67a1c9ba___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditVehicleCore_vue_vue_type_template_id_67a1c9ba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/viewmodels/ManageVehicleCores.vue":
/*!***********************************************!*\
  !*** ./src/viewmodels/ManageVehicleCores.vue ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ManageVehicleCores_vue_vue_type_template_id_fc9c3778___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ManageVehicleCores.vue?vue&type=template&id=fc9c3778& */ "./src/viewmodels/ManageVehicleCores.vue?vue&type=template&id=fc9c3778&");
/* harmony import */ var _ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ManageVehicleCores.vue?vue&type=script&lang=js& */ "./src/viewmodels/ManageVehicleCores.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ManageVehicleCores_vue_vue_type_template_id_fc9c3778___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ManageVehicleCores_vue_vue_type_template_id_fc9c3778___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src\\viewmodels\\ManageVehicleCores.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/viewmodels/ManageVehicleCores.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./src/viewmodels/ManageVehicleCores.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/babel-loader/lib!../../node_modules/vue-loader/lib??vue-loader-options!./ManageVehicleCores.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/ManageVehicleCores.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ManageVehicleCores_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/viewmodels/ManageVehicleCores.vue?vue&type=template&id=fc9c3778&":
/*!******************************************************************************!*\
  !*** ./src/viewmodels/ManageVehicleCores.vue?vue&type=template&id=fc9c3778& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ManageVehicleCores_vue_vue_type_template_id_fc9c3778___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/vue-loader/lib??vue-loader-options!./ManageVehicleCores.vue?vue&type=template&id=fc9c3778& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/ManageVehicleCores.vue?vue&type=template&id=fc9c3778&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ManageVehicleCores_vue_vue_type_template_id_fc9c3778___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ManageVehicleCores_vue_vue_type_template_id_fc9c3778___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./src/viewmodels/VehicleCore.vue":
/*!****************************************!*\
  !*** ./src/viewmodels/VehicleCore.vue ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _VehicleCore_vue_vue_type_template_id_7b164da4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./VehicleCore.vue?vue&type=template&id=7b164da4& */ "./src/viewmodels/VehicleCore.vue?vue&type=template&id=7b164da4&");
/* harmony import */ var _VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./VehicleCore.vue?vue&type=script&lang=js& */ "./src/viewmodels/VehicleCore.vue?vue&type=script&lang=js&");
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[key]; }) }(__WEBPACK_IMPORT_KEY__));
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _VehicleCore_vue_vue_type_template_id_7b164da4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _VehicleCore_vue_vue_type_template_id_7b164da4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "src\\viewmodels\\VehicleCore.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./src/viewmodels/VehicleCore.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./src/viewmodels/VehicleCore.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/babel-loader/lib!../../node_modules/vue-loader/lib??vue-loader-options!./VehicleCore.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/VehicleCore.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_VehicleCore_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./src/viewmodels/VehicleCore.vue?vue&type=template&id=7b164da4&":
/*!***********************************************************************!*\
  !*** ./src/viewmodels/VehicleCore.vue?vue&type=template&id=7b164da4& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VehicleCore_vue_vue_type_template_id_7b164da4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/vue-loader/lib??vue-loader-options!./VehicleCore.vue?vue&type=template&id=7b164da4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./src/viewmodels/VehicleCore.vue?vue&type=template&id=7b164da4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VehicleCore_vue_vue_type_template_id_7b164da4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_VehicleCore_vue_vue_type_template_id_7b164da4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./test/index.js":
/*!***********************!*\
  !*** ./test/index.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! jsdom-global */ "jsdom-global")();

// var mocha = require("mocha");
// var chai = require("chai");

// global.describe = mocha.describe;
// global.it = mocha.it;

// global.expect = chai.expect;
// global.assert = chai.assert;

__webpack_require__(/*! ./specs/VehicleCore.spec */ "./test/specs/VehicleCore.spec.js");
__webpack_require__(/*! ./specs/EditVehicleCore.spec */ "./test/specs/EditVehicleCore.spec.js");
__webpack_require__(/*! ./specs/ManageVehicleCores.spec */ "./test/specs/ManageVehicleCores.spec.js");
__webpack_require__(/*! ./specs/CaptureDealVehicleCores.spec */ "./test/specs/CaptureDealVehicleCores.spec.js");

/***/ }),

/***/ "./test/specs/CaptureDealVehicleCores.spec.js":
/*!****************************************************!*\
  !*** ./test/specs/CaptureDealVehicleCores.spec.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _vue = __webpack_require__(/*! vue */ "vue");

var _vue2 = _interopRequireDefault(_vue);

var _testUtils = __webpack_require__(/*! @vue/test-utils */ "@vue/test-utils");

var _CaptureDealVehicleCores = __webpack_require__(/*! ../../src/viewmodels/CaptureDealVehicleCores.vue */ "./src/viewmodels/CaptureDealVehicleCores.vue");

var _CaptureDealVehicleCores2 = _interopRequireDefault(_CaptureDealVehicleCores);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Debugging does not work if mocha is required.
// var mocha = require("mocha");
// var describe = mocha.describe;
// var it = mocha.it;

var chai = __webpack_require__(/*! chai */ "chai");
var assert = chai.assert;

var sinon = __webpack_require__(/*! sinon */ "sinon");

describe("CaptureDealVehicleCores.vue", function () {
    it("should be able to mount", function () {
        // Arrange
        var alertsManager = {
            showSuccess: sinon.spy(),
            showDanger: sinon.spy()
        };

        var apiClient = {
            getDealVehicleCores: sinon.stub().returns({
                then: sinon.stub().returns({
                    catch: sinon.spy()
                })
            }),
            getConfigurationData: sinon.stub().returns({
                then: sinon.stub().returns({
                    catch: sinon.spy()
                })
            })
        };

        var validator = {
            addValidationFor: sinon.spy(),
            isValid: sinon.spy()
        };

        var urlHelper = {
            getQueryStringParam: sinon.spy()
        };

        var onInit = sinon.spy();

        var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
            provide: {
                alertsManager: alertsManager,
                apiClient: apiClient,
                validator: validator,
                onInit: onInit,
                urlHelper: urlHelper
            }
        });

        // Act & Assert
        assert.isNotNull(wrapper.vm);
    });

    describe("created", function () {
        it("should get deal id from query string", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                }),
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            var urlHelper = {
                getQueryStringParam: sinon.spy()
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act & Assert
            assert.isTrue(urlHelper.getQueryStringParam.calledOnce);
        });

        it("should initialize correctly when deal id is given", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                }),
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.dealId, "12345");
        });

        it("should show error when no deal id is given", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                }),
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns(null)
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act & Assert
            assert.isTrue(wrapper.vm.isInitError);
            assert.isNotEmpty(wrapper.vm.initErrorMessage);
        });

        it("should load core data correctly when response is 200", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE",
                                description: "Description",
                                defaultQuantity: 10,
                                defaultPricePerUnit: 10.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act & Assert
            assert.isNotEmpty(wrapper.vm.cores);
            assert.equal(wrapper.vm.cores[0].vehicleCoreId, 1);
            assert.equal(wrapper.vm.cores[0].code, "code");
            assert.equal(wrapper.vm.cores[0].description, "Description");
            assert.isFalse(wrapper.vm.cores[0].isExtracted);
            assert.equal(wrapper.vm.cores[0].quantity, 10);
            assert.equal(wrapper.vm.cores[0].pricePerUnit, 10.00);
            assert.equal(wrapper.vm.cores[0].totalPrice, 0.00);
        });

        it("should use deal's core data if present", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: [{
                                vehicleCoreId: 1,
                                quantity: 5,
                                pricePerUnit: 5.00,
                                totalPrice: 25.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE",
                                description: "Description",
                                defaultQuantity: 10,
                                defaultPricePerUnit: 10.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act & Assert
            assert.isNotEmpty(wrapper.vm.cores);
            assert.equal(wrapper.vm.cores[0].vehicleCoreId, 1);
            assert.equal(wrapper.vm.cores[0].code, "code");
            assert.equal(wrapper.vm.cores[0].description, "Description");
            assert.isTrue(wrapper.vm.cores[0].isExtracted);
            assert.equal(wrapper.vm.cores[0].quantity, 5);
            assert.equal(wrapper.vm.cores[0].pricePerUnit, 5.00);
            assert.equal(wrapper.vm.cores[0].totalPrice, 25.00);
        });

        it("should not use deal's core data if id mismatch", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: [{
                                vehicleCoreId: 2,
                                quantity: 5,
                                pricePerUnit: 5.00,
                                totalPrice: 25.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE",
                                description: "Description",
                                defaultQuantity: 10,
                                defaultPricePerUnit: 10.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act & Assert
            assert.isNotEmpty(wrapper.vm.cores);
            assert.equal(wrapper.vm.cores[0].vehicleCoreId, 1);
            assert.equal(wrapper.vm.cores[0].code, "code");
            assert.equal(wrapper.vm.cores[0].description, "Description");
            assert.isFalse(wrapper.vm.cores[0].isExtracted);
            assert.equal(wrapper.vm.cores[0].quantity, 10);
            assert.equal(wrapper.vm.cores[0].pricePerUnit, 10.00);
            assert.equal(wrapper.vm.cores[0].totalPrice, 0.00);
        });

        it("should configure validator for the given cores", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE_1",
                                description: "Description_1",
                                defaultQuantity: 10,
                                defaultPricePerUnit: 10.00
                            }, {
                                id: 2,
                                code: "CODE_2",
                                description: "Description_2",
                                defaultQuantity: 5,
                                defaultPricePerUnit: 5.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act & Assert
            assert.isTrue(validator.addValidationFor.calledTwice);
        });
    });

    describe("computed: totalValue", function () {
        it("should be calculated correctly", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: [{
                                vehicleCoreId: 1,
                                quantity: 10,
                                pricePerUnit: 10.00,
                                totalPrice: 100.00
                            }, {
                                vehicleCoreId: 2,
                                quantity: 5,
                                pricePerUnit: 5.00,
                                totalPrice: 25.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE_1",
                                description: "Description_1",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }, {
                                id: 2,
                                code: "CODE_2",
                                description: "Description_2",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.totalValue, 125.00);
        });

        it("should be zero when no cores are extracted", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE_1",
                                description: "Description_1",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }, {
                                id: 2,
                                code: "CODE_2",
                                description: "Description_2",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.totalValue, 0.00);
        });
    });

    describe("method: submit", function () {
        it("should call the api", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE_1",
                                description: "Description_1",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(apiClient.submitDealVehicleCores.called);
        });

        it("should not call the api when data is not valid", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE_1",
                                description: "Description_1",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(false)
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(apiClient.submitDealVehicleCores.notCalled);
        });

        it("should show a success alert when response is 200", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE_1",
                                description: "Description_1",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(alertsManager.showSuccess.called);
        });

        it("should show a danger alert when response is 400", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE_1",
                                description: "Description_1",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 400
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(alertsManager.showDanger.called);
        });

        it("should show a danger alert when response is 401", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE_1",
                                description: "Description_1",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 401
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(alertsManager.showDanger.called);
        });

        it("should show a danger alert when network error", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{
                                id: 1,
                                code: "CODE_1",
                                description: "Description_1",
                                defaultQuantity: 1,
                                defaultPricePerUnit: 1.00
                            }]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: function _catch(callback) {
                            callback("mock error");
                            return;
                        }
                    })
                })
            };

            var validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            var urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_CaptureDealVehicleCores2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validator: validator,
                    onInit: onInit,
                    urlHelper: urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(alertsManager.showDanger.called);
        });
    });
});

/***/ }),

/***/ "./test/specs/EditVehicleCore.spec.js":
/*!********************************************!*\
  !*** ./test/specs/EditVehicleCore.spec.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _vue = __webpack_require__(/*! vue */ "vue");

var _vue2 = _interopRequireDefault(_vue);

var _testUtils = __webpack_require__(/*! @vue/test-utils */ "@vue/test-utils");

var _EditVehicleCore = __webpack_require__(/*! ../../src/viewmodels/EditVehicleCore.vue */ "./src/viewmodels/EditVehicleCore.vue");

var _EditVehicleCore2 = _interopRequireDefault(_EditVehicleCore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Debugging does not work if mocha is required.
// var mocha = require("mocha");
// var describe = mocha.describe;
// var it = mocha.it;

var chai = __webpack_require__(/*! chai */ "chai");
var assert = chai.assert;

var sinon = __webpack_require__(/*! sinon */ "sinon");

describe("EditVehicleCore.vue", function () {
    it("should be able to mount", function () {
        // Arrange
        var alertsManager = {
            showSuccess: sinon.spy(),
            showDanger: sinon.spy()
        };

        var apiClient = {
            updateVehicleCore: sinon.spy()
        };

        var validatorFactory = {
            makeValidator: sinon.spy()
        };

        var wrapper = (0, _testUtils.mount)(_EditVehicleCore2.default, {
            provide: {
                alertsManager: alertsManager,
                apiClient: apiClient,
                validatorFactory: validatorFactory
            },
            propsData: {
                coreToEdit: {
                    id: 1,
                    code: "CODE",
                    description: "Code",
                    defaultQuantity: 10,
                    defaultPricePerUnit: 10.00
                }
            }
        });

        // Act & Assert
        assert.isNotNull(wrapper.vm);
    });

    describe("data: core", function () {
        it("should correctly be initialized", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                updateVehicleCore: sinon.spy()
            };

            var validatorFactory = {
                makeValidator: sinon.spy()
            };

            var wrapper = (0, _testUtils.mount)(_EditVehicleCore2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validatorFactory: validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.core.id, 1);
            assert.equal(wrapper.vm.core.code, "code");
            assert.equal(wrapper.vm.core.description, "Code");
            assert.equal(wrapper.vm.core.defaultQuantity, 10);
            assert.equal(wrapper.vm.core.defaultPricePerUnit, 10.00);
        });
    });

    describe("method: save", function () {
        it("should do nothing when not valid", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                updateVehicleCore: sinon.spy()
            };

            var validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: function isValid() {
                        return false;
                    }
                })
            };

            var wrapper = (0, _testUtils.mount)(_EditVehicleCore2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validatorFactory: validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.notCalled);
        });

        it("should call the api when valid", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            var validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: function isValid() {
                        return true;
                    }
                })
            };

            var wrapper = (0, _testUtils.mount)(_EditVehicleCore2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validatorFactory: validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
        });

        it("should show success alert when response is 200", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: function isValid() {
                        return true;
                    }
                })
            };

            var wrapper = (0, _testUtils.mount)(_EditVehicleCore2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validatorFactory: validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
            assert.isTrue(alertsManager.showSuccess.calledOnce);
            assert.isTrue(alertsManager.showDanger.notCalled);
        });

        it("should show danger alert when response is 400", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 400
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: function isValid() {
                        return true;
                    }
                })
            };

            var wrapper = (0, _testUtils.mount)(_EditVehicleCore2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validatorFactory: validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
            assert.isTrue(alertsManager.showSuccess.notCalled);
            assert.isTrue(alertsManager.showDanger.calledOnce);
        });

        it("should show danger alert when response is 401", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 401
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: function isValid() {
                        return true;
                    }
                })
            };

            var wrapper = (0, _testUtils.mount)(_EditVehicleCore2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validatorFactory: validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
            assert.isTrue(alertsManager.showSuccess.notCalled);
            assert.isTrue(alertsManager.showDanger.calledOnce);
        });

        it("should show danger alert when there's a fetch error", function () {
            // Arrange
            var alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            var apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: function then() {
                        return {
                            catch: function _catch(callback) {
                                callback("mock error");
                            }
                        };
                    }
                })
            };

            var validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: function isValid() {
                        return true;
                    }
                })
            };

            var wrapper = (0, _testUtils.mount)(_EditVehicleCore2.default, {
                provide: {
                    alertsManager: alertsManager,
                    apiClient: apiClient,
                    validatorFactory: validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
            assert.isTrue(alertsManager.showSuccess.notCalled);
            assert.isTrue(alertsManager.showDanger.calledOnce);
        });
    });
});

/***/ }),

/***/ "./test/specs/ManageVehicleCores.spec.js":
/*!***********************************************!*\
  !*** ./test/specs/ManageVehicleCores.spec.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _vue = __webpack_require__(/*! vue */ "vue");

var _vue2 = _interopRequireDefault(_vue);

var _testUtils = __webpack_require__(/*! @vue/test-utils */ "@vue/test-utils");

var _ManageVehicleCores = __webpack_require__(/*! ../../src/viewmodels/ManageVehicleCores.vue */ "./src/viewmodels/ManageVehicleCores.vue");

var _ManageVehicleCores2 = _interopRequireDefault(_ManageVehicleCores);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Debugging does not work if mocha is required.
// var mocha = require("mocha");
// var describe = mocha.describe;
// var it = mocha.it;

var chai = __webpack_require__(/*! chai */ "chai");
var assert = chai.assert;

var sinon = __webpack_require__(/*! sinon */ "sinon");

describe("ManageVehicleCores.vue", function () {
    it("should be able to mount", function () {
        // Arrange
        var apiClient = {
            getConfigurationData: sinon.stub().returns({
                then: sinon.stub().returns({
                    catch: sinon.spy()
                })
            })
        };

        var onInit = sinon.spy();

        var wrapper = (0, _testUtils.shallowMount)(_ManageVehicleCores2.default, {
            provide: {
                apiClient: apiClient,
                onInit: onInit
            }
        });

        // Act & Assert
        assert.isNotNull(wrapper.vm);
    });

    describe("data", function () {
        it("should initialize correctly", function () {
            // Arrange
            var apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_ManageVehicleCores2.default, {
                provide: {
                    apiClient: apiClient,
                    onInit: onInit
                }
            });

            // Act & Assert
            assert.isFalse(wrapper.vm.isInitError);
            assert.isEmpty(wrapper.vm.initErrorMessage);
            assert.isFalse(wrapper.vm.isLoadingRequest);
            assert.isEmpty(wrapper.vm.cores);
        });
    });

    describe("created", function () {
        it("should assign cores on response 200", function () {
            // Arrange
            var apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200,
                            cores: [{ code: "a" }, { code: "b" }, { code: "c" }]
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_ManageVehicleCores2.default, {
                provide: {
                    apiClient: apiClient,
                    onInit: onInit
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.cores.length, 3);
            assert.equal(wrapper.vm.cores[0].code, "a");
            assert.equal(wrapper.vm.cores[1].code, "b");
            assert.equal(wrapper.vm.cores[2].code, "c");
        });

        it("should call onInit on response 200", function () {
            // Arrange
            var apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 200
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_ManageVehicleCores2.default, {
                provide: {
                    apiClient: apiClient,
                    onInit: onInit
                }
            });

            // Act & Assert
            assert.isTrue(onInit.calledOnce);
        });

        it("should alert on response not 200", function () {
            // Arrange
            var apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 400
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_ManageVehicleCores2.default, {
                provide: {
                    apiClient: apiClient,
                    onInit: onInit
                }
            });

            // Act & Assert
            assert.isTrue(wrapper.vm.isInitError);
            assert.isNotEmpty(wrapper.vm.initErrorMessage);
        });

        it("should call onInit on response not 200", function () {
            // Arrange
            var apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: function then(callback) {
                        callback({
                            status: 400
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_ManageVehicleCores2.default, {
                provide: {
                    apiClient: apiClient,
                    onInit: onInit
                }
            });

            // Act & Assert
            assert.isTrue(onInit.calledOnce);
        });

        it("should alert on network error", function () {
            // Arrange
            var apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: function _catch(callback) {
                            callback("mock error");
                        }
                    })
                })
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_ManageVehicleCores2.default, {
                provide: {
                    apiClient: apiClient,
                    onInit: onInit
                }
            });

            // Act & Assert
            assert.isTrue(wrapper.vm.isInitError);
            assert.isNotEmpty(wrapper.vm.initErrorMessage);
        });

        it("should call onInit on network error", function () {
            // Arrange
            var apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: function _catch(callback) {
                            callback("mock error");
                        }
                    })
                })
            };

            var onInit = sinon.spy();

            var wrapper = (0, _testUtils.shallowMount)(_ManageVehicleCores2.default, {
                provide: {
                    apiClient: apiClient,
                    onInit: onInit
                }
            });

            // Act & Assert
            assert.isTrue(onInit.calledOnce);
        });
    });
});

/***/ }),

/***/ "./test/specs/VehicleCore.spec.js":
/*!****************************************!*\
  !*** ./test/specs/VehicleCore.spec.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _vue = __webpack_require__(/*! vue */ "vue");

var _vue2 = _interopRequireDefault(_vue);

var _testUtils = __webpack_require__(/*! @vue/test-utils */ "@vue/test-utils");

var _VehicleCore = __webpack_require__(/*! ../../src/viewmodels/VehicleCore.vue */ "./src/viewmodels/VehicleCore.vue");

var _VehicleCore2 = _interopRequireDefault(_VehicleCore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Debugging does not work if mocha is required.
// var mocha = require("mocha");
// var describe = mocha.describe;
// var it = mocha.it;

var chai = __webpack_require__(/*! chai */ "chai");
var assert = chai.assert;

describe("VehicleCore.vue", function () {
    it("should be able to mount", function () {
        // Arrange
        var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
            propsData: {
                core: {
                    isExtracted: true,
                    quantity: 15,
                    pricePerUnit: 10.00,
                    code: "asdasd",
                    description: "adasdasd"
                }
            }
        });

        // Act & Assert
        assert.isNotNull(wrapper.vm);
    });

    describe("data: isExtracted", function () {
        it("should be yes when incoming prop is true", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: true,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.isExtracted, "yes");
        });

        it("should be no when incoming prop is false", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.isExtracted, "no");
        });

        it("should be no when incoming prop is null", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: null,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.isExtracted, "no");
        });
    });

    describe("computed: totalPrice", function () {
        it("should be calculated initially", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.totalPrice, 150);
        });

        it("should be calculated reactively", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.setProps({
                core: {
                    isExtracted: false,
                    quantity: 10,
                    pricePerUnit: 10.00,
                    code: "asdasd",
                    description: "adasdasd"
                }
            });

            // Assert
            assert.equal(wrapper.vm.totalPrice, 100);
        });

        it("should be zero when quantity missing", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: null,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.totalPrice, 0.00);
        });

        it("should be zero when pricePerUnit missing", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 10,
                        pricePerUnit: null,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.totalPrice, 0.00);
        });

        it("should be zero when quantity and pricePerUnit missing", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 10,
                        pricePerUnit: null,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.totalPrice, 0.00);
        });
    });

    describe("computed: totalPriceDisplay", function () {
        it("should be calculated initially", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.totalPriceDisplay, "150.00");
        });

        it("should be calculated reactively", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.setProps({
                core: {
                    isExtracted: false,
                    quantity: 10,
                    pricePerUnit: 10.00,
                    code: "asdasd",
                    description: "adasdasd"
                }
            });

            // Assert
            assert.equal(wrapper.vm.totalPriceDisplay, "100.00");
        });

        it("should be zero when no totalPrice", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.setProps({
                core: {
                    isExtracted: false,
                    quantity: null,
                    pricePerUnit: null,
                    code: "asdasd",
                    description: "adasdasd"
                }
            });

            // Assert
            assert.equal(wrapper.vm.totalPriceDisplay, "0.00");
        });
    });

    describe("method: isExtractedYesOnClick", function () {
        it("should emit change-is-extracted with a true argument", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.vm.isExtractedYesOnClick();

            // Assert
            // the event was emitted
            assert.exists(wrapper.emitted()["change-is-extracted"]);
            // times that the event was emitted
            assert.equal(wrapper.emitted()["change-is-extracted"].length, 1);

            // playload with which the event was emitted
            var firstCall = 0;
            var firstPayloadArg = 0;
            assert.equal(wrapper.emitted()["change-is-extracted"][firstCall][firstPayloadArg], true);
        });
    });

    describe("method: isExtractedNoOnClick", function () {
        it("should emit change-is-extracted with a false argument", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.vm.isExtractedNoOnClick();

            // Assert
            // the event was emitted
            assert.exists(wrapper.emitted()["change-is-extracted"]);
            // times that the event was emitted
            assert.equal(wrapper.emitted()["change-is-extracted"].length, 1);

            // playload with which the event was emitted
            var firstCall = 0;
            var firstPayloadArg = 0;
            assert.equal(wrapper.emitted()["change-is-extracted"][firstCall][firstPayloadArg], false);
        });
    });

    describe("method: quantityOnInput", function () {
        it("should emit input-quantity with given argument", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.vm.quantityOnInput({
                target: {
                    value: 10
                }
            });

            // Assert
            // the event was emitted
            assert.exists(wrapper.emitted()["input-quantity"]);
            // times that the event was emitted
            assert.equal(wrapper.emitted()["input-quantity"].length, 1);

            // playload with which the event was emitted
            var firstCall = 0;
            var firstPayloadArg = 0;
            assert.equal(wrapper.emitted()["input-quantity"][firstCall][firstPayloadArg], 10);
        });
    });

    describe("method: pricePerUnitOnInput", function () {
        it("should emit input-price-per-unit with given argument", function () {
            // Arrange
            var wrapper = (0, _testUtils.mount)(_VehicleCore2.default, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.vm.pricePerUnitOnInput({
                target: {
                    value: 10.00
                }
            });

            // Assert
            // the event was emitted
            assert.exists(wrapper.emitted()["input-price-per-unit"]);
            // times that the event was emitted
            assert.equal(wrapper.emitted()["input-price-per-unit"].length, 1);

            // playload with which the event was emitted
            var firstCall = 0;
            var firstPayloadArg = 0;
            assert.equal(wrapper.emitted()["input-price-per-unit"][firstCall][firstPayloadArg], 10.00);
        });
    });
});

/***/ }),

/***/ "@vue/test-utils":
/*!**********************************!*\
  !*** external "@vue/test-utils" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@vue/test-utils");

/***/ }),

/***/ "chai":
/*!***********************!*\
  !*** external "chai" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("chai");

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jquery" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("jquery");

/***/ }),

/***/ "jsdom-global":
/*!*******************************!*\
  !*** external "jsdom-global" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("jsdom-global");

/***/ }),

/***/ "sinon":
/*!************************!*\
  !*** external "sinon" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sinon");

/***/ }),

/***/ "vue":
/*!**********************!*\
  !*** external "vue" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("vue");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC5idW5kbGUuanMiLCJzb3VyY2VzIjpbIndlYnBhY2svYm9vdHN0cmFwIiwiQ2FwdHVyZURlYWxWZWhpY2xlQ29yZXMudnVlIiwiRWRpdFZlaGljbGVDb3JlLnZ1ZSIsIk1hbmFnZVZlaGljbGVDb3Jlcy52dWUiLCJWZWhpY2xlQ29yZS52dWUiLCJDOlxcVXNlcnNcXGtldmluXFxEZXNrdG9wXFxTb2Z0d2FyZVByb2plY3RzXFxjc2FyLWNvcmUtYWRtaW5cXHB1YmxpY1xcanNcXHNyY1xcdmlld21vZGVsc1xcQ2FwdHVyZURlYWxWZWhpY2xlQ29yZXMudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWMyOTk3MmRhJj83Y2I2IiwiQzpcXFVzZXJzXFxrZXZpblxcRGVza3RvcFxcU29mdHdhcmVQcm9qZWN0c1xcY3Nhci1jb3JlLWFkbWluXFxwdWJsaWNcXGpzXFxzcmNcXHZpZXdtb2RlbHNcXEVkaXRWZWhpY2xlQ29yZS52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9NjdhMWM5YmEmPzU3NjgiLCJDOlxcVXNlcnNcXGtldmluXFxEZXNrdG9wXFxTb2Z0d2FyZVByb2plY3RzXFxjc2FyLWNvcmUtYWRtaW5cXHB1YmxpY1xcanNcXHNyY1xcdmlld21vZGVsc1xcTWFuYWdlVmVoaWNsZUNvcmVzLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1mYzljMzc3OCY/MTdmYSIsIkM6XFxVc2Vyc1xca2V2aW5cXERlc2t0b3BcXFNvZnR3YXJlUHJvamVjdHNcXGNzYXItY29yZS1hZG1pblxccHVibGljXFxqc1xcc3JjXFx2aWV3bW9kZWxzXFxWZWhpY2xlQ29yZS52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9N2IxNjRkYTQmPzMwYmYiLCJDOlxcVXNlcnNcXGtldmluXFxEZXNrdG9wXFxTb2Z0d2FyZVByb2plY3RzXFxjc2FyLWNvcmUtYWRtaW5cXHB1YmxpY1xcanNcXG5vZGVfbW9kdWxlc1xcdnVlLWxvYWRlclxcbGliXFxydW50aW1lXFxjb21wb25lbnROb3JtYWxpemVyLmpzIiwiQzpcXFVzZXJzXFxrZXZpblxcRGVza3RvcFxcU29mdHdhcmVQcm9qZWN0c1xcY3Nhci1jb3JlLWFkbWluXFxwdWJsaWNcXGpzXFxzcmNcXHZpZXdtb2RlbHNcXENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLnZ1ZSIsIkM6XFxVc2Vyc1xca2V2aW5cXERlc2t0b3BcXFNvZnR3YXJlUHJvamVjdHNcXGNzYXItY29yZS1hZG1pblxccHVibGljXFxqc1xcc3JjXFx2aWV3bW9kZWxzXFxDYXB0dXJlRGVhbFZlaGljbGVDb3Jlcy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmIiwiQzpcXFVzZXJzXFxrZXZpblxcRGVza3RvcFxcU29mdHdhcmVQcm9qZWN0c1xcY3Nhci1jb3JlLWFkbWluXFxwdWJsaWNcXGpzXFxzcmNcXHZpZXdtb2RlbHNcXENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1jMjk5NzJkYSYiLCJDOlxcVXNlcnNcXGtldmluXFxEZXNrdG9wXFxTb2Z0d2FyZVByb2plY3RzXFxjc2FyLWNvcmUtYWRtaW5cXHB1YmxpY1xcanNcXHNyY1xcdmlld21vZGVsc1xcRWRpdFZlaGljbGVDb3JlLnZ1ZSIsIkM6XFxVc2Vyc1xca2V2aW5cXERlc2t0b3BcXFNvZnR3YXJlUHJvamVjdHNcXGNzYXItY29yZS1hZG1pblxccHVibGljXFxqc1xcc3JjXFx2aWV3bW9kZWxzXFxFZGl0VmVoaWNsZUNvcmUudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJiIsIkM6XFxVc2Vyc1xca2V2aW5cXERlc2t0b3BcXFNvZnR3YXJlUHJvamVjdHNcXGNzYXItY29yZS1hZG1pblxccHVibGljXFxqc1xcc3JjXFx2aWV3bW9kZWxzXFxFZGl0VmVoaWNsZUNvcmUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTY3YTFjOWJhJiIsIkM6XFxVc2Vyc1xca2V2aW5cXERlc2t0b3BcXFNvZnR3YXJlUHJvamVjdHNcXGNzYXItY29yZS1hZG1pblxccHVibGljXFxqc1xcc3JjXFx2aWV3bW9kZWxzXFxNYW5hZ2VWZWhpY2xlQ29yZXMudnVlIiwiQzpcXFVzZXJzXFxrZXZpblxcRGVza3RvcFxcU29mdHdhcmVQcm9qZWN0c1xcY3Nhci1jb3JlLWFkbWluXFxwdWJsaWNcXGpzXFxzcmNcXHZpZXdtb2RlbHNcXE1hbmFnZVZlaGljbGVDb3Jlcy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmIiwiQzpcXFVzZXJzXFxrZXZpblxcRGVza3RvcFxcU29mdHdhcmVQcm9qZWN0c1xcY3Nhci1jb3JlLWFkbWluXFxwdWJsaWNcXGpzXFxzcmNcXHZpZXdtb2RlbHNcXE1hbmFnZVZlaGljbGVDb3Jlcy52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9ZmM5YzM3NzgmIiwiQzpcXFVzZXJzXFxrZXZpblxcRGVza3RvcFxcU29mdHdhcmVQcm9qZWN0c1xcY3Nhci1jb3JlLWFkbWluXFxwdWJsaWNcXGpzXFxzcmNcXHZpZXdtb2RlbHNcXFZlaGljbGVDb3JlLnZ1ZSIsIkM6XFxVc2Vyc1xca2V2aW5cXERlc2t0b3BcXFNvZnR3YXJlUHJvamVjdHNcXGNzYXItY29yZS1hZG1pblxccHVibGljXFxqc1xcc3JjXFx2aWV3bW9kZWxzXFxWZWhpY2xlQ29yZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmIiwiQzpcXFVzZXJzXFxrZXZpblxcRGVza3RvcFxcU29mdHdhcmVQcm9qZWN0c1xcY3Nhci1jb3JlLWFkbWluXFxwdWJsaWNcXGpzXFxzcmNcXHZpZXdtb2RlbHNcXFZlaGljbGVDb3JlLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD03YjE2NGRhNCYiLCJ0ZXN0XFxpbmRleC5qcyIsInRlc3RcXHNwZWNzXFxDYXB0dXJlRGVhbFZlaGljbGVDb3Jlcy5zcGVjLmpzIiwidGVzdFxcc3BlY3NcXEVkaXRWZWhpY2xlQ29yZS5zcGVjLmpzIiwidGVzdFxcc3BlY3NcXE1hbmFnZVZlaGljbGVDb3Jlcy5zcGVjLmpzIiwidGVzdFxcc3BlY3NcXFZlaGljbGVDb3JlLnNwZWMuanMiLCJleHRlcm5hbCBcIkB2dWUvdGVzdC11dGlsc1wiIiwiZXh0ZXJuYWwgXCJjaGFpXCIiLCJleHRlcm5hbCBcImpxdWVyeVwiIiwiZXh0ZXJuYWwgXCJqc2RvbS1nbG9iYWxcIiIsImV4dGVybmFsIFwic2lub25cIiIsImV4dGVybmFsIFwidnVlXCIiXSwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi90ZXN0L2luZGV4LmpzXCIpO1xuIiwiPHRlbXBsYXRlPlxyXG4gICAgPGRpdj5cclxuICAgICAgICA8dGVtcGxhdGUgdi1pZj1cImlzSW5pdEVycm9yXCI+XHJcbiAgICAgICAgICAgIDxkaXYgaWQ9XCJpbml0X2FsZXJ0c19jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJhbGVydCBhbGVydC1kYW5nZXIgYWxlcnQtZGlzbWlzc2FibGUgY2VudGVyZWQtY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge3sgaW5pdEVycm9yTWVzc2FnZSB9fVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvdGVtcGxhdGU+XHJcbiAgICAgICAgPHRlbXBsYXRlIHYtZWxzZT5cclxuXHJcbiAgICAgICAgICAgIDxoMj5FbnRlciB0aGUgdmVoaWNsZSdzIGV4dHJhY3RlZCBjb3JlcyBmb3IgZGVhbCB7eyBkZWFsSWQgfX08L2gyPlxyXG5cclxuICAgICAgICAgICAgPGhyPlxyXG5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRvdGFsLWNvcmUtdmFsdWUtY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICBUb3RhbCBWYWx1ZTogPHNwYW4gY2xhc3M9XCJsYWJlbCBsYWJlbC1zdWNjZXNzXCI+JHt7IHRvdGFsVmFsdWUgfX08L3NwYW4+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPGhyPlxyXG5cclxuICAgICAgICAgICAgPGZvcm0gaWQ9XCJjb3Jlc19mb3JtXCI+XHJcbiAgICAgICAgICAgICAgICA8dGVtcGxhdGUgdi1mb3I9XCJjb3JlIGluIGNvcmVzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPFZlaGljbGVDb3JlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHYtYmluZDprZXk9XCJjb3JlLmNvZGVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2LWJpbmQ6Y29yZT1cImNvcmVcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2LW9uOmNoYW5nZS1pcy1leHRyYWN0ZWQ9XCJjb3JlLmlzRXh0cmFjdGVkID0gJGV2ZW50XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdi1vbjppbnB1dC1wcmljZS1wZXItdW5pdD1cImNvcmUucHJpY2VQZXJVbml0ID0gJGV2ZW50XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdi1vbjppbnB1dC1xdWFudGl0eT1cImNvcmUucXVhbnRpdHkgPSAkZXZlbnRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2LW9uOmNvbXB1dGVkLXRvdGFsLXByaWNlPVwiY29yZS50b3RhbFByaWNlID0gJGV2ZW50XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9WZWhpY2xlQ29yZT5cclxuICAgICAgICAgICAgICAgIDwvdGVtcGxhdGU+XHJcbiAgICAgICAgICAgIDwvZm9ybT5cclxuXHJcbiAgICAgICAgICAgIDxocj5cclxuXHJcbiAgICAgICAgICAgIDxkaXYgaWQ9XCJhbGVydHNfY29udGFpbmVyXCI+PC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IGlkPVwidmFsaWRhdGlvbl9lcnJvcl9jb250YWluZXJcIiBjbGFzcz1cInZhbGlkYXRpb24tZXJyb3ItYWxlcnQgYWxlcnQgYWxlcnQtZGFuZ2VyIGFsZXJ0LWRpc21pc3NhYmxlIGZhZGUgaW5cIj5cclxuICAgICAgICAgICAgICAgIDxhIGNsYXNzPVwiY2xvc2VcIiBhcmlhLWxhYmVsPVwiY2xvc2VcIj7DlzwvYT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cImJ0biBidG4tcHJpbWFyeSBidG4tbGdcIiB2LW9uOmNsaWNrPVwic3VibWl0XCIgdi1iaW5kOmRpc2FibGVkPVwiaXNMb2FkaW5nUmVxdWVzdFwiPlNhdmU8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDxkaXYgdi1zaG93PVwiaXNMb2FkaW5nUmVxdWVzdFwiIGNsYXNzPVwic3VibWl0LWJ1dHRvbi1sb2FkaW5nLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVwiaW1nL2xvYWRpbmdfc21hbGwuZ2lmXCIgYWx0PVwiTG9hZGluZy4uLlwiPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRvdGFsLWNvcmUtdmFsdWUtY29udGFpbmVyIHB1bGwtcmlnaHRcIj5cclxuICAgICAgICAgICAgICAgICAgICBUb3RhbCBWYWx1ZTogPHNwYW4gY2xhc3M9XCJsYWJlbCBsYWJlbC1zdWNjZXNzXCI+JHt7IHRvdGFsVmFsdWUgfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgIDwvdGVtcGxhdGU+XHJcbiAgICA8L2Rpdj5cclxuPC90ZW1wbGF0ZT5cclxuXHJcbjxzY3JpcHQ+XHJcbmltcG9ydCAkIGZyb20gXCJqcXVlcnlcIjtcclxuaW1wb3J0IFZlaGljbGVDb3JlIGZyb20gXCIuL1ZlaGljbGVDb3JlLnZ1ZVwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cclxuICAgIGNvbXBvbmVudHM6IHtcclxuICAgICAgICBWZWhpY2xlQ29yZVxyXG4gICAgfSxcclxuXHJcbiAgICBpbmplY3Q6IFtcImFsZXJ0c01hbmFnZXJcIiwgXCJhcGlDbGllbnRcIiwgXCJ2YWxpZGF0b3JcIiwgXCJ1cmxIZWxwZXJcIiwgXCJvbkluaXRcIl0sXHJcblxyXG4gICAgZGF0YTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGlzSW5pdEVycm9yOiBmYWxzZSxcclxuICAgICAgICAgICAgaW5pdEVycm9yTWVzc2FnZTogXCJcIixcclxuICAgICAgICAgICAgaXNMb2FkaW5nUmVxdWVzdDogZmFsc2UsXHJcbiAgICAgICAgICAgIGRlYWxJZDogbnVsbCxcclxuICAgICAgICAgICAgY29yZXM6IFtdLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGNyZWF0ZWQ6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgdGhpcy5kZWFsSWQgPSB0aGlzLnVybEhlbHBlci5nZXRRdWVyeVN0cmluZ1BhcmFtKFwiZGVhbElkXCIpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuZGVhbElkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNJbml0RXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmluaXRFcnJvck1lc3NhZ2UgPSBcIk5vIGRlYWwgbnVtYmVyIHNwZWNpZmllZC5cIjtcclxuXHJcbiAgICAgICAgICAgIHRoaXMub25Jbml0KCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuYXBpQ2xpZW50LmdldERlYWxWZWhpY2xlQ29yZXModGhpcy5kZWFsSWQpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGRlYWxWZWhpY2xlQ29yZXMgPSByZXNwb25zZS5kZWFsVmVoaWNsZUNvcmVzO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFwaUNsaWVudC5nZXRDb25maWd1cmF0aW9uRGF0YSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09IDIwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBkZWZhdWx0Q29yZXMgPSByZXNwb25zZS5jb3JlcztcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb3JlcyA9IGRlZmF1bHRDb3Jlcy5tYXAoZGVmYXVsdENvcmUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgYWN0dWFsQ29yZSA9IGRlYWxWZWhpY2xlQ29yZXMuZmluZChjID0+IGMudmVoaWNsZUNvcmVJZCA9PSBkZWZhdWx0Q29yZS5pZCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmVoaWNsZUNvcmVJZDogZGVmYXVsdENvcmUuaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBkZWZhdWx0Q29yZS5jb2RlLnRvTG93ZXJDYXNlKCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogZGVmYXVsdENvcmUuZGVzY3JpcHRpb24sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc0V4dHJhY3RlZDogYWN0dWFsQ29yZSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiBhY3R1YWxDb3JlID8gYWN0dWFsQ29yZS5xdWFudGl0eSA6IGRlZmF1bHRDb3JlLmRlZmF1bHRRdWFudGl0eSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaWNlUGVyVW5pdDogYWN0dWFsQ29yZSA/IGFjdHVhbENvcmUucHJpY2VQZXJVbml0IDogZGVmYXVsdENvcmUuZGVmYXVsdFByaWNlUGVyVW5pdCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsUHJpY2U6IGFjdHVhbENvcmUgPyBhY3R1YWxDb3JlLnRvdGFsUHJpY2UgOiAwLjAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRDb3Jlcy5mb3JFYWNoKGMgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbGlkYXRvci5hZGRWYWxpZGF0aW9uRm9yKGMuY29kZS50b0xvd2VyQ2FzZSgpLCBjLmRlc2NyaXB0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNJbml0RXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW5pdEVycm9yTWVzc2FnZSA9IFwiQ291bGQgbm90IHJldHJpZXZlIGNvbmZpZ3VyYXRpb24gZGF0YS4gUGxlYXNlIHRyeSBhZ2FpbiBsYXRlci5cIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9uSW5pdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNJbml0RXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbml0RXJyb3JNZXNzYWdlID0gXCJUaGVyZSBoYXMgYmVlbiBhIG5ldHdvcmsgb3Igc2VydmVyIGVycm9yLiBQbGVhc2UgdHJ5IGFnYWluIGxhdGVyLiBcIiArIGVycm9yO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub25Jbml0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAocmVzcG9uc2Uuc3RhdHVzID09IDQwMCkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzSW5pdEVycm9yID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmluaXRFcnJvck1lc3NhZ2UgPSBcIlRoZSBzcGVjaWZpZWQgZGVhbCBkb2VzIG5vdCBleGlzdC5cIjtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbkluaXQoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PSA0MDEpIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0luaXRFcnJvciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbml0RXJyb3JNZXNzYWdlID0gXCJZb3VyIHNlc3Npb24gaGFzIGV4cGlyZWQuIFBsZWFzZSBjbG9zZSB0aGlzIHdpbmRvdyBhbmQgdHJ5IHRvIG9wZW4gaXQgYWdhaW4gZnJvbSB0aGUgU3ByZWFkc2hlZXQuXCI7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25Jbml0KCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5pc0luaXRFcnJvciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmluaXRFcnJvck1lc3NhZ2UgPSBcIlRoZXJlIGhhcyBiZWVuIGEgbmV0d29yayBvciBzZXJ2ZXIgZXJyb3IuIFBsZWFzZSB0cnkgYWdhaW4gbGF0ZXIuIFwiICsgZXJyb3I7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5vbkluaXQoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIG1vdW50ZWQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKFwiZGl2LnZhbGlkYXRpb24tZXJyb3ItYWxlcnQgYS5jbG9zZVwiKS5jbGljayhmdW5jdGlvbihldmVudE9iamVjdCkge1xyXG4gICAgICAgICAgICAkKGV2ZW50T2JqZWN0LnRhcmdldC5wYXJlbnRFbGVtZW50KS5oaWRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9LFxyXG5cclxuICAgIGNvbXB1dGVkOiB7XHJcbiAgICAgICAgdG90YWxWYWx1ZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBsZXQgY29yZXNUb3RhbFZhbHVlID0gMC4wMDtcclxuICAgICAgICAgICAgbGV0IGV4dHJhY3RlZENvcmVzID0gdGhpcy5jb3Jlcy5maWx0ZXIoYyA9PiBjLmlzRXh0cmFjdGVkKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChleHRyYWN0ZWRDb3Jlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBjb3Jlc1RvdGFsVmFsdWUgPSBleHRyYWN0ZWRDb3Jlcy5tYXAoYyA9PiBjLnRvdGFsUHJpY2UpLnJlZHVjZSgocHJldmlvdXMsIGN1cnJlbnQpID0+IHByZXZpb3VzICsgY3VycmVudCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiBOdW1iZXIoY29yZXNUb3RhbFZhbHVlKS50b0ZpeGVkKDIpO1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHN1Ym1pdDogZnVuY3Rpb24gKC8qZXZlbnQqLykge1xyXG5cclxuICAgICAgICAgICAgaWYgKCF0aGlzLnZhbGlkYXRvci5pc1ZhbGlkKCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IHZlaGljbGVDb3Jlc0RhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBkZWFsSWQ6IHRoaXMuZGVhbElkLFxyXG4gICAgICAgICAgICAgICAgY29yZXM6IHRoaXMuY29yZXNcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nUmVxdWVzdCA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmFwaUNsaWVudC5zdWJtaXREZWFsVmVoaWNsZUNvcmVzKHZlaGljbGVDb3Jlc0RhdGEpLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hbGVydHNNYW5hZ2VyLnNob3dTdWNjZXNzKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiZGl2I2FsZXJ0c19jb250YWluZXJcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVGhlIGNvcmVzIGluZm9ybWF0aW9uIGhhcyBiZWVuIHNhdmVkIHN1Y2Nlc3NmdWxseS4gUGxlYXNlIHJlZnJlc2ggeW91ciBTcHJlYWRzaGVldCB0byBzZWUgdGhlIGNoYW5nZXMuXCJcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAocmVzcG9uc2Uuc3RhdHVzID09IDQwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWxlcnRzTWFuYWdlci5zaG93RGFuZ2VyKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiZGl2I2FsZXJ0c19jb250YWluZXJcIiksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVGhlcmUgaGFzIGJlZW4gYSB2YWxpZGF0aW9uIGVycm9yLiBNYWtlIHN1cmUgdGhlIGlucHV0IGRhdGEgaXMgdmFsaWQuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVkFMSURBVElPTl9FUlJPUlwiXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PSA0MDEpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFsZXJ0c01hbmFnZXIuc2hvd0RhbmdlcihcclxuICAgICAgICAgICAgICAgICAgICAgICAgJChcImRpdiNhbGVydHNfY29udGFpbmVyXCIpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIllvdXIgc2Vzc2lvbiBoYXMgZXhwaXJlZC4gUGxlYXNlIGNsb3NlIHRoaXMgd2luZG93IGFuZCB0cnkgdG8gb3BlbiBpdCBhZ2FpbiBmcm9tIHRoZSBTcHJlYWRzaGVldC5cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJTRVNTSU9OX0VYUElSRURcIlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5pc0xvYWRpbmdSZXF1ZXN0ID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICB9KS5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWxlcnRzTWFuYWdlci5zaG93RGFuZ2VyKFxyXG4gICAgICAgICAgICAgICAgICAgICQoXCJkaXYjYWxlcnRzX2NvbnRhaW5lclwiKSxcclxuICAgICAgICAgICAgICAgICAgICBcIlRoZXJlIGhhcyBiZWVuIGEgbmV0d29yayBvciBzZXJ2ZXIgZXJyb3IuIFBsZWFzZSB0cnkgYWdhaW4gbGF0ZXIuIFwiICsgZXJyb3IsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJORVRXT1JLX0VSUk9SXCJcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZ1JlcXVlc3QgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxufTtcclxuPC9zY3JpcHQ+XHJcbiIsIjx0ZW1wbGF0ZT5cclxuICAgIDxkaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBtYW5hZ2UtY29yZXMtcm93XCI+XHJcbiAgICAgICAgICAgIDxmb3JtIHYtYmluZDppZD1cImNvcmUuY29kZSArICdfZm9ybSdcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNFwiPnt7IGNvcmUuZGVzY3JpcHRpb24gfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwibnVtYmVyXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBwbGFjZWhvbGRlcj1cIkhvdyBtYW55IHBhcnRzP1wiIG1pbj1cIjBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2LWJpbmQ6bmFtZT1cImNvcmUuY29kZSArICdfcXVhbnRpdHknXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdi1tb2RlbD1cImNvcmUuZGVmYXVsdFF1YW50aXR5XCIgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJudW1iZXJcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHBsYWNlaG9sZGVyPVwiSG93IG11Y2ggZm9yIGVhY2g/XCIgbWluPVwiMFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHYtYmluZDpuYW1lPVwiY29yZS5jb2RlICsgJ19wcmljZV9wZXJfdW5pdCdcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiY29yZS5kZWZhdWx0UHJpY2VQZXJVbml0XCIgLz5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Zvcm0+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTJcIj5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XCJidG4gYnRuLXByaW1hcnlcIiB2LW9uOmNsaWNrPVwic2F2ZVwiIHYtYmluZDpkaXNhYmxlZD1cImlzTG9hZGluZ1JlcXVlc3RcIj5TYXZlPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IHYtc2hvdz1cImlzTG9hZGluZ1JlcXVlc3RcIiBjbGFzcz1cInN1Ym1pdC1idXR0b24tbG9hZGluZy1jb250YWluZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cImltZy9sb2FkaW5nX3NtYWxsLmdpZlwiIGFsdD1cIkxvYWRpbmcuLi5cIj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwicm93IG1hbmFnZS1jb3JlLXJvdy1lcnJvci1jb250YWluZXJcIiB2LXNob3c9XCIhaXNWYWxpZFwiPlxyXG4gICAgICAgICAgICA8ZGl2IHYtYmluZDppZD1cImNvcmUuY29kZSArICdfdmFsaWRhdGlvbl9lcnJvcl9jb250YWluZXInXCIgY2xhc3M9XCJhbGVydCBhbGVydC1kYW5nZXJcIj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiB2LWJpbmQ6aWQ9XCJjb3JlLmNvZGUgKyAnX2FsZXJ0c19jb250YWluZXInXCIgY2xhc3M9XCJyb3cgbWFuYWdlLWNvcmUtcm93LWVycm9yLWNvbnRhaW5lclwiID5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuaW1wb3J0ICQgZnJvbSBcImpxdWVyeVwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cclxuICAgIGluamVjdDogW1wiYWxlcnRzTWFuYWdlclwiLCBcImFwaUNsaWVudFwiLCBcInZhbGlkYXRvckZhY3RvcnlcIl0sXHJcblxyXG4gICAgcHJvcHM6IHtcclxuICAgICAgICBjb3JlVG9FZGl0OiBPYmplY3RcclxuICAgIH0sXHJcblxyXG4gICAgZGF0YTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGlzTG9hZGluZ1JlcXVlc3Q6IGZhbHNlLFxyXG4gICAgICAgICAgICBpc1ZhbGlkOiB0cnVlLFxyXG4gICAgICAgICAgICBpc1NlcnZlckVycm9yOiBmYWxzZSxcclxuXHJcbiAgICAgICAgICAgIGNvcmU6IHtcclxuICAgICAgICAgICAgICAgIGlkOiB0aGlzLmNvcmVUb0VkaXQuaWQsXHJcbiAgICAgICAgICAgICAgICBjb2RlOiB0aGlzLmNvcmVUb0VkaXQuY29kZS50b0xvd2VyQ2FzZSgpLFxyXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IHRoaXMuY29yZVRvRWRpdC5kZXNjcmlwdGlvbixcclxuICAgICAgICAgICAgICAgIGRlZmF1bHRRdWFudGl0eTogdGhpcy5jb3JlVG9FZGl0LmRlZmF1bHRRdWFudGl0eSxcclxuICAgICAgICAgICAgICAgIGRlZmF1bHRQcmljZVBlclVuaXQ6IHRoaXMuY29yZVRvRWRpdC5kZWZhdWx0UHJpY2VQZXJVbml0LFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgbWV0aG9kczoge1xyXG4gICAgICAgIHNhdmU6IGZ1bmN0aW9uICgvKmV2ZW50Ki8pIHtcclxuXHJcbiAgICAgICAgICAgIGxldCB2YWxpZGF0b3IgPSB0aGlzLnZhbGlkYXRvckZhY3RvcnkubWFrZVZhbGlkYXRvcihcclxuICAgICAgICAgICAgICAgIHRoaXMuY29yZS5jb2RlLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5jb3JlLmRlc2NyaXB0aW9uLFxyXG4gICAgICAgICAgICAgICAgYGZvcm0jJHsgdGhpcy5jb3JlLmNvZGUgfV9mb3JtYCxcclxuICAgICAgICAgICAgICAgIGBkaXYjJHsgdGhpcy5jb3JlLmNvZGUgfV92YWxpZGF0aW9uX2Vycm9yX2NvbnRhaW5lcmBcclxuICAgICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuaXNWYWxpZCA9IHZhbGlkYXRvci5pc1ZhbGlkKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNWYWxpZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgY29yZURhdGEgPSB0aGlzLmNvcmU7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmlzTG9hZGluZ1JlcXVlc3QgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5hcGlDbGllbnQudXBkYXRlVmVoaWNsZUNvcmUoY29yZURhdGEpLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5zdGF0dXMgPT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hbGVydHNNYW5hZ2VyLnNob3dTdWNjZXNzKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKGBkaXYjJHsgdGhpcy5jb3JlLmNvZGUgfV9hbGVydHNfY29udGFpbmVyYCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVGhlIGNvcmUncyBpbmZvcm1hdGlvbiBoYXMgYmVlbiBzYXZlZCBzdWNjZXNzZnVsbHkuXCJcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAocmVzcG9uc2Uuc3RhdHVzID09IDQwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWxlcnRzTWFuYWdlci5zaG93RGFuZ2VyKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKGBkaXYjJHsgdGhpcy5jb3JlLmNvZGUgfV9hbGVydHNfY29udGFpbmVyYCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVGhlcmUgaGFzIGJlZW4gYSB2YWxpZGF0aW9uIGVycm9yLiBNYWtlIHN1cmUgdGhlIGlucHV0IGRhdGEgaXMgdmFsaWQuXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiVkFMSURBVElPTl9FUlJPUlwiXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PSA0MDEpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFsZXJ0c01hbmFnZXIuc2hvd0RhbmdlcihcclxuICAgICAgICAgICAgICAgICAgICAgICAgJChgZGl2IyR7IHRoaXMuY29yZS5jb2RlIH1fYWxlcnRzX2NvbnRhaW5lcmApLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIllvdXIgc2Vzc2lvbiBoYXMgZXhwaXJlZC4gUGxlYXNlIGNsb3NlIHRoaXMgd2luZG93IGFuZCB0cnkgdG8gb3BlbiBpdCBhZ2FpbiBmcm9tIHRoZSBBY2NvdW50IE1hbmFnZXIgcGFnZS5cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJTRVNTSU9OX0VYUElSRURcIlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5pc0xvYWRpbmdSZXF1ZXN0ID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICB9KS5jYXRjaCgoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWxlcnRzTWFuYWdlci5zaG93RGFuZ2VyKFxyXG4gICAgICAgICAgICAgICAgICAgICQoYGRpdiMkeyB0aGlzLmNvcmUuY29kZSB9X2FsZXJ0c19jb250YWluZXJgKSxcclxuICAgICAgICAgICAgICAgICAgICBcIlRoZXJlIGhhcyBiZWVuIGEgbmV0d29yayBvciBzZXJ2ZXIgZXJyb3IuIFBsZWFzZSB0cnkgYWdhaW4gbGF0ZXIuIFwiICsgZXJyb3IsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJORVRXT1JLX0VSUk9SXCJcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZ1JlcXVlc3QgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59O1xyXG48L3NjcmlwdD5cclxuXHJcbiIsIjx0ZW1wbGF0ZT5cclxuICAgIDxkaXY+XHJcbiAgICAgICAgPHRlbXBsYXRlIHYtaWY9XCJpc0luaXRFcnJvclwiPlxyXG4gICAgICAgICAgICA8ZGl2IGlkPVwiaW5pdF9hbGVydHNfY29udGFpbmVyXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiYWxlcnQgYWxlcnQtZGFuZ2VyIGFsZXJ0LWRpc21pc3NhYmxlIGNlbnRlcmVkLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHt7IGluaXRFcnJvck1lc3NhZ2UgfX1cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L3RlbXBsYXRlPlxyXG4gICAgICAgIDx0ZW1wbGF0ZSB2LWVsc2U+XHJcblxyXG4gICAgICAgICAgICA8aDI+TWFuYWdlIFZlaGljbGUgQ29yZXM8L2gyPlxyXG5cclxuICAgICAgICAgICAgPGhyPlxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJyb3cgbWFuYWdlLWNvcmVzLWhlYWRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNFwiPkNvcmU8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTNcIj5EZWZhdWx0IFF1YW50aXR5PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC0zXCI+RGVmYXVsdCBQcmljZSBQZXIgVW5pdDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtMlwiPjwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8dGVtcGxhdGUgdi1mb3I9XCJjb3JlIGluIGNvcmVzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPEVkaXRWZWhpY2xlQ29yZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2LWJpbmQ6a2V5PVwiY29yZS5jb2RlXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdi1iaW5kOmNvcmVUb0VkaXQ9XCJjb3JlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9FZGl0VmVoaWNsZUNvcmU+XHJcbiAgICAgICAgICAgICAgICA8L3RlbXBsYXRlPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgIDxocj5cclxuXHJcbiAgICAgICAgPC90ZW1wbGF0ZT5cclxuICAgIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuaW1wb3J0ICQgZnJvbSBcImpxdWVyeVwiO1xyXG5cclxuaW1wb3J0IEVkaXRWZWhpY2xlQ29yZSBmcm9tIFwiLi9FZGl0VmVoaWNsZUNvcmUudnVlXCI7XHJcblxyXG5leHBvcnQgZGVmYXVsdCB7XHJcblxyXG4gICAgY29tcG9uZW50czoge1xyXG4gICAgICAgIEVkaXRWZWhpY2xlQ29yZVxyXG4gICAgfSxcclxuXHJcbiAgICBpbmplY3Q6IFtcImFwaUNsaWVudFwiLCBcIm9uSW5pdFwiXSxcclxuXHJcbiAgICBkYXRhOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgaXNJbml0RXJyb3I6IGZhbHNlLFxyXG4gICAgICAgICAgICBpbml0RXJyb3JNZXNzYWdlOiBcIlwiLFxyXG4gICAgICAgICAgICBpc0xvYWRpbmdSZXF1ZXN0OiBmYWxzZSxcclxuICAgICAgICAgICAgY29yZXM6IFtdLFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cclxuICAgIGNyZWF0ZWQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB0aGlzLmFwaUNsaWVudC5nZXRDb25maWd1cmF0aW9uRGF0YSgpXHJcbiAgICAgICAgICAgIC50aGVuKHJlc3BvbnNlID0+IHtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09IDIwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29yZXMgPSByZXNwb25zZS5jb3JlcztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNJbml0RXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaW5pdEVycm9yTWVzc2FnZSA9IFwiQ291bGQgbm90IHJldHJpZXZlIGNvbmZpZ3VyYXRpb24gZGF0YS4gUGxlYXNlIHRyeSBhZ2FpbiBsYXRlci5cIjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uSW5pdCgpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuaXNJbml0RXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbml0RXJyb3JNZXNzYWdlID0gXCJUaGVyZSBoYXMgYmVlbiBhIG5ldHdvcmsgb3Igc2VydmVyIGVycm9yLiBQbGVhc2UgdHJ5IGFnYWluIGxhdGVyLiBcIiArIGVycm9yO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMub25Jbml0KCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfSxcclxuXHJcbiAgICBtb3VudGVkOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgJChcImRpdi52YWxpZGF0aW9uLWVycm9yLWFsZXJ0IGEuY2xvc2VcIikuY2xpY2soZnVuY3Rpb24oZXZlbnRPYmplY3QpIHtcclxuICAgICAgICAgICAgJChldmVudE9iamVjdC50YXJnZXQucGFyZW50RWxlbWVudCkuaGlkZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59O1xyXG48L3NjcmlwdD5cclxuIiwiPHRlbXBsYXRlPlxyXG4gICAgPGRpdj5cclxuICAgICAgICA8aDQ+e3sgY29yZS5kZXNjcmlwdGlvbiB9fTwvaDQ+XHJcblxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJyb3cgY2FwdHVyZS1kZWFsLXZlaGljbGUtY29yZS1jb250YWluZXJcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0zXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCB2LWJpbmQ6Zm9yPVwiY29yZS5jb2RlICsgJ19pc19leHRyYWN0ZWQnXCI+RXh0cmFjdGVkPzwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwiY2hlY2tib3gtaW5saW5lXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cInJhZGlvXCIgdmFsdWU9XCJ5ZXNcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHYtYmluZDpuYW1lPVwiY29yZS5jb2RlICsgJ19pc19leHRyYWN0ZWQnXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiaXNFeHRyYWN0ZWRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHYtb246Y2xpY2s9XCJpc0V4dHJhY3RlZFllc09uQ2xpY2tcIj4gWWVzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cImNoZWNrYm94LWlubGluZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IHR5cGU9XCJyYWRpb1wiIHZhbHVlPVwibm9cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHYtYmluZDpuYW1lPVwiY29yZS5jb2RlICsgJ19pc19leHRyYWN0ZWQnXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2LW1vZGVsPVwiaXNFeHRyYWN0ZWRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHYtb246Y2xpY2s9XCJpc0V4dHJhY3RlZE5vT25DbGlja1wiPiBOb1xyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPHRyYW5zaXRpb24gbmFtZT1cInNsaWRlLWZhZGVcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgdi1zaG93PVwiaXNFeHRyYWN0ZWQgPT0gJ3llcydcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXBcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCB2LWJpbmQ6Zm9yPVwiY29yZS5jb2RlICsgJ19xdWFudGl0eSdcIj5RdWFudGl0eTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgdHlwZT1cIm51bWJlclwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgcGxhY2Vob2xkZXI9XCJIb3cgbWFueSBwYXJ0cz9cIiBtaW49XCIwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2LWJpbmQ6bmFtZT1cImNvcmUuY29kZSArICdfcXVhbnRpdHknXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2LWJpbmQ6dmFsdWU9XCJjb3JlLnF1YW50aXR5XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2LW9uOmlucHV0PVwicXVhbnRpdHlPbklucHV0XCIgLz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIHYtYmluZDpmb3I9XCJjb3JlLmNvZGUgKyAnX3ByaWNlX3Blcl91bml0J1wiPlByaWNlIHBlciB1bml0PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj4kPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwibnVtYmVyXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2xcIiBwbGFjZWhvbGRlcj1cIkhvdyBtdWNoIGZvciBlYWNoP1wiIG1pbj1cIjBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2LWJpbmQ6bmFtZT1cImNvcmUuY29kZSArICdfcHJpY2VfcGVyX3VuaXQnXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdi1iaW5kOnZhbHVlPVwiY29yZS5wcmljZVBlclVuaXRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2LW9uOmlucHV0PVwicHJpY2VQZXJVbml0T25JbnB1dFwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIHYtYmluZDpmb3I9XCJjb3JlLmNvZGUgKyAnX3RvdGFsX3ByaWNlJ1wiPlRvdGFsPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJpbnB1dC1ncm91cFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiaW5wdXQtZ3JvdXAtYWRkb25cIj4kPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCByZWFkb25seSB0eXBlPVwidGV4dFwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgcGxhY2Vob2xkZXI9XCJUb3RhbCBwcmljZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHYtYmluZDpuYW1lPVwiY29yZS5jb2RlICsgJ190b3RhbF9wcmljZSdcIiB2LWJpbmQ6dmFsdWU9XCJ0b3RhbFByaWNlRGlzcGxheVwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC90cmFuc2l0aW9uPlxyXG4gICAgICAgIDwvZGl2PlxyXG5cclxuICAgIDwvZGl2PlxyXG48L3RlbXBsYXRlPlxyXG5cclxuPHNjcmlwdD5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG5cclxuICAgIHByb3BzOiB7XHJcbiAgICAgICAgY29yZTogeyB0eXBlOiBPYmplY3QgfVxyXG4gICAgfSxcclxuXHJcbiAgICBkYXRhOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgaXNFeHRyYWN0ZWQ6IHRoaXMuY29yZS5pc0V4dHJhY3RlZCA/IFwieWVzXCIgOiBcIm5vXCJcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBjb21wdXRlZDoge1xyXG4gICAgICAgIHRvdGFsUHJpY2U6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgbGV0IG5ld1RvdGFsUHJpY2UgPSAwLjAwO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuY29yZS5xdWFudGl0eSAmJiB0aGlzLmNvcmUucHJpY2VQZXJVbml0KSB7XHJcbiAgICAgICAgICAgICAgICBuZXdUb3RhbFByaWNlID0gdGhpcy5jb3JlLnF1YW50aXR5ICogdGhpcy5jb3JlLnByaWNlUGVyVW5pdDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy4kZW1pdChcImNvbXB1dGVkLXRvdGFsLXByaWNlXCIsIG5ld1RvdGFsUHJpY2UpO1xyXG4gICAgICAgICAgICByZXR1cm4gbmV3VG90YWxQcmljZTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICB0b3RhbFByaWNlRGlzcGxheTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy50b3RhbFByaWNlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gTnVtYmVyKHRoaXMudG90YWxQcmljZSkudG9GaXhlZCgyKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIFwiMC4wMFwiO1xyXG4gICAgICAgIH0sXHJcbiAgICB9LFxyXG5cclxuICAgIG1ldGhvZHM6IHtcclxuICAgICAgICBpc0V4dHJhY3RlZFllc09uQ2xpY2s6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdGhpcy4kZW1pdChcImNoYW5nZS1pcy1leHRyYWN0ZWRcIiwgdHJ1ZSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBpc0V4dHJhY3RlZE5vT25DbGljazogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB0aGlzLiRlbWl0KFwiY2hhbmdlLWlzLWV4dHJhY3RlZFwiLCBmYWxzZSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBxdWFudGl0eU9uSW5wdXQ6IGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgICAgICB0aGlzLiRlbWl0KFwiaW5wdXQtcXVhbnRpdHlcIiwgcGFyc2VJbnQoZXZlbnQudGFyZ2V0LnZhbHVlKSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBwcmljZVBlclVuaXRPbklucHV0OiBmdW5jdGlvbiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy4kZW1pdChcImlucHV0LXByaWNlLXBlci11bml0XCIsIHBhcnNlRmxvYXQoZXZlbnQudGFyZ2V0LnZhbHVlKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59O1xyXG48L3NjcmlwdD5cclxuIiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcbiAgICBcImRpdlwiLFxuICAgIFtcbiAgICAgIF92bS5pc0luaXRFcnJvclxuICAgICAgICA/IFtcbiAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgYXR0cnM6IHsgaWQ6IFwiaW5pdF9hbGVydHNfY29udGFpbmVyXCIgfSB9LCBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6XG4gICAgICAgICAgICAgICAgICAgIFwiYWxlcnQgYWxlcnQtZGFuZ2VyIGFsZXJ0LWRpc21pc3NhYmxlIGNlbnRlcmVkLWNvbnRhaW5lclwiXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXG4gICAgICAgICAgICAgICAgICAgIFwiXFxuICAgICAgICAgICAgICAgIFwiICtcbiAgICAgICAgICAgICAgICAgICAgICBfdm0uX3MoX3ZtLmluaXRFcnJvck1lc3NhZ2UpICtcbiAgICAgICAgICAgICAgICAgICAgICBcIlxcbiAgICAgICAgICAgIFwiXG4gICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF1cbiAgICAgICAgOiBbXG4gICAgICAgICAgICBfYyhcImgyXCIsIFtcbiAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgIFwiRW50ZXIgdGhlIHZlaGljbGUncyBleHRyYWN0ZWQgY29yZXMgZm9yIGRlYWwgXCIgK1xuICAgICAgICAgICAgICAgICAgX3ZtLl9zKF92bS5kZWFsSWQpXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFwiaHJcIiksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJ0b3RhbC1jb3JlLXZhbHVlLWNvbnRhaW5lclwiIH0sIFtcbiAgICAgICAgICAgICAgX3ZtLl92KFwiXFxuICAgICAgICAgICAgVG90YWwgVmFsdWU6IFwiKSxcbiAgICAgICAgICAgICAgX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwibGFiZWwgbGFiZWwtc3VjY2Vzc1wiIH0sIFtcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIkXCIgKyBfdm0uX3MoX3ZtLnRvdGFsVmFsdWUpKVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXSksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJoclwiKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgXCJmb3JtXCIsXG4gICAgICAgICAgICAgIHsgYXR0cnM6IHsgaWQ6IFwiY29yZXNfZm9ybVwiIH0gfSxcbiAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgIF92bS5fbChfdm0uY29yZXMsIGZ1bmN0aW9uKGNvcmUpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAgICAgICAgIF9jKFwiVmVoaWNsZUNvcmVcIiwge1xuICAgICAgICAgICAgICAgICAgICAgIGtleTogY29yZS5jb2RlLFxuICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGNvcmU6IGNvcmUgfSxcbiAgICAgICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgXCJjaGFuZ2UtaXMtZXh0cmFjdGVkXCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjb3JlLmlzRXh0cmFjdGVkID0gJGV2ZW50XG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJpbnB1dC1wcmljZS1wZXItdW5pdFwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY29yZS5wcmljZVBlclVuaXQgPSAkZXZlbnRcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICBcImlucHV0LXF1YW50aXR5XCI6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjb3JlLnF1YW50aXR5ID0gJGV2ZW50XG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJjb21wdXRlZC10b3RhbC1wcmljZVwiOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY29yZS50b3RhbFByaWNlID0gJGV2ZW50XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgIDJcbiAgICAgICAgICAgICksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJoclwiKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IGF0dHJzOiB7IGlkOiBcImFsZXJ0c19jb250YWluZXJcIiB9IH0pLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF92bS5fbSgwKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImRpdlwiLCBbXG4gICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgIFwiYnV0dG9uXCIsXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiYnRuIGJ0bi1wcmltYXJ5IGJ0bi1sZ1wiLFxuICAgICAgICAgICAgICAgICAgYXR0cnM6IHsgZGlzYWJsZWQ6IF92bS5pc0xvYWRpbmdSZXF1ZXN0IH0sXG4gICAgICAgICAgICAgICAgICBvbjogeyBjbGljazogX3ZtLnN1Ym1pdCB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBbX3ZtLl92KFwiU2F2ZVwiKV1cbiAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgICAgICAgICAgICAgICAgICByYXdOYW1lOiBcInYtc2hvd1wiLFxuICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBfdm0uaXNMb2FkaW5nUmVxdWVzdCxcbiAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImlzTG9hZGluZ1JlcXVlc3RcIlxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwic3VibWl0LWJ1dHRvbi1sb2FkaW5nLWNvbnRhaW5lclwiXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgICBfYyhcImltZ1wiLCB7XG4gICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IHNyYzogXCJpbWcvbG9hZGluZ19zbWFsbC5naWZcIiwgYWx0OiBcIkxvYWRpbmcuLi5cIiB9XG4gICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgX2MoXG4gICAgICAgICAgICAgICAgXCJkaXZcIixcbiAgICAgICAgICAgICAgICB7IHN0YXRpY0NsYXNzOiBcInRvdGFsLWNvcmUtdmFsdWUtY29udGFpbmVyIHB1bGwtcmlnaHRcIiB9LFxuICAgICAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIlxcbiAgICAgICAgICAgICAgICBUb3RhbCBWYWx1ZTogXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwibGFiZWwgbGFiZWwtc3VjY2Vzc1wiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiJFwiICsgX3ZtLl9zKF92bS50b3RhbFZhbHVlKSlcbiAgICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF1cbiAgICBdLFxuICAgIDJcbiAgKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXG4gICAgICBcImRpdlwiLFxuICAgICAge1xuICAgICAgICBzdGF0aWNDbGFzczpcbiAgICAgICAgICBcInZhbGlkYXRpb24tZXJyb3ItYWxlcnQgYWxlcnQgYWxlcnQtZGFuZ2VyIGFsZXJ0LWRpc21pc3NhYmxlIGZhZGUgaW5cIixcbiAgICAgICAgYXR0cnM6IHsgaWQ6IFwidmFsaWRhdGlvbl9lcnJvcl9jb250YWluZXJcIiB9XG4gICAgICB9LFxuICAgICAgW1xuICAgICAgICBfYyhcImFcIiwgeyBzdGF0aWNDbGFzczogXCJjbG9zZVwiLCBhdHRyczogeyBcImFyaWEtbGFiZWxcIjogXCJjbG9zZVwiIH0gfSwgW1xuICAgICAgICAgIF92bS5fdihcIsOXXCIpXG4gICAgICAgIF0pXG4gICAgICBdXG4gICAgKVxuICB9XG5dXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXCJkaXZcIiwgW1xuICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwicm93IG1hbmFnZS1jb3Jlcy1yb3dcIiB9LCBbXG4gICAgICBfYyhcImZvcm1cIiwgeyBhdHRyczogeyBpZDogX3ZtLmNvcmUuY29kZSArIFwiX2Zvcm1cIiB9IH0sIFtcbiAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJjb2wtbWQtNFwiIH0sIFtcbiAgICAgICAgICBfdm0uX3YoX3ZtLl9zKF92bS5jb3JlLmRlc2NyaXB0aW9uKSlcbiAgICAgICAgXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTNcIiB9LCBbXG4gICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgdmFsdWU6IF92bS5jb3JlLmRlZmF1bHRRdWFudGl0eSxcbiAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImNvcmUuZGVmYXVsdFF1YW50aXR5XCJcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImZvcm0tY29udHJvbFwiLFxuICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgdHlwZTogXCJudW1iZXJcIixcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IFwiSG93IG1hbnkgcGFydHM/XCIsXG4gICAgICAgICAgICAgIG1pbjogXCIwXCIsXG4gICAgICAgICAgICAgIG5hbWU6IF92bS5jb3JlLmNvZGUgKyBcIl9xdWFudGl0eVwiXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZG9tUHJvcHM6IHsgdmFsdWU6IF92bS5jb3JlLmRlZmF1bHRRdWFudGl0eSB9LFxuICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgaW5wdXQ6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF92bS4kc2V0KF92bS5jb3JlLCBcImRlZmF1bHRRdWFudGl0eVwiLCAkZXZlbnQudGFyZ2V0LnZhbHVlKVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSlcbiAgICAgICAgXSksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTNcIiB9LCBbXG4gICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgdmFsdWU6IF92bS5jb3JlLmRlZmF1bHRQcmljZVBlclVuaXQsXG4gICAgICAgICAgICAgICAgZXhwcmVzc2lvbjogXCJjb3JlLmRlZmF1bHRQcmljZVBlclVuaXRcIlxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICBhdHRyczoge1xuICAgICAgICAgICAgICB0eXBlOiBcIm51bWJlclwiLFxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogXCJIb3cgbXVjaCBmb3IgZWFjaD9cIixcbiAgICAgICAgICAgICAgbWluOiBcIjBcIixcbiAgICAgICAgICAgICAgbmFtZTogX3ZtLmNvcmUuY29kZSArIFwiX3ByaWNlX3Blcl91bml0XCJcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBkb21Qcm9wczogeyB2YWx1ZTogX3ZtLmNvcmUuZGVmYXVsdFByaWNlUGVyVW5pdCB9LFxuICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgaW5wdXQ6IGZ1bmN0aW9uKCRldmVudCkge1xuICAgICAgICAgICAgICAgIGlmICgkZXZlbnQudGFyZ2V0LmNvbXBvc2luZykge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF92bS4kc2V0KF92bS5jb3JlLCBcImRlZmF1bHRQcmljZVBlclVuaXRcIiwgJGV2ZW50LnRhcmdldC52YWx1ZSlcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pXG4gICAgICAgIF0pXG4gICAgICBdKSxcbiAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1tZC0yXCIgfSwgW1xuICAgICAgICBfYyhcbiAgICAgICAgICBcImJ1dHRvblwiLFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcImJ0biBidG4tcHJpbWFyeVwiLFxuICAgICAgICAgICAgYXR0cnM6IHsgZGlzYWJsZWQ6IF92bS5pc0xvYWRpbmdSZXF1ZXN0IH0sXG4gICAgICAgICAgICBvbjogeyBjbGljazogX3ZtLnNhdmUgfVxuICAgICAgICAgIH0sXG4gICAgICAgICAgW192bS5fdihcIlNhdmVcIildXG4gICAgICAgICksXG4gICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgIF9jKFxuICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAge1xuICAgICAgICAgICAgZGlyZWN0aXZlczogW1xuICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgbmFtZTogXCJzaG93XCIsXG4gICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LXNob3dcIixcbiAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmlzTG9hZGluZ1JlcXVlc3QsXG4gICAgICAgICAgICAgICAgZXhwcmVzc2lvbjogXCJpc0xvYWRpbmdSZXF1ZXN0XCJcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIHN0YXRpY0NsYXNzOiBcInN1Ym1pdC1idXR0b24tbG9hZGluZy1jb250YWluZXJcIlxuICAgICAgICAgIH0sXG4gICAgICAgICAgW1xuICAgICAgICAgICAgX2MoXCJpbWdcIiwge1xuICAgICAgICAgICAgICBhdHRyczogeyBzcmM6IFwiaW1nL2xvYWRpbmdfc21hbGwuZ2lmXCIsIGFsdDogXCJMb2FkaW5nLi4uXCIgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICBdXG4gICAgICAgIClcbiAgICAgIF0pXG4gICAgXSksXG4gICAgX3ZtLl92KFwiIFwiKSxcbiAgICBfYyhcbiAgICAgIFwiZGl2XCIsXG4gICAgICB7XG4gICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICB2YWx1ZTogIV92bS5pc1ZhbGlkLFxuICAgICAgICAgICAgZXhwcmVzc2lvbjogXCIhaXNWYWxpZFwiXG4gICAgICAgICAgfVxuICAgICAgICBdLFxuICAgICAgICBzdGF0aWNDbGFzczogXCJyb3cgbWFuYWdlLWNvcmUtcm93LWVycm9yLWNvbnRhaW5lclwiXG4gICAgICB9LFxuICAgICAgW1xuICAgICAgICBfYyhcImRpdlwiLCB7XG4gICAgICAgICAgc3RhdGljQ2xhc3M6IFwiYWxlcnQgYWxlcnQtZGFuZ2VyXCIsXG4gICAgICAgICAgYXR0cnM6IHsgaWQ6IF92bS5jb3JlLmNvZGUgKyBcIl92YWxpZGF0aW9uX2Vycm9yX2NvbnRhaW5lclwiIH1cbiAgICAgICAgfSlcbiAgICAgIF1cbiAgICApLFxuICAgIF92bS5fdihcIiBcIiksXG4gICAgX2MoXCJkaXZcIiwge1xuICAgICAgc3RhdGljQ2xhc3M6IFwicm93IG1hbmFnZS1jb3JlLXJvdy1lcnJvci1jb250YWluZXJcIixcbiAgICAgIGF0dHJzOiB7IGlkOiBfdm0uY29yZS5jb2RlICsgXCJfYWxlcnRzX2NvbnRhaW5lclwiIH1cbiAgICB9KVxuICBdKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtdXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsInZhciByZW5kZXIgPSBmdW5jdGlvbigpIHtcbiAgdmFyIF92bSA9IHRoaXNcbiAgdmFyIF9oID0gX3ZtLiRjcmVhdGVFbGVtZW50XG4gIHZhciBfYyA9IF92bS5fc2VsZi5fYyB8fCBfaFxuICByZXR1cm4gX2MoXG4gICAgXCJkaXZcIixcbiAgICBbXG4gICAgICBfdm0uaXNJbml0RXJyb3JcbiAgICAgICAgPyBbXG4gICAgICAgICAgICBfYyhcImRpdlwiLCB7IGF0dHJzOiB7IGlkOiBcImluaXRfYWxlcnRzX2NvbnRhaW5lclwiIH0gfSwgW1xuICAgICAgICAgICAgICBfYyhcbiAgICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgIHN0YXRpY0NsYXNzOlxuICAgICAgICAgICAgICAgICAgICBcImFsZXJ0IGFsZXJ0LWRhbmdlciBhbGVydC1kaXNtaXNzYWJsZSBjZW50ZXJlZC1jb250YWluZXJcIlxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgICAgX3ZtLl92KFxuICAgICAgICAgICAgICAgICAgICBcIlxcbiAgICAgICAgICAgICAgICBcIiArXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl9zKF92bS5pbml0RXJyb3JNZXNzYWdlKSArXG4gICAgICAgICAgICAgICAgICAgICAgXCJcXG4gICAgICAgICAgICBcIlxuICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgXSlcbiAgICAgICAgICBdXG4gICAgICAgIDogW1xuICAgICAgICAgICAgX2MoXCJoMlwiLCBbX3ZtLl92KFwiTWFuYWdlIFZlaGljbGUgQ29yZXNcIildKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImhyXCIpLFxuICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICBcImRpdlwiLFxuICAgICAgICAgICAgICBbXG4gICAgICAgICAgICAgICAgX3ZtLl9tKDApLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgX3ZtLl9sKF92bS5jb3JlcywgZnVuY3Rpb24oY29yZSkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJFZGl0VmVoaWNsZUNvcmVcIiwge1xuICAgICAgICAgICAgICAgICAgICAgIGtleTogY29yZS5jb2RlLFxuICAgICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7IGNvcmVUb0VkaXQ6IGNvcmUgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgIDJcbiAgICAgICAgICAgICksXG4gICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgX2MoXCJoclwiKVxuICAgICAgICAgIF1cbiAgICBdLFxuICAgIDJcbiAgKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtcbiAgZnVuY3Rpb24oKSB7XG4gICAgdmFyIF92bSA9IHRoaXNcbiAgICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgICB2YXIgX2MgPSBfdm0uX3NlbGYuX2MgfHwgX2hcbiAgICByZXR1cm4gX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJyb3cgbWFuYWdlLWNvcmVzLWhlYWRlclwiIH0sIFtcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTRcIiB9LCBbX3ZtLl92KFwiQ29yZVwiKV0pLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTNcIiB9LCBbX3ZtLl92KFwiRGVmYXVsdCBRdWFudGl0eVwiKV0pLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTNcIiB9LCBbXG4gICAgICAgIF92bS5fdihcIkRlZmF1bHQgUHJpY2UgUGVyIFVuaXRcIilcbiAgICAgIF0pLFxuICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLW1kLTJcIiB9KVxuICAgIF0pXG4gIH1cbl1cbnJlbmRlci5fd2l0aFN0cmlwcGVkID0gdHJ1ZVxuXG5leHBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IiwidmFyIHJlbmRlciA9IGZ1bmN0aW9uKCkge1xuICB2YXIgX3ZtID0gdGhpc1xuICB2YXIgX2ggPSBfdm0uJGNyZWF0ZUVsZW1lbnRcbiAgdmFyIF9jID0gX3ZtLl9zZWxmLl9jIHx8IF9oXG4gIHJldHVybiBfYyhcImRpdlwiLCBbXG4gICAgX2MoXCJoNFwiLCBbX3ZtLl92KF92bS5fcyhfdm0uY29yZS5kZXNjcmlwdGlvbikpXSksXG4gICAgX3ZtLl92KFwiIFwiKSxcbiAgICBfYyhcbiAgICAgIFwiZGl2XCIsXG4gICAgICB7IHN0YXRpY0NsYXNzOiBcInJvdyBjYXB0dXJlLWRlYWwtdmVoaWNsZS1jb3JlLWNvbnRhaW5lclwiIH0sXG4gICAgICBbXG4gICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTNcIiB9LCBbXG4gICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIgfSwgW1xuICAgICAgICAgICAgX2MoXCJsYWJlbFwiLCB7IGF0dHJzOiB7IGZvcjogX3ZtLmNvcmUuY29kZSArIFwiX2lzX2V4dHJhY3RlZFwiIH0gfSwgW1xuICAgICAgICAgICAgICBfdm0uX3YoXCJFeHRyYWN0ZWQ/XCIpXG4gICAgICAgICAgICBdKSxcbiAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICBfYyhcImRpdlwiLCBbXG4gICAgICAgICAgICAgIF9jKFwibGFiZWxcIiwgeyBzdGF0aWNDbGFzczogXCJjaGVja2JveC1pbmxpbmVcIiB9LCBbXG4gICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5pc0V4dHJhY3RlZCxcbiAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImlzRXh0cmFjdGVkXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IFwicmFkaW9cIixcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IFwieWVzXCIsXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IF92bS5jb3JlLmNvZGUgKyBcIl9pc19leHRyYWN0ZWRcIlxuICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIGRvbVByb3BzOiB7IGNoZWNrZWQ6IF92bS5fcShfdm0uaXNFeHRyYWN0ZWQsIFwieWVzXCIpIH0sXG4gICAgICAgICAgICAgICAgICBvbjoge1xuICAgICAgICAgICAgICAgICAgICBjbGljazogX3ZtLmlzRXh0cmFjdGVkWWVzT25DbGljayxcbiAgICAgICAgICAgICAgICAgICAgY2hhbmdlOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICBfdm0uaXNFeHRyYWN0ZWQgPSBcInllc1wiXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICBfdm0uX3YoXCIgWWVzXFxuICAgICAgICAgICAgICAgICAgICBcIilcbiAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgIF9jKFwibGFiZWxcIiwgeyBzdGF0aWNDbGFzczogXCJjaGVja2JveC1pbmxpbmVcIiB9LCBbXG4gICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICBkaXJlY3RpdmVzOiBbXG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBcIm1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgcmF3TmFtZTogXCJ2LW1vZGVsXCIsXG4gICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IF92bS5pc0V4dHJhY3RlZCxcbiAgICAgICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImlzRXh0cmFjdGVkXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IFwicmFkaW9cIixcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IFwibm9cIixcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogX3ZtLmNvcmUuY29kZSArIFwiX2lzX2V4dHJhY3RlZFwiXG4gICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgZG9tUHJvcHM6IHsgY2hlY2tlZDogX3ZtLl9xKF92bS5pc0V4dHJhY3RlZCwgXCJub1wiKSB9LFxuICAgICAgICAgICAgICAgICAgb246IHtcbiAgICAgICAgICAgICAgICAgICAgY2xpY2s6IF92bS5pc0V4dHJhY3RlZE5vT25DbGljayxcbiAgICAgICAgICAgICAgICAgICAgY2hhbmdlOiBmdW5jdGlvbigkZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICBfdm0uaXNFeHRyYWN0ZWQgPSBcIm5vXCJcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgIF92bS5fdihcIiBOb1xcbiAgICAgICAgICAgICAgICAgICAgXCIpXG4gICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICBdKVxuICAgICAgICAgIF0pXG4gICAgICAgIF0pLFxuICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICBfYyhcInRyYW5zaXRpb25cIiwgeyBhdHRyczogeyBuYW1lOiBcInNsaWRlLWZhZGVcIiB9IH0sIFtcbiAgICAgICAgICBfYyhcbiAgICAgICAgICAgIFwiZGl2XCIsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGRpcmVjdGl2ZXM6IFtcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICBuYW1lOiBcInNob3dcIixcbiAgICAgICAgICAgICAgICAgIHJhd05hbWU6IFwidi1zaG93XCIsXG4gICAgICAgICAgICAgICAgICB2YWx1ZTogX3ZtLmlzRXh0cmFjdGVkID09IFwieWVzXCIsXG4gICAgICAgICAgICAgICAgICBleHByZXNzaW9uOiBcImlzRXh0cmFjdGVkID09ICd5ZXMnXCJcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIF1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBbXG4gICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiY29sLXNtLTNcIiB9LCBbXG4gICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJmb3JtLWdyb3VwXCIgfSwgW1xuICAgICAgICAgICAgICAgICAgX2MoXCJsYWJlbFwiLCB7IGF0dHJzOiB7IGZvcjogX3ZtLmNvcmUuY29kZSArIFwiX3F1YW50aXR5XCIgfSB9LCBbXG4gICAgICAgICAgICAgICAgICAgIF92bS5fdihcIlF1YW50aXR5XCIpXG4gICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgIF92bS5fdihcIiBcIiksXG4gICAgICAgICAgICAgICAgICBfYyhcImlucHV0XCIsIHtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgIGF0dHJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJudW1iZXJcIixcbiAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogXCJIb3cgbWFueSBwYXJ0cz9cIixcbiAgICAgICAgICAgICAgICAgICAgICBtaW46IFwiMFwiLFxuICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IF92bS5jb3JlLmNvZGUgKyBcIl9xdWFudGl0eVwiXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGRvbVByb3BzOiB7IHZhbHVlOiBfdm0uY29yZS5xdWFudGl0eSB9LFxuICAgICAgICAgICAgICAgICAgICBvbjogeyBpbnB1dDogX3ZtLnF1YW50aXR5T25JbnB1dCB9XG4gICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1zbS0zXCIgfSwgW1xuICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgIHsgYXR0cnM6IHsgZm9yOiBfdm0uY29yZS5jb2RlICsgXCJfcHJpY2VfcGVyX3VuaXRcIiB9IH0sXG4gICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJQcmljZSBwZXIgdW5pdFwiKV1cbiAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiJFwiKVxuICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwibnVtYmVyXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogXCJIb3cgbXVjaCBmb3IgZWFjaD9cIixcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbjogXCIwXCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBfdm0uY29yZS5jb2RlICsgXCJfcHJpY2VfcGVyX3VuaXRcIlxuICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgZG9tUHJvcHM6IHsgdmFsdWU6IF92bS5jb3JlLnByaWNlUGVyVW5pdCB9LFxuICAgICAgICAgICAgICAgICAgICAgIG9uOiB7IGlucHV0OiBfdm0ucHJpY2VQZXJVbml0T25JbnB1dCB9XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICAgIF0pXG4gICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICBfYyhcImRpdlwiLCB7IHN0YXRpY0NsYXNzOiBcImNvbC1zbS0zXCIgfSwgW1xuICAgICAgICAgICAgICAgIF9jKFwiZGl2XCIsIHsgc3RhdGljQ2xhc3M6IFwiZm9ybS1ncm91cFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgIF9jKFxuICAgICAgICAgICAgICAgICAgICBcImxhYmVsXCIsXG4gICAgICAgICAgICAgICAgICAgIHsgYXR0cnM6IHsgZm9yOiBfdm0uY29yZS5jb2RlICsgXCJfdG90YWxfcHJpY2VcIiB9IH0sXG4gICAgICAgICAgICAgICAgICAgIFtfdm0uX3YoXCJUb3RhbFwiKV1cbiAgICAgICAgICAgICAgICAgICksXG4gICAgICAgICAgICAgICAgICBfdm0uX3YoXCIgXCIpLFxuICAgICAgICAgICAgICAgICAgX2MoXCJkaXZcIiwgeyBzdGF0aWNDbGFzczogXCJpbnB1dC1ncm91cFwiIH0sIFtcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJzcGFuXCIsIHsgc3RhdGljQ2xhc3M6IFwiaW5wdXQtZ3JvdXAtYWRkb25cIiB9LCBbXG4gICAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiJFwiKVxuICAgICAgICAgICAgICAgICAgICBdKSxcbiAgICAgICAgICAgICAgICAgICAgX3ZtLl92KFwiIFwiKSxcbiAgICAgICAgICAgICAgICAgICAgX2MoXCJpbnB1dFwiLCB7XG4gICAgICAgICAgICAgICAgICAgICAgc3RhdGljQ2xhc3M6IFwiZm9ybS1jb250cm9sXCIsXG4gICAgICAgICAgICAgICAgICAgICAgYXR0cnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRvbmx5OiBcIlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogXCJ0ZXh0XCIsXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogXCJUb3RhbCBwcmljZVwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogX3ZtLmNvcmUuY29kZSArIFwiX3RvdGFsX3ByaWNlXCJcbiAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgIGRvbVByb3BzOiB7IHZhbHVlOiBfdm0udG90YWxQcmljZURpc3BsYXkgfVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgXSlcbiAgICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgICBdKVxuICAgICAgICAgICAgXVxuICAgICAgICAgIClcbiAgICAgICAgXSlcbiAgICAgIF0sXG4gICAgICAxXG4gICAgKVxuICBdKVxufVxudmFyIHN0YXRpY1JlbmRlckZucyA9IFtdXG5yZW5kZXIuX3dpdGhTdHJpcHBlZCA9IHRydWVcblxuZXhwb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSIsIi8qIGdsb2JhbHMgX19WVUVfU1NSX0NPTlRFWFRfXyAqL1xuXG4vLyBJTVBPUlRBTlQ6IERvIE5PVCB1c2UgRVMyMDE1IGZlYXR1cmVzIGluIHRoaXMgZmlsZSAoZXhjZXB0IGZvciBtb2R1bGVzKS5cbi8vIFRoaXMgbW9kdWxlIGlzIGEgcnVudGltZSB1dGlsaXR5IGZvciBjbGVhbmVyIGNvbXBvbmVudCBtb2R1bGUgb3V0cHV0IGFuZCB3aWxsXG4vLyBiZSBpbmNsdWRlZCBpbiB0aGUgZmluYWwgd2VicGFjayB1c2VyIGJ1bmRsZS5cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gbm9ybWFsaXplQ29tcG9uZW50IChcbiAgc2NyaXB0RXhwb3J0cyxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZ1bmN0aW9uYWxUZW1wbGF0ZSxcbiAgaW5qZWN0U3R5bGVzLFxuICBzY29wZUlkLFxuICBtb2R1bGVJZGVudGlmaWVyLCAvKiBzZXJ2ZXIgb25seSAqL1xuICBzaGFkb3dNb2RlIC8qIHZ1ZS1jbGkgb25seSAqL1xuKSB7XG4gIC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3BcbiAgdmFyIG9wdGlvbnMgPSB0eXBlb2Ygc2NyaXB0RXhwb3J0cyA9PT0gJ2Z1bmN0aW9uJ1xuICAgID8gc2NyaXB0RXhwb3J0cy5vcHRpb25zXG4gICAgOiBzY3JpcHRFeHBvcnRzXG5cbiAgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuICBpZiAocmVuZGVyKSB7XG4gICAgb3B0aW9ucy5yZW5kZXIgPSByZW5kZXJcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IHN0YXRpY1JlbmRlckZuc1xuICAgIG9wdGlvbnMuX2NvbXBpbGVkID0gdHJ1ZVxuICB9XG5cbiAgLy8gZnVuY3Rpb25hbCB0ZW1wbGF0ZVxuICBpZiAoZnVuY3Rpb25hbFRlbXBsYXRlKSB7XG4gICAgb3B0aW9ucy5mdW5jdGlvbmFsID0gdHJ1ZVxuICB9XG5cbiAgLy8gc2NvcGVkSWRcbiAgaWYgKHNjb3BlSWQpIHtcbiAgICBvcHRpb25zLl9zY29wZUlkID0gJ2RhdGEtdi0nICsgc2NvcGVJZFxuICB9XG5cbiAgdmFyIGhvb2tcbiAgaWYgKG1vZHVsZUlkZW50aWZpZXIpIHsgLy8gc2VydmVyIGJ1aWxkXG4gICAgaG9vayA9IGZ1bmN0aW9uIChjb250ZXh0KSB7XG4gICAgICAvLyAyLjMgaW5qZWN0aW9uXG4gICAgICBjb250ZXh0ID1cbiAgICAgICAgY29udGV4dCB8fCAvLyBjYWNoZWQgY2FsbFxuICAgICAgICAodGhpcy4kdm5vZGUgJiYgdGhpcy4kdm5vZGUuc3NyQ29udGV4dCkgfHwgLy8gc3RhdGVmdWxcbiAgICAgICAgKHRoaXMucGFyZW50ICYmIHRoaXMucGFyZW50LiR2bm9kZSAmJiB0aGlzLnBhcmVudC4kdm5vZGUuc3NyQ29udGV4dCkgLy8gZnVuY3Rpb25hbFxuICAgICAgLy8gMi4yIHdpdGggcnVuSW5OZXdDb250ZXh0OiB0cnVlXG4gICAgICBpZiAoIWNvbnRleHQgJiYgdHlwZW9mIF9fVlVFX1NTUl9DT05URVhUX18gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIGNvbnRleHQgPSBfX1ZVRV9TU1JfQ09OVEVYVF9fXG4gICAgICB9XG4gICAgICAvLyBpbmplY3QgY29tcG9uZW50IHN0eWxlc1xuICAgICAgaWYgKGluamVjdFN0eWxlcykge1xuICAgICAgICBpbmplY3RTdHlsZXMuY2FsbCh0aGlzLCBjb250ZXh0KVxuICAgICAgfVxuICAgICAgLy8gcmVnaXN0ZXIgY29tcG9uZW50IG1vZHVsZSBpZGVudGlmaWVyIGZvciBhc3luYyBjaHVuayBpbmZlcnJlbmNlXG4gICAgICBpZiAoY29udGV4dCAmJiBjb250ZXh0Ll9yZWdpc3RlcmVkQ29tcG9uZW50cykge1xuICAgICAgICBjb250ZXh0Ll9yZWdpc3RlcmVkQ29tcG9uZW50cy5hZGQobW9kdWxlSWRlbnRpZmllcilcbiAgICAgIH1cbiAgICB9XG4gICAgLy8gdXNlZCBieSBzc3IgaW4gY2FzZSBjb21wb25lbnQgaXMgY2FjaGVkIGFuZCBiZWZvcmVDcmVhdGVcbiAgICAvLyBuZXZlciBnZXRzIGNhbGxlZFxuICAgIG9wdGlvbnMuX3NzclJlZ2lzdGVyID0gaG9va1xuICB9IGVsc2UgaWYgKGluamVjdFN0eWxlcykge1xuICAgIGhvb2sgPSBzaGFkb3dNb2RlXG4gICAgICA/IGZ1bmN0aW9uICgpIHsgaW5qZWN0U3R5bGVzLmNhbGwodGhpcywgdGhpcy4kcm9vdC4kb3B0aW9ucy5zaGFkb3dSb290KSB9XG4gICAgICA6IGluamVjdFN0eWxlc1xuICB9XG5cbiAgaWYgKGhvb2spIHtcbiAgICBpZiAob3B0aW9ucy5mdW5jdGlvbmFsKSB7XG4gICAgICAvLyBmb3IgdGVtcGxhdGUtb25seSBob3QtcmVsb2FkIGJlY2F1c2UgaW4gdGhhdCBjYXNlIHRoZSByZW5kZXIgZm4gZG9lc24ndFxuICAgICAgLy8gZ28gdGhyb3VnaCB0aGUgbm9ybWFsaXplclxuICAgICAgb3B0aW9ucy5faW5qZWN0U3R5bGVzID0gaG9va1xuICAgICAgLy8gcmVnaXN0ZXIgZm9yIGZ1bmN0aW9hbCBjb21wb25lbnQgaW4gdnVlIGZpbGVcbiAgICAgIHZhciBvcmlnaW5hbFJlbmRlciA9IG9wdGlvbnMucmVuZGVyXG4gICAgICBvcHRpb25zLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcldpdGhTdHlsZUluamVjdGlvbiAoaCwgY29udGV4dCkge1xuICAgICAgICBob29rLmNhbGwoY29udGV4dClcbiAgICAgICAgcmV0dXJuIG9yaWdpbmFsUmVuZGVyKGgsIGNvbnRleHQpXG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIGluamVjdCBjb21wb25lbnQgcmVnaXN0cmF0aW9uIGFzIGJlZm9yZUNyZWF0ZSBob29rXG4gICAgICB2YXIgZXhpc3RpbmcgPSBvcHRpb25zLmJlZm9yZUNyZWF0ZVxuICAgICAgb3B0aW9ucy5iZWZvcmVDcmVhdGUgPSBleGlzdGluZ1xuICAgICAgICA/IFtdLmNvbmNhdChleGlzdGluZywgaG9vaylcbiAgICAgICAgOiBbaG9va11cbiAgICB9XG4gIH1cblxuICByZXR1cm4ge1xuICAgIGV4cG9ydHM6IHNjcmlwdEV4cG9ydHMsXG4gICAgb3B0aW9uczogb3B0aW9uc1xuICB9XG59XG4iLCJpbXBvcnQgeyByZW5kZXIsIHN0YXRpY1JlbmRlckZucyB9IGZyb20gXCIuL0NhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1jMjk5NzJkYSZcIlxuaW1wb3J0IHNjcmlwdCBmcm9tIFwiLi9DYXB0dXJlRGVhbFZlaGljbGVDb3Jlcy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL0NhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIlxuXG5cbi8qIG5vcm1hbGl6ZSBjb21wb25lbnQgKi9cbmltcG9ydCBub3JtYWxpemVyIGZyb20gXCIhLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL3J1bnRpbWUvY29tcG9uZW50Tm9ybWFsaXplci5qc1wiXG52YXIgY29tcG9uZW50ID0gbm9ybWFsaXplcihcbiAgc2NyaXB0LFxuICByZW5kZXIsXG4gIHN0YXRpY1JlbmRlckZucyxcbiAgZmFsc2UsXG4gIG51bGwsXG4gIG51bGwsXG4gIG51bGxcbiAgXG4pXG5cbi8qIGhvdCByZWxvYWQgKi9cbmlmIChtb2R1bGUuaG90KSB7XG4gIHZhciBhcGkgPSByZXF1aXJlKFwiQzpcXFxcVXNlcnNcXFxca2V2aW5cXFxcRGVza3RvcFxcXFxTb2Z0d2FyZVByb2plY3RzXFxcXGNzYXItY29yZS1hZG1pblxcXFxwdWJsaWNcXFxcanNcXFxcbm9kZV9tb2R1bGVzXFxcXHZ1ZS1ob3QtcmVsb2FkLWFwaVxcXFxkaXN0XFxcXGluZGV4LmpzXCIpXG4gIGFwaS5pbnN0YWxsKHJlcXVpcmUoJ3Z1ZScpKVxuICBpZiAoYXBpLmNvbXBhdGlibGUpIHtcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpXG4gICAgaWYgKCFtb2R1bGUuaG90LmRhdGEpIHtcbiAgICAgIGFwaS5jcmVhdGVSZWNvcmQoJ2MyOTk3MmRhJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfSBlbHNlIHtcbiAgICAgIGFwaS5yZWxvYWQoJ2MyOTk3MmRhJywgY29tcG9uZW50Lm9wdGlvbnMpXG4gICAgfVxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KFwiLi9DYXB0dXJlRGVhbFZlaGljbGVDb3Jlcy52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9YzI5OTcyZGEmXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGFwaS5yZXJlbmRlcignYzI5OTcyZGEnLCB7XG4gICAgICAgIHJlbmRlcjogcmVuZGVyLFxuICAgICAgICBzdGF0aWNSZW5kZXJGbnM6IHN0YXRpY1JlbmRlckZuc1xuICAgICAgfSlcbiAgICB9KVxuICB9XG59XG5jb21wb25lbnQub3B0aW9ucy5fX2ZpbGUgPSBcInNyY1xcXFx2aWV3bW9kZWxzXFxcXENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanMhLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9DYXB0dXJlRGVhbFZlaGljbGVDb3Jlcy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCI7IGV4cG9ydCBkZWZhdWx0IG1vZDsgZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanMhLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9DYXB0dXJlRGVhbFZlaGljbGVDb3Jlcy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCIiLCJleHBvcnQgKiBmcm9tIFwiLSEuLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvbG9hZGVycy90ZW1wbGF0ZUxvYWRlci5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vQ2FwdHVyZURlYWxWZWhpY2xlQ29yZXMudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWMyOTk3MmRhJlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9FZGl0VmVoaWNsZUNvcmUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTY3YTFjOWJhJlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL0VkaXRWZWhpY2xlQ29yZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL0VkaXRWZWhpY2xlQ29yZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkM6XFxcXFVzZXJzXFxcXGtldmluXFxcXERlc2t0b3BcXFxcU29mdHdhcmVQcm9qZWN0c1xcXFxjc2FyLWNvcmUtYWRtaW5cXFxccHVibGljXFxcXGpzXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCc2N2ExYzliYScsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCc2N2ExYzliYScsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vRWRpdFZlaGljbGVDb3JlLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD02N2ExYzliYSZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCc2N2ExYzliYScsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic3JjXFxcXHZpZXdtb2RlbHNcXFxcRWRpdFZlaGljbGVDb3JlLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanMhLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9FZGl0VmVoaWNsZUNvcmUudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vRWRpdFZlaGljbGVDb3JlLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9FZGl0VmVoaWNsZUNvcmUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTY3YTFjOWJhJlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9NYW5hZ2VWZWhpY2xlQ29yZXMudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWZjOWMzNzc4JlwiXG5pbXBvcnQgc2NyaXB0IGZyb20gXCIuL01hbmFnZVZlaGljbGVDb3Jlcy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcbmV4cG9ydCAqIGZyb20gXCIuL01hbmFnZVZlaGljbGVDb3Jlcy52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkM6XFxcXFVzZXJzXFxcXGtldmluXFxcXERlc2t0b3BcXFxcU29mdHdhcmVQcm9qZWN0c1xcXFxjc2FyLWNvcmUtYWRtaW5cXFxccHVibGljXFxcXGpzXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCdmYzljMzc3OCcsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCdmYzljMzc3OCcsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vTWFuYWdlVmVoaWNsZUNvcmVzLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD1mYzljMzc3OCZcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgYXBpLnJlcmVuZGVyKCdmYzljMzc3OCcsIHtcbiAgICAgICAgcmVuZGVyOiByZW5kZXIsXG4gICAgICAgIHN0YXRpY1JlbmRlckZuczogc3RhdGljUmVuZGVyRm5zXG4gICAgICB9KVxuICAgIH0pXG4gIH1cbn1cbmNvbXBvbmVudC5vcHRpb25zLl9fZmlsZSA9IFwic3JjXFxcXHZpZXdtb2RlbHNcXFxcTWFuYWdlVmVoaWNsZUNvcmVzLnZ1ZVwiXG5leHBvcnQgZGVmYXVsdCBjb21wb25lbnQuZXhwb3J0cyIsImltcG9ydCBtb2QgZnJvbSBcIi0hLi4vLi4vbm9kZV9tb2R1bGVzL2JhYmVsLWxvYWRlci9saWIvaW5kZXguanMhLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9NYW5hZ2VWZWhpY2xlQ29yZXMudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vTWFuYWdlVmVoaWNsZUNvcmVzLnZ1ZT92dWUmdHlwZT1zY3JpcHQmbGFuZz1qcyZcIiIsImV4cG9ydCAqIGZyb20gXCItIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9sb2FkZXJzL3RlbXBsYXRlTG9hZGVyLmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2luZGV4LmpzPz92dWUtbG9hZGVyLW9wdGlvbnMhLi9NYW5hZ2VWZWhpY2xlQ29yZXMudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPWZjOWMzNzc4JlwiIiwiaW1wb3J0IHsgcmVuZGVyLCBzdGF0aWNSZW5kZXJGbnMgfSBmcm9tIFwiLi9WZWhpY2xlQ29yZS52dWU/dnVlJnR5cGU9dGVtcGxhdGUmaWQ9N2IxNjRkYTQmXCJcbmltcG9ydCBzY3JpcHQgZnJvbSBcIi4vVmVoaWNsZUNvcmUudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiXG5leHBvcnQgKiBmcm9tIFwiLi9WZWhpY2xlQ29yZS52dWU/dnVlJnR5cGU9c2NyaXB0Jmxhbmc9anMmXCJcblxuXG4vKiBub3JtYWxpemUgY29tcG9uZW50ICovXG5pbXBvcnQgbm9ybWFsaXplciBmcm9tIFwiIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9ydW50aW1lL2NvbXBvbmVudE5vcm1hbGl6ZXIuanNcIlxudmFyIGNvbXBvbmVudCA9IG5vcm1hbGl6ZXIoXG4gIHNjcmlwdCxcbiAgcmVuZGVyLFxuICBzdGF0aWNSZW5kZXJGbnMsXG4gIGZhbHNlLFxuICBudWxsLFxuICBudWxsLFxuICBudWxsXG4gIFxuKVxuXG4vKiBob3QgcmVsb2FkICovXG5pZiAobW9kdWxlLmhvdCkge1xuICB2YXIgYXBpID0gcmVxdWlyZShcIkM6XFxcXFVzZXJzXFxcXGtldmluXFxcXERlc2t0b3BcXFxcU29mdHdhcmVQcm9qZWN0c1xcXFxjc2FyLWNvcmUtYWRtaW5cXFxccHVibGljXFxcXGpzXFxcXG5vZGVfbW9kdWxlc1xcXFx2dWUtaG90LXJlbG9hZC1hcGlcXFxcZGlzdFxcXFxpbmRleC5qc1wiKVxuICBhcGkuaW5zdGFsbChyZXF1aXJlKCd2dWUnKSlcbiAgaWYgKGFwaS5jb21wYXRpYmxlKSB7XG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKVxuICAgIGlmICghbW9kdWxlLmhvdC5kYXRhKSB7XG4gICAgICBhcGkuY3JlYXRlUmVjb3JkKCc3YjE2NGRhNCcsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH0gZWxzZSB7XG4gICAgICBhcGkucmVsb2FkKCc3YjE2NGRhNCcsIGNvbXBvbmVudC5vcHRpb25zKVxuICAgIH1cbiAgICBtb2R1bGUuaG90LmFjY2VwdChcIi4vVmVoaWNsZUNvcmUudnVlP3Z1ZSZ0eXBlPXRlbXBsYXRlJmlkPTdiMTY0ZGE0JlwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBhcGkucmVyZW5kZXIoJzdiMTY0ZGE0Jywge1xuICAgICAgICByZW5kZXI6IHJlbmRlcixcbiAgICAgICAgc3RhdGljUmVuZGVyRm5zOiBzdGF0aWNSZW5kZXJGbnNcbiAgICAgIH0pXG4gICAgfSlcbiAgfVxufVxuY29tcG9uZW50Lm9wdGlvbnMuX19maWxlID0gXCJzcmNcXFxcdmlld21vZGVsc1xcXFxWZWhpY2xlQ29yZS52dWVcIlxuZXhwb3J0IGRlZmF1bHQgY29tcG9uZW50LmV4cG9ydHMiLCJpbXBvcnQgbW9kIGZyb20gXCItIS4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVmVoaWNsZUNvcmUudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiOyBleHBvcnQgZGVmYXVsdCBtb2Q7IGV4cG9ydCAqIGZyb20gXCItIS4uLy4uL25vZGVfbW9kdWxlcy9iYWJlbC1sb2FkZXIvbGliL2luZGV4LmpzIS4uLy4uL25vZGVfbW9kdWxlcy92dWUtbG9hZGVyL2xpYi9pbmRleC5qcz8/dnVlLWxvYWRlci1vcHRpb25zIS4vVmVoaWNsZUNvcmUudnVlP3Z1ZSZ0eXBlPXNjcmlwdCZsYW5nPWpzJlwiIiwiZXhwb3J0ICogZnJvbSBcIi0hLi4vLi4vbm9kZV9tb2R1bGVzL3Z1ZS1sb2FkZXIvbGliL2xvYWRlcnMvdGVtcGxhdGVMb2FkZXIuanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuLi8uLi9ub2RlX21vZHVsZXMvdnVlLWxvYWRlci9saWIvaW5kZXguanM/P3Z1ZS1sb2FkZXItb3B0aW9ucyEuL1ZlaGljbGVDb3JlLnZ1ZT92dWUmdHlwZT10ZW1wbGF0ZSZpZD03YjE2NGRhNCZcIiIsInJlcXVpcmUoXCJqc2RvbS1nbG9iYWxcIikoKTtcclxuXHJcbi8vIHZhciBtb2NoYSA9IHJlcXVpcmUoXCJtb2NoYVwiKTtcclxuLy8gdmFyIGNoYWkgPSByZXF1aXJlKFwiY2hhaVwiKTtcclxuXHJcbi8vIGdsb2JhbC5kZXNjcmliZSA9IG1vY2hhLmRlc2NyaWJlO1xyXG4vLyBnbG9iYWwuaXQgPSBtb2NoYS5pdDtcclxuXHJcbi8vIGdsb2JhbC5leHBlY3QgPSBjaGFpLmV4cGVjdDtcclxuLy8gZ2xvYmFsLmFzc2VydCA9IGNoYWkuYXNzZXJ0O1xyXG5cclxucmVxdWlyZShcIi4vc3BlY3MvVmVoaWNsZUNvcmUuc3BlY1wiKTtcclxucmVxdWlyZShcIi4vc3BlY3MvRWRpdFZlaGljbGVDb3JlLnNwZWNcIik7XHJcbnJlcXVpcmUoXCIuL3NwZWNzL01hbmFnZVZlaGljbGVDb3Jlcy5zcGVjXCIpO1xyXG5yZXF1aXJlKFwiLi9zcGVjcy9DYXB0dXJlRGVhbFZlaGljbGVDb3Jlcy5zcGVjXCIpO1xyXG4iLCIvLyBEZWJ1Z2dpbmcgZG9lcyBub3Qgd29yayBpZiBtb2NoYSBpcyByZXF1aXJlZC5cclxuLy8gdmFyIG1vY2hhID0gcmVxdWlyZShcIm1vY2hhXCIpO1xyXG4vLyB2YXIgZGVzY3JpYmUgPSBtb2NoYS5kZXNjcmliZTtcclxuLy8gdmFyIGl0ID0gbW9jaGEuaXQ7XHJcblxyXG52YXIgY2hhaSA9IHJlcXVpcmUoXCJjaGFpXCIpO1xyXG52YXIgYXNzZXJ0ID0gY2hhaS5hc3NlcnQ7XHJcblxyXG52YXIgc2lub24gPSByZXF1aXJlKFwic2lub25cIik7XHJcblxyXG5pbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcbmltcG9ydCB7IHNoYWxsb3dNb3VudCB9IGZyb20gXCJAdnVlL3Rlc3QtdXRpbHNcIjtcclxuaW1wb3J0IENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzIGZyb20gXCIuLi8uLi9zcmMvdmlld21vZGVscy9DYXB0dXJlRGVhbFZlaGljbGVDb3Jlcy52dWVcIjtcclxuXHJcbmRlc2NyaWJlKFwiQ2FwdHVyZURlYWxWZWhpY2xlQ29yZXMudnVlXCIsICgpID0+IHtcclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gbW91bnRcIiwgKCkgPT4ge1xyXG4gICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICBjb25zdCBhbGVydHNNYW5hZ2VyID0ge1xyXG4gICAgICAgICAgICBzaG93U3VjY2Vzczogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICBnZXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICB0aGVuOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgIHRoZW46IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc3QgdmFsaWRhdG9yID0ge1xyXG4gICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgaXNWYWxpZDogc2lub24uc3B5KClcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgIGdldFF1ZXJ5U3RyaW5nUGFyYW06IHNpbm9uLnNweSgpXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc3Qgb25Jbml0ID0gc2lub24uc3B5KCk7XHJcblxyXG4gICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgIHByb3ZpZGU6IHtcclxuICAgICAgICAgICAgICAgIGFsZXJ0c01hbmFnZXIsXHJcbiAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICB2YWxpZGF0b3IsXHJcbiAgICAgICAgICAgICAgICBvbkluaXQsXHJcbiAgICAgICAgICAgICAgICB1cmxIZWxwZXJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICBhc3NlcnQuaXNOb3ROdWxsKHdyYXBwZXIudm0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgZGVzY3JpYmUoXCJjcmVhdGVkXCIsICgpID0+IHtcclxuICAgICAgICBpdChcInNob3VsZCBnZXQgZGVhbCBpZCBmcm9tIHF1ZXJ5IHN0cmluZ1wiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBnZXRDb25maWd1cmF0aW9uRGF0YTogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQ6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXRRdWVyeVN0cmluZ1BhcmFtOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3Qgb25Jbml0ID0gc2lub24uc3B5KCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IHNoYWxsb3dNb3VudChDYXB0dXJlRGVhbFZlaGljbGVDb3Jlcywge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0c01hbmFnZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgYXBpQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcixcclxuICAgICAgICAgICAgICAgICAgICBvbkluaXQsXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsSGVscGVyXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0ICYgQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUodXJsSGVscGVyLmdldFF1ZXJ5U3RyaW5nUGFyYW0uY2FsbGVkT25jZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGl0KFwic2hvdWxkIGluaXRpYWxpemUgY29ycmVjdGx5IHdoZW4gZGVhbCBpZCBpcyBnaXZlblwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBnZXRDb25maWd1cmF0aW9uRGF0YTogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQ6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXRRdWVyeVN0cmluZ1BhcmFtOiBzaW5vbi5zdHViKCkucmV0dXJucyhcIjEyMzQ1XCIpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluaXQgPSBzaW5vbi5zcHkoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdCxcclxuICAgICAgICAgICAgICAgICAgICB1cmxIZWxwZXJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uZGVhbElkLCBcIjEyMzQ1XCIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpdChcInNob3VsZCBzaG93IGVycm9yIHdoZW4gbm8gZGVhbCBpZCBpcyBnaXZlblwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBnZXRDb25maWd1cmF0aW9uRGF0YTogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQ6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXRRdWVyeVN0cmluZ1BhcmFtOiBzaW5vbi5zdHViKCkucmV0dXJucyhudWxsKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3Qgb25Jbml0ID0gc2lub24uc3B5KCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IHNoYWxsb3dNb3VudChDYXB0dXJlRGVhbFZlaGljbGVDb3Jlcywge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0c01hbmFnZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgYXBpQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcixcclxuICAgICAgICAgICAgICAgICAgICBvbkluaXQsXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsSGVscGVyXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0ICYgQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUod3JhcHBlci52bS5pc0luaXRFcnJvcik7XHJcbiAgICAgICAgICAgIGFzc2VydC5pc05vdEVtcHR5KHdyYXBwZXIudm0uaW5pdEVycm9yTWVzc2FnZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGl0KFwic2hvdWxkIGxvYWQgY29yZSBkYXRhIGNvcnJlY3RseSB3aGVuIHJlc3BvbnNlIGlzIDIwMFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVhbFZlaGljbGVDb3JlczogW11cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBnZXRDb25maWd1cmF0aW9uRGF0YTogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IDIwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvcmVzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIkRlc2NyaXB0aW9uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRRdWFudGl0eTogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmljZVBlclVuaXQ6IDEwLjAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdmFsaWRhdG9yID0ge1xyXG4gICAgICAgICAgICAgICAgYWRkVmFsaWRhdGlvbkZvcjogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgICAgICBpc1ZhbGlkOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdXJsSGVscGVyID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0UXVlcnlTdHJpbmdQYXJhbTogc2lub24uc3R1YigpLnJldHVybnMoXCIxMjM0NVwiKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3Qgb25Jbml0ID0gc2lub24uc3B5KCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IHNoYWxsb3dNb3VudChDYXB0dXJlRGVhbFZlaGljbGVDb3Jlcywge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0c01hbmFnZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgYXBpQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcixcclxuICAgICAgICAgICAgICAgICAgICBvbkluaXQsXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsSGVscGVyXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0ICYgQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc05vdEVtcHR5KHdyYXBwZXIudm0uY29yZXMpO1xyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3Jlc1swXS52ZWhpY2xlQ29yZUlkLCAxKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uY29yZXNbMF0uY29kZSwgXCJjb2RlXCIpO1xyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3Jlc1swXS5kZXNjcmlwdGlvbiwgXCJEZXNjcmlwdGlvblwiKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmlzRmFsc2Uod3JhcHBlci52bS5jb3Jlc1swXS5pc0V4dHJhY3RlZCk7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmNvcmVzWzBdLnF1YW50aXR5LCAxMCk7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmNvcmVzWzBdLnByaWNlUGVyVW5pdCwgMTAuMDApO1xyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3Jlc1swXS50b3RhbFByaWNlLCAwLjAwKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgdXNlIGRlYWwncyBjb3JlIGRhdGEgaWYgcHJlc2VudFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVhbFZlaGljbGVDb3JlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmVoaWNsZUNvcmVJZDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IDUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHByaWNlUGVyVW5pdDogNS4wMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG90YWxQcmljZTogMjUuMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGdldENvbmZpZ3VyYXRpb25EYXRhOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29yZXM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcIkNPREVcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiRGVzY3JpcHRpb25cIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFF1YW50aXR5OiAxMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFByaWNlUGVyVW5pdDogMTAuMDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQ6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXRRdWVyeVN0cmluZ1BhcmFtOiBzaW5vbi5zdHViKCkucmV0dXJucyhcIjEyMzQ1XCIpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluaXQgPSBzaW5vbi5zcHkoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdCxcclxuICAgICAgICAgICAgICAgICAgICB1cmxIZWxwZXJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmlzTm90RW1wdHkod3JhcHBlci52bS5jb3Jlcyk7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmNvcmVzWzBdLnZlaGljbGVDb3JlSWQsIDEpO1xyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3Jlc1swXS5jb2RlLCBcImNvZGVcIik7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmNvcmVzWzBdLmRlc2NyaXB0aW9uLCBcIkRlc2NyaXB0aW9uXCIpO1xyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKHdyYXBwZXIudm0uY29yZXNbMF0uaXNFeHRyYWN0ZWQpO1xyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3Jlc1swXS5xdWFudGl0eSwgNSk7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmNvcmVzWzBdLnByaWNlUGVyVW5pdCwgNS4wMCk7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmNvcmVzWzBdLnRvdGFsUHJpY2UsIDI1LjAwKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgbm90IHVzZSBkZWFsJ3MgY29yZSBkYXRhIGlmIGlkIG1pc21hdGNoXCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBjb25zdCBhbGVydHNNYW5hZ2VyID0ge1xyXG4gICAgICAgICAgICAgICAgc2hvd1N1Y2Nlc3M6IHNpbm9uLnNweSgpLFxyXG4gICAgICAgICAgICAgICAgc2hvd0Rhbmdlcjogc2lub24uc3B5KClcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgICAgIGdldERlYWxWZWhpY2xlQ29yZXM6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWFsVmVoaWNsZUNvcmVzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2ZWhpY2xlQ29yZUlkOiAyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBxdWFudGl0eTogNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcHJpY2VQZXJVbml0OiA1LjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3RhbFByaWNlOiAyNS4wMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3JlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiQ09ERVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJEZXNjcmlwdGlvblwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UXVhbnRpdHk6IDEwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UHJpY2VQZXJVbml0OiAxMC4wMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHZhbGlkYXRvciA9IHtcclxuICAgICAgICAgICAgICAgIGFkZFZhbGlkYXRpb25Gb3I6IHNpbm9uLnNweSgpLFxyXG4gICAgICAgICAgICAgICAgaXNWYWxpZDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHVybEhlbHBlciA9IHtcclxuICAgICAgICAgICAgICAgIGdldFF1ZXJ5U3RyaW5nUGFyYW06IHNpbm9uLnN0dWIoKS5yZXR1cm5zKFwiMTIzNDVcIilcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG9uSW5pdCA9IHNpbm9uLnNweSgpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBzaGFsbG93TW91bnQoQ2FwdHVyZURlYWxWZWhpY2xlQ29yZXMsIHtcclxuICAgICAgICAgICAgICAgIHByb3ZpZGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGVydHNNYW5hZ2VyLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3IsXHJcbiAgICAgICAgICAgICAgICAgICAgb25Jbml0LFxyXG4gICAgICAgICAgICAgICAgICAgIHVybEhlbHBlclxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuaXNOb3RFbXB0eSh3cmFwcGVyLnZtLmNvcmVzKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uY29yZXNbMF0udmVoaWNsZUNvcmVJZCwgMSk7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmNvcmVzWzBdLmNvZGUsIFwiY29kZVwiKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uY29yZXNbMF0uZGVzY3JpcHRpb24sIFwiRGVzY3JpcHRpb25cIik7XHJcbiAgICAgICAgICAgIGFzc2VydC5pc0ZhbHNlKHdyYXBwZXIudm0uY29yZXNbMF0uaXNFeHRyYWN0ZWQpO1xyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3Jlc1swXS5xdWFudGl0eSwgMTApO1xyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3Jlc1swXS5wcmljZVBlclVuaXQsIDEwLjAwKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uY29yZXNbMF0udG90YWxQcmljZSwgMC4wMCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGl0KFwic2hvdWxkIGNvbmZpZ3VyZSB2YWxpZGF0b3IgZm9yIHRoZSBnaXZlbiBjb3Jlc1wiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVhbFZlaGljbGVDb3JlczogW11cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBnZXRDb25maWd1cmF0aW9uRGF0YTogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IDIwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvcmVzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXzFcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiRGVzY3JpcHRpb25fMVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UXVhbnRpdHk6IDEwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UHJpY2VQZXJVbml0OiAxMC4wMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXzJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiRGVzY3JpcHRpb25fMlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UXVhbnRpdHk6IDUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmljZVBlclVuaXQ6IDUuMDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQ6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXRRdWVyeVN0cmluZ1BhcmFtOiBzaW5vbi5zdHViKCkucmV0dXJucyhcIjEyMzQ1XCIpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluaXQgPSBzaW5vbi5zcHkoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdCxcclxuICAgICAgICAgICAgICAgICAgICB1cmxIZWxwZXJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmlzVHJ1ZSh2YWxpZGF0b3IuYWRkVmFsaWRhdGlvbkZvci5jYWxsZWRUd2ljZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBkZXNjcmliZShcImNvbXB1dGVkOiB0b3RhbFZhbHVlXCIsICgpID0+IHtcclxuICAgICAgICBpdChcInNob3VsZCBiZSBjYWxjdWxhdGVkIGNvcnJlY3RseVwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVhbFZlaGljbGVDb3JlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmVoaWNsZUNvcmVJZDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IDEwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmljZVBlclVuaXQ6IDEwLjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3RhbFByaWNlOiAxMDAuMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZlaGljbGVDb3JlSWQ6IDIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiA1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcmljZVBlclVuaXQ6IDUuMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsUHJpY2U6IDI1LjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBnZXRDb25maWd1cmF0aW9uRGF0YTogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IDIwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvcmVzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXzFcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiRGVzY3JpcHRpb25fMVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UXVhbnRpdHk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmljZVBlclVuaXQ6IDEuMDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IDIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiQ09ERV8yXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIkRlc2NyaXB0aW9uXzJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFF1YW50aXR5OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UHJpY2VQZXJVbml0OiAxLjAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdmFsaWRhdG9yID0ge1xyXG4gICAgICAgICAgICAgICAgYWRkVmFsaWRhdGlvbkZvcjogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgICAgICBpc1ZhbGlkOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdXJsSGVscGVyID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0UXVlcnlTdHJpbmdQYXJhbTogc2lub24uc3R1YigpLnJldHVybnMoXCIxMjM0NVwiKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3Qgb25Jbml0ID0gc2lub24uc3B5KCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IHNoYWxsb3dNb3VudChDYXB0dXJlRGVhbFZlaGljbGVDb3Jlcywge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0c01hbmFnZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgYXBpQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcixcclxuICAgICAgICAgICAgICAgICAgICBvbkluaXQsXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsSGVscGVyXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0ICYgQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLnRvdGFsVmFsdWUsIDEyNS4wMCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGl0KFwic2hvdWxkIGJlIHplcm8gd2hlbiBubyBjb3JlcyBhcmUgZXh0cmFjdGVkXCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBjb25zdCBhbGVydHNNYW5hZ2VyID0ge1xyXG4gICAgICAgICAgICAgICAgc2hvd1N1Y2Nlc3M6IHNpbm9uLnNweSgpLFxyXG4gICAgICAgICAgICAgICAgc2hvd0Rhbmdlcjogc2lub24uc3B5KClcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgICAgIGdldERlYWxWZWhpY2xlQ29yZXM6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWFsVmVoaWNsZUNvcmVzOiBbXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGdldENvbmZpZ3VyYXRpb25EYXRhOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29yZXM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcIkNPREVfMVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJEZXNjcmlwdGlvbl8xXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRRdWFudGl0eTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFByaWNlUGVyVW5pdDogMS4wMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXzJcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiRGVzY3JpcHRpb25fMlwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UXVhbnRpdHk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmljZVBlclVuaXQ6IDEuMDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQ6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXRRdWVyeVN0cmluZ1BhcmFtOiBzaW5vbi5zdHViKCkucmV0dXJucyhcIjEyMzQ1XCIpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluaXQgPSBzaW5vbi5zcHkoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdCxcclxuICAgICAgICAgICAgICAgICAgICB1cmxIZWxwZXJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0udG90YWxWYWx1ZSwgMC4wMCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBkZXNjcmliZShcIm1ldGhvZDogc3VibWl0XCIsICgpID0+IHtcclxuICAgICAgICBpdChcInNob3VsZCBjYWxsIHRoZSBhcGlcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGNvbnN0IGFsZXJ0c01hbmFnZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBzaG93U3VjY2Vzczogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgICAgICBzaG93RGFuZ2VyOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0RGVhbFZlaGljbGVDb3Jlczogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IDIwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlYWxWZWhpY2xlQ29yZXM6IFtdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3JlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiQ09ERV8xXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIkRlc2NyaXB0aW9uXzFcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFF1YW50aXR5OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UHJpY2VQZXJVbml0OiAxLjAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIHN1Ym1pdERlYWxWZWhpY2xlQ29yZXM6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdmFsaWRhdG9yID0ge1xyXG4gICAgICAgICAgICAgICAgYWRkVmFsaWRhdGlvbkZvcjogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgICAgICBpc1ZhbGlkOiBzaW5vbi5zdHViKCkucmV0dXJucyh0cnVlKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdXJsSGVscGVyID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0UXVlcnlTdHJpbmdQYXJhbTogc2lub24uc3R1YigpLnJldHVybnMoXCIxMjM0NVwiKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3Qgb25Jbml0ID0gc2lub24uc3B5KCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IHNoYWxsb3dNb3VudChDYXB0dXJlRGVhbFZlaGljbGVDb3Jlcywge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0c01hbmFnZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgYXBpQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvcixcclxuICAgICAgICAgICAgICAgICAgICBvbkluaXQsXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsSGVscGVyXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0XHJcbiAgICAgICAgICAgIHdyYXBwZXIudm0uc3VibWl0KCk7XHJcblxyXG4gICAgICAgICAgICAvLyBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmlzVHJ1ZShhcGlDbGllbnQuc3VibWl0RGVhbFZlaGljbGVDb3Jlcy5jYWxsZWQpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpdChcInNob3VsZCBub3QgY2FsbCB0aGUgYXBpIHdoZW4gZGF0YSBpcyBub3QgdmFsaWRcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGNvbnN0IGFsZXJ0c01hbmFnZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBzaG93U3VjY2Vzczogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgICAgICBzaG93RGFuZ2VyOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0RGVhbFZlaGljbGVDb3Jlczogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IDIwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlYWxWZWhpY2xlQ29yZXM6IFtdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3JlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiQ09ERV8xXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIkRlc2NyaXB0aW9uXzFcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFF1YW50aXR5OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UHJpY2VQZXJVbml0OiAxLjAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIHN1Ym1pdERlYWxWZWhpY2xlQ29yZXM6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdmFsaWRhdG9yID0ge1xyXG4gICAgICAgICAgICAgICAgYWRkVmFsaWRhdGlvbkZvcjogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgICAgICBpc1ZhbGlkOiBzaW5vbi5zdHViKCkucmV0dXJucyhmYWxzZSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHVybEhlbHBlciA9IHtcclxuICAgICAgICAgICAgICAgIGdldFF1ZXJ5U3RyaW5nUGFyYW06IHNpbm9uLnN0dWIoKS5yZXR1cm5zKFwiMTIzNDVcIilcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG9uSW5pdCA9IHNpbm9uLnNweSgpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBzaGFsbG93TW91bnQoQ2FwdHVyZURlYWxWZWhpY2xlQ29yZXMsIHtcclxuICAgICAgICAgICAgICAgIHByb3ZpZGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGVydHNNYW5hZ2VyLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3IsXHJcbiAgICAgICAgICAgICAgICAgICAgb25Jbml0LFxyXG4gICAgICAgICAgICAgICAgICAgIHVybEhlbHBlclxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdFxyXG4gICAgICAgICAgICB3cmFwcGVyLnZtLnN1Ym1pdCgpO1xyXG5cclxuICAgICAgICAgICAgLy8gQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUoYXBpQ2xpZW50LnN1Ym1pdERlYWxWZWhpY2xlQ29yZXMubm90Q2FsbGVkKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgc2hvdyBhIHN1Y2Nlc3MgYWxlcnQgd2hlbiByZXNwb25zZSBpcyAyMDBcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGNvbnN0IGFsZXJ0c01hbmFnZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBzaG93U3VjY2Vzczogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgICAgICBzaG93RGFuZ2VyOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0RGVhbFZlaGljbGVDb3Jlczogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IDIwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlYWxWZWhpY2xlQ29yZXM6IFtdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3JlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiQ09ERV8xXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIkRlc2NyaXB0aW9uXzFcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFF1YW50aXR5OiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UHJpY2VQZXJVbml0OiAxLjAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIHN1Ym1pdERlYWxWZWhpY2xlQ29yZXM6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHZhbGlkYXRvciA9IHtcclxuICAgICAgICAgICAgICAgIGFkZFZhbGlkYXRpb25Gb3I6IHNpbm9uLnNweSgpLFxyXG4gICAgICAgICAgICAgICAgaXNWYWxpZDogc2lub24uc3R1YigpLnJldHVybnModHJ1ZSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHVybEhlbHBlciA9IHtcclxuICAgICAgICAgICAgICAgIGdldFF1ZXJ5U3RyaW5nUGFyYW06IHNpbm9uLnN0dWIoKS5yZXR1cm5zKFwiMTIzNDVcIilcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG9uSW5pdCA9IHNpbm9uLnNweSgpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBzaGFsbG93TW91bnQoQ2FwdHVyZURlYWxWZWhpY2xlQ29yZXMsIHtcclxuICAgICAgICAgICAgICAgIHByb3ZpZGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGVydHNNYW5hZ2VyLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3IsXHJcbiAgICAgICAgICAgICAgICAgICAgb25Jbml0LFxyXG4gICAgICAgICAgICAgICAgICAgIHVybEhlbHBlclxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdFxyXG4gICAgICAgICAgICB3cmFwcGVyLnZtLnN1Ym1pdCgpO1xyXG5cclxuICAgICAgICAgICAgLy8gQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUoYWxlcnRzTWFuYWdlci5zaG93U3VjY2Vzcy5jYWxsZWQpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpdChcInNob3VsZCBzaG93IGEgZGFuZ2VyIGFsZXJ0IHdoZW4gcmVzcG9uc2UgaXMgNDAwXCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBjb25zdCBhbGVydHNNYW5hZ2VyID0ge1xyXG4gICAgICAgICAgICAgICAgc2hvd1N1Y2Nlc3M6IHNpbm9uLnNweSgpLFxyXG4gICAgICAgICAgICAgICAgc2hvd0Rhbmdlcjogc2lub24uc3B5KClcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgICAgIGdldERlYWxWZWhpY2xlQ29yZXM6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWFsVmVoaWNsZUNvcmVzOiBbXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGdldENvbmZpZ3VyYXRpb25EYXRhOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29yZXM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcIkNPREVfMVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJEZXNjcmlwdGlvbl8xXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRRdWFudGl0eTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFByaWNlUGVyVW5pdDogMS4wMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBzdWJtaXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogNDAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQ6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHRydWUpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXRRdWVyeVN0cmluZ1BhcmFtOiBzaW5vbi5zdHViKCkucmV0dXJucyhcIjEyMzQ1XCIpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluaXQgPSBzaW5vbi5zcHkoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdCxcclxuICAgICAgICAgICAgICAgICAgICB1cmxIZWxwZXJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3RcclxuICAgICAgICAgICAgd3JhcHBlci52bS5zdWJtaXQoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKGFsZXJ0c01hbmFnZXIuc2hvd0Rhbmdlci5jYWxsZWQpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpdChcInNob3VsZCBzaG93IGEgZGFuZ2VyIGFsZXJ0IHdoZW4gcmVzcG9uc2UgaXMgNDAxXCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBjb25zdCBhbGVydHNNYW5hZ2VyID0ge1xyXG4gICAgICAgICAgICAgICAgc2hvd1N1Y2Nlc3M6IHNpbm9uLnNweSgpLFxyXG4gICAgICAgICAgICAgICAgc2hvd0Rhbmdlcjogc2lub24uc3B5KClcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgICAgIGdldERlYWxWZWhpY2xlQ29yZXM6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWFsVmVoaWNsZUNvcmVzOiBbXVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGdldENvbmZpZ3VyYXRpb25EYXRhOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29yZXM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcIkNPREVfMVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJEZXNjcmlwdGlvbl8xXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRRdWFudGl0eTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFByaWNlUGVyVW5pdDogMS4wMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBzdWJtaXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogNDAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQ6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHRydWUpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXRRdWVyeVN0cmluZ1BhcmFtOiBzaW5vbi5zdHViKCkucmV0dXJucyhcIjEyMzQ1XCIpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluaXQgPSBzaW5vbi5zcHkoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdCxcclxuICAgICAgICAgICAgICAgICAgICB1cmxIZWxwZXJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3RcclxuICAgICAgICAgICAgd3JhcHBlci52bS5zdWJtaXQoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKGFsZXJ0c01hbmFnZXIuc2hvd0Rhbmdlci5jYWxsZWQpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpdChcInNob3VsZCBzaG93IGEgZGFuZ2VyIGFsZXJ0IHdoZW4gbmV0d29yayBlcnJvclwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXREZWFsVmVoaWNsZUNvcmVzOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVhbFZlaGljbGVDb3JlczogW11cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBnZXRDb25maWd1cmF0aW9uRGF0YTogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IDIwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvcmVzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXzFcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiRGVzY3JpcHRpb25fMVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UXVhbnRpdHk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmljZVBlclVuaXQ6IDEuMDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgc3VibWl0RGVhbFZlaGljbGVDb3Jlczogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soXCJtb2NrIGVycm9yXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3IgPSB7XHJcbiAgICAgICAgICAgICAgICBhZGRWYWxpZGF0aW9uRm9yOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQ6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHRydWUpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmxIZWxwZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBnZXRRdWVyeVN0cmluZ1BhcmFtOiBzaW5vbi5zdHViKCkucmV0dXJucyhcIjEyMzQ1XCIpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluaXQgPSBzaW5vbi5zcHkoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KENhcHR1cmVEZWFsVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yLFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdCxcclxuICAgICAgICAgICAgICAgICAgICB1cmxIZWxwZXJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3RcclxuICAgICAgICAgICAgd3JhcHBlci52bS5zdWJtaXQoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKGFsZXJ0c01hbmFnZXIuc2hvd0Rhbmdlci5jYWxsZWQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbn0pO1xyXG4iLCIvLyBEZWJ1Z2dpbmcgZG9lcyBub3Qgd29yayBpZiBtb2NoYSBpcyByZXF1aXJlZC5cclxuLy8gdmFyIG1vY2hhID0gcmVxdWlyZShcIm1vY2hhXCIpO1xyXG4vLyB2YXIgZGVzY3JpYmUgPSBtb2NoYS5kZXNjcmliZTtcclxuLy8gdmFyIGl0ID0gbW9jaGEuaXQ7XHJcblxyXG52YXIgY2hhaSA9IHJlcXVpcmUoXCJjaGFpXCIpO1xyXG52YXIgYXNzZXJ0ID0gY2hhaS5hc3NlcnQ7XHJcblxyXG52YXIgc2lub24gPSByZXF1aXJlKFwic2lub25cIik7XHJcblxyXG5pbXBvcnQgVnVlIGZyb20gJ3Z1ZSc7XHJcbmltcG9ydCB7IG1vdW50IH0gZnJvbSBcIkB2dWUvdGVzdC11dGlsc1wiO1xyXG5pbXBvcnQgRWRpdFZlaGljbGVDb3JlIGZyb20gXCIuLi8uLi9zcmMvdmlld21vZGVscy9FZGl0VmVoaWNsZUNvcmUudnVlXCI7XHJcblxyXG5kZXNjcmliZShcIkVkaXRWZWhpY2xlQ29yZS52dWVcIiwgKCkgPT4ge1xyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBtb3VudFwiLCAoKSA9PiB7XHJcbiAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgIGNvbnN0IGFsZXJ0c01hbmFnZXIgPSB7XHJcbiAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgc2hvd0Rhbmdlcjogc2lub24uc3B5KClcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgIHVwZGF0ZVZlaGljbGVDb3JlOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IHZhbGlkYXRvckZhY3RvcnkgPSB7XHJcbiAgICAgICAgICAgIG1ha2VWYWxpZGF0b3I6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgbGV0IHdyYXBwZXIgPSBtb3VudChFZGl0VmVoaWNsZUNvcmUsIHtcclxuICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgIHZhbGlkYXRvckZhY3RvcnlcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgcHJvcHNEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICBjb3JlVG9FZGl0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiQ29kZVwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHRRdWFudGl0eTogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFByaWNlUGVyVW5pdDogMTAuMDBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICBhc3NlcnQuaXNOb3ROdWxsKHdyYXBwZXIudm0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgZGVzY3JpYmUoXCJkYXRhOiBjb3JlXCIsICgpID0+IHtcclxuICAgICAgICBpdChcInNob3VsZCBjb3JyZWN0bHkgYmUgaW5pdGlhbGl6ZWRcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGNvbnN0IGFsZXJ0c01hbmFnZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBzaG93U3VjY2Vzczogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgICAgICBzaG93RGFuZ2VyOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgdXBkYXRlVmVoaWNsZUNvcmU6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3JGYWN0b3J5ID0ge1xyXG4gICAgICAgICAgICAgICAgbWFrZVZhbGlkYXRvcjogc2lub24uc3B5KClcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gbW91bnQoRWRpdFZlaGljbGVDb3JlLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yRmFjdG9yeVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHByb3BzRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvcmVUb0VkaXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiQ09ERVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJDb2RlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRRdWFudGl0eTogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmljZVBlclVuaXQ6IDEwLjAwXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3JlLmlkLCAxKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uY29yZS5jb2RlLCBcImNvZGVcIik7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmNvcmUuZGVzY3JpcHRpb24sIFwiQ29kZVwiKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uY29yZS5kZWZhdWx0UXVhbnRpdHksIDEwKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uY29yZS5kZWZhdWx0UHJpY2VQZXJVbml0LCAxMC4wMCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBkZXNjcmliZShcIm1ldGhvZDogc2F2ZVwiLCAoKSA9PiB7XHJcbiAgICAgICAgaXQoXCJzaG91bGQgZG8gbm90aGluZyB3aGVuIG5vdCB2YWxpZFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcbiAgICBcclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgdXBkYXRlVmVoaWNsZUNvcmU6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3JGYWN0b3J5ID0ge1xyXG4gICAgICAgICAgICAgICAgbWFrZVZhbGlkYXRvcjogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIGlzVmFsaWQ6ICgpID0+IGZhbHNlXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBtb3VudChFZGl0VmVoaWNsZUNvcmUsIHtcclxuICAgICAgICAgICAgICAgIHByb3ZpZGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGVydHNNYW5hZ2VyLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3JGYWN0b3J5XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgcHJvcHNEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29yZVRvRWRpdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIkNvZGVcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFF1YW50aXR5OiAxMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFByaWNlUGVyVW5pdDogMTAuMDBcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0XHJcbiAgICAgICAgICAgIHdyYXBwZXIudm0uc2F2ZSgpO1xyXG5cclxuICAgICAgICAgICAgLy8gQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUoYXBpQ2xpZW50LnVwZGF0ZVZlaGljbGVDb3JlLm5vdENhbGxlZCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGl0KFwic2hvdWxkIGNhbGwgdGhlIGFwaSB3aGVuIHZhbGlkXCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBjb25zdCBhbGVydHNNYW5hZ2VyID0ge1xyXG4gICAgICAgICAgICAgICAgc2hvd1N1Y2Nlc3M6IHNpbm9uLnNweSgpLFxyXG4gICAgICAgICAgICAgICAgc2hvd0Rhbmdlcjogc2lub24uc3B5KClcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgICAgIHVwZGF0ZVZlaGljbGVDb3JlOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHZhbGlkYXRvckZhY3RvcnkgPSB7XHJcbiAgICAgICAgICAgICAgICBtYWtlVmFsaWRhdG9yOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNWYWxpZDogKCkgPT4gdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gbW91bnQoRWRpdFZlaGljbGVDb3JlLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yRmFjdG9yeVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHByb3BzRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvcmVUb0VkaXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiQ09ERVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJDb2RlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRRdWFudGl0eTogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmljZVBlclVuaXQ6IDEwLjAwXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdFxyXG4gICAgICAgICAgICB3cmFwcGVyLnZtLnNhdmUoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKGFwaUNsaWVudC51cGRhdGVWZWhpY2xlQ29yZS5jYWxsZWRPbmNlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgc2hvdyBzdWNjZXNzIGFsZXJ0IHdoZW4gcmVzcG9uc2UgaXMgMjAwXCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBjb25zdCBhbGVydHNNYW5hZ2VyID0ge1xyXG4gICAgICAgICAgICAgICAgc2hvd1N1Y2Nlc3M6IHNpbm9uLnNweSgpLFxyXG4gICAgICAgICAgICAgICAgc2hvd0Rhbmdlcjogc2lub24uc3B5KClcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgICAgIHVwZGF0ZVZlaGljbGVDb3JlOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCB2YWxpZGF0b3JGYWN0b3J5ID0ge1xyXG4gICAgICAgICAgICAgICAgbWFrZVZhbGlkYXRvcjogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIGlzVmFsaWQ6ICgpID0+IHRydWVcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IG1vdW50KEVkaXRWZWhpY2xlQ29yZSwge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0c01hbmFnZXIsXHJcbiAgICAgICAgICAgICAgICAgICAgYXBpQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIHZhbGlkYXRvckZhY3RvcnlcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBwcm9wc0RhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBjb3JlVG9FZGl0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcIkNPREVcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiQ29kZVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UXVhbnRpdHk6IDEwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UHJpY2VQZXJVbml0OiAxMC4wMFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3RcclxuICAgICAgICAgICAgd3JhcHBlci52bS5zYXZlKCk7XHJcblxyXG4gICAgICAgICAgICAvLyBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmlzVHJ1ZShhcGlDbGllbnQudXBkYXRlVmVoaWNsZUNvcmUuY2FsbGVkT25jZSk7XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUoYWxlcnRzTWFuYWdlci5zaG93U3VjY2Vzcy5jYWxsZWRPbmNlKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmlzVHJ1ZShhbGVydHNNYW5hZ2VyLnNob3dEYW5nZXIubm90Q2FsbGVkKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgc2hvdyBkYW5nZXIgYWxlcnQgd2hlbiByZXNwb25zZSBpcyA0MDBcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGNvbnN0IGFsZXJ0c01hbmFnZXIgPSB7XHJcbiAgICAgICAgICAgICAgICBzaG93U3VjY2Vzczogc2lub24uc3B5KCksXHJcbiAgICAgICAgICAgICAgICBzaG93RGFuZ2VyOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgdXBkYXRlVmVoaWNsZUNvcmU6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiA0MDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHZhbGlkYXRvckZhY3RvcnkgPSB7XHJcbiAgICAgICAgICAgICAgICBtYWtlVmFsaWRhdG9yOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNWYWxpZDogKCkgPT4gdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gbW91bnQoRWRpdFZlaGljbGVDb3JlLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWxlcnRzTWFuYWdlcixcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yRmFjdG9yeVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHByb3BzRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvcmVUb0VkaXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiQ09ERVwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJDb2RlXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRRdWFudGl0eTogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmljZVBlclVuaXQ6IDEwLjAwXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdFxyXG4gICAgICAgICAgICB3cmFwcGVyLnZtLnNhdmUoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKGFwaUNsaWVudC51cGRhdGVWZWhpY2xlQ29yZS5jYWxsZWRPbmNlKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmlzVHJ1ZShhbGVydHNNYW5hZ2VyLnNob3dTdWNjZXNzLm5vdENhbGxlZCk7XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUoYWxlcnRzTWFuYWdlci5zaG93RGFuZ2VyLmNhbGxlZE9uY2UpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpdChcInNob3VsZCBzaG93IGRhbmdlciBhbGVydCB3aGVuIHJlc3BvbnNlIGlzIDQwMVwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYWxlcnRzTWFuYWdlciA9IHtcclxuICAgICAgICAgICAgICAgIHNob3dTdWNjZXNzOiBzaW5vbi5zcHkoKSxcclxuICAgICAgICAgICAgICAgIHNob3dEYW5nZXI6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcGlDbGllbnQgPSB7XHJcbiAgICAgICAgICAgICAgICB1cGRhdGVWZWhpY2xlQ29yZTogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoZW46IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IDQwMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdmFsaWRhdG9yRmFjdG9yeSA9IHtcclxuICAgICAgICAgICAgICAgIG1ha2VWYWxpZGF0b3I6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICBpc1ZhbGlkOiAoKSA9PiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBtb3VudChFZGl0VmVoaWNsZUNvcmUsIHtcclxuICAgICAgICAgICAgICAgIHByb3ZpZGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGVydHNNYW5hZ2VyLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3JGYWN0b3J5XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgcHJvcHNEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29yZVRvRWRpdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIkNvZGVcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFF1YW50aXR5OiAxMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFByaWNlUGVyVW5pdDogMTAuMDBcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0XHJcbiAgICAgICAgICAgIHdyYXBwZXIudm0uc2F2ZSgpO1xyXG5cclxuICAgICAgICAgICAgLy8gQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUoYXBpQ2xpZW50LnVwZGF0ZVZlaGljbGVDb3JlLmNhbGxlZE9uY2UpO1xyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKGFsZXJ0c01hbmFnZXIuc2hvd1N1Y2Nlc3Mubm90Q2FsbGVkKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmlzVHJ1ZShhbGVydHNNYW5hZ2VyLnNob3dEYW5nZXIuY2FsbGVkT25jZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGl0KFwic2hvdWxkIHNob3cgZGFuZ2VyIGFsZXJ0IHdoZW4gdGhlcmUncyBhIGZldGNoIGVycm9yXCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBjb25zdCBhbGVydHNNYW5hZ2VyID0ge1xyXG4gICAgICAgICAgICAgICAgc2hvd1N1Y2Nlc3M6IHNpbm9uLnNweSgpLFxyXG4gICAgICAgICAgICAgICAgc2hvd0Rhbmdlcjogc2lub24uc3B5KClcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgICAgIHVwZGF0ZVZlaGljbGVDb3JlOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IChjYWxsYmFjaykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKFwibW9jayBlcnJvclwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3QgdmFsaWRhdG9yRmFjdG9yeSA9IHtcclxuICAgICAgICAgICAgICAgIG1ha2VWYWxpZGF0b3I6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICBpc1ZhbGlkOiAoKSA9PiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBtb3VudChFZGl0VmVoaWNsZUNvcmUsIHtcclxuICAgICAgICAgICAgICAgIHByb3ZpZGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBhbGVydHNNYW5hZ2VyLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3JGYWN0b3J5XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgcHJvcHNEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29yZVRvRWRpdDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJDT0RFXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcIkNvZGVcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFF1YW50aXR5OiAxMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdFByaWNlUGVyVW5pdDogMTAuMDBcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0XHJcbiAgICAgICAgICAgIHdyYXBwZXIudm0uc2F2ZSgpO1xyXG5cclxuICAgICAgICAgICAgLy8gQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUoYXBpQ2xpZW50LnVwZGF0ZVZlaGljbGVDb3JlLmNhbGxlZE9uY2UpO1xyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKGFsZXJ0c01hbmFnZXIuc2hvd1N1Y2Nlc3Mubm90Q2FsbGVkKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmlzVHJ1ZShhbGVydHNNYW5hZ2VyLnNob3dEYW5nZXIuY2FsbGVkT25jZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxufSk7XHJcbiIsIi8vIERlYnVnZ2luZyBkb2VzIG5vdCB3b3JrIGlmIG1vY2hhIGlzIHJlcXVpcmVkLlxyXG4vLyB2YXIgbW9jaGEgPSByZXF1aXJlKFwibW9jaGFcIik7XHJcbi8vIHZhciBkZXNjcmliZSA9IG1vY2hhLmRlc2NyaWJlO1xyXG4vLyB2YXIgaXQgPSBtb2NoYS5pdDtcclxuXHJcbnZhciBjaGFpID0gcmVxdWlyZShcImNoYWlcIik7XHJcbnZhciBhc3NlcnQgPSBjaGFpLmFzc2VydDtcclxuXHJcbnZhciBzaW5vbiA9IHJlcXVpcmUoXCJzaW5vblwiKTtcclxuXHJcbmltcG9ydCBWdWUgZnJvbSAndnVlJztcclxuaW1wb3J0IHsgc2hhbGxvd01vdW50IH0gZnJvbSBcIkB2dWUvdGVzdC11dGlsc1wiO1xyXG5pbXBvcnQgTWFuYWdlVmVoaWNsZUNvcmVzIGZyb20gXCIuLi8uLi9zcmMvdmlld21vZGVscy9NYW5hZ2VWZWhpY2xlQ29yZXMudnVlXCI7XHJcblxyXG5kZXNjcmliZShcIk1hbmFnZVZlaGljbGVDb3Jlcy52dWVcIiwgKCkgPT4ge1xyXG4gICAgaXQoXCJzaG91bGQgYmUgYWJsZSB0byBtb3VudFwiLCAoKSA9PiB7XHJcbiAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgIHRoZW46IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc3Qgb25Jbml0ID0gc2lub24uc3B5KCk7XHJcblxyXG4gICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KE1hbmFnZVZlaGljbGVDb3Jlcywge1xyXG4gICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICBvbkluaXRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICBhc3NlcnQuaXNOb3ROdWxsKHdyYXBwZXIudm0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgZGVzY3JpYmUoXCJkYXRhXCIsICgpID0+IHtcclxuICAgICAgICBpdChcInNob3VsZCBpbml0aWFsaXplIGNvcnJlY3RseVwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoOiBzaW5vbi5zcHkoKVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgXHJcbiAgICAgICAgICAgIGNvbnN0IG9uSW5pdCA9IHNpbm9uLnNweSgpO1xyXG4gICAgXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KE1hbmFnZVZlaGljbGVDb3Jlcywge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgICAgICBvbkluaXRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICBcclxuICAgICAgICAgICAgLy8gQWN0ICYgQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc0ZhbHNlKHdyYXBwZXIudm0uaXNJbml0RXJyb3IpO1xyXG4gICAgICAgICAgICBhc3NlcnQuaXNFbXB0eSh3cmFwcGVyLnZtLmluaXRFcnJvck1lc3NhZ2UpO1xyXG4gICAgICAgICAgICBhc3NlcnQuaXNGYWxzZSh3cmFwcGVyLnZtLmlzTG9hZGluZ1JlcXVlc3QpO1xyXG4gICAgICAgICAgICBhc3NlcnQuaXNFbXB0eSh3cmFwcGVyLnZtLmNvcmVzKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuICAgIGRlc2NyaWJlKFwiY3JlYXRlZFwiLCAoKSA9PiB7XHJcbiAgICAgICAgaXQoXCJzaG91bGQgYXNzaWduIGNvcmVzIG9uIHJlc3BvbnNlIDIwMFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAyMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3JlczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsgY29kZTogXCJhXCIgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7IGNvZGU6IFwiYlwiIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyBjb2RlOiBcImNcIiB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG9uSW5pdCA9IHNpbm9uLnNweSgpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBzaGFsbG93TW91bnQoTWFuYWdlVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXBpQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3Jlcy5sZW5ndGgsIDMpO1xyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5jb3Jlc1swXS5jb2RlLCBcImFcIik7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmNvcmVzWzFdLmNvZGUsIFwiYlwiKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uY29yZXNbMl0uY29kZSwgXCJjXCIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpdChcInNob3VsZCBjYWxsIG9uSW5pdCBvbiByZXNwb25zZSAyMDBcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgICAgIGdldENvbmZpZ3VyYXRpb25EYXRhOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogMjAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG9uSW5pdCA9IHNpbm9uLnNweSgpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBzaGFsbG93TW91bnQoTWFuYWdlVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXBpQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKG9uSW5pdC5jYWxsZWRPbmNlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgYWxlcnQgb24gcmVzcG9uc2Ugbm90IDIwMFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IHNpbm9uLnN0dWIoKS5yZXR1cm5zKHtcclxuICAgICAgICAgICAgICAgICAgICB0aGVuOiAoY2FsbGJhY2spID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2soe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiA0MDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogc2lub24uc3B5KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgY29uc3Qgb25Jbml0ID0gc2lub24uc3B5KCk7XHJcblxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IHNoYWxsb3dNb3VudChNYW5hZ2VWZWhpY2xlQ29yZXMsIHtcclxuICAgICAgICAgICAgICAgIHByb3ZpZGU6IHtcclxuICAgICAgICAgICAgICAgICAgICBhcGlDbGllbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgb25Jbml0XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0ICYgQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5pc1RydWUod3JhcHBlci52bS5pc0luaXRFcnJvcik7XHJcbiAgICAgICAgICAgIGFzc2VydC5pc05vdEVtcHR5KHdyYXBwZXIudm0uaW5pdEVycm9yTWVzc2FnZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGl0KFwic2hvdWxkIGNhbGwgb25Jbml0IG9uIHJlc3BvbnNlIG5vdCAyMDBcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGNvbnN0IGFwaUNsaWVudCA9IHtcclxuICAgICAgICAgICAgICAgIGdldENvbmZpZ3VyYXRpb25EYXRhOiBzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogNDAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2g6IHNpbm9uLnNweSgpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IG9uSW5pdCA9IHNpbm9uLnNweSgpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBzaGFsbG93TW91bnQoTWFuYWdlVmVoaWNsZUNvcmVzLCB7XHJcbiAgICAgICAgICAgICAgICBwcm92aWRlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXBpQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIG9uSW5pdFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuaXNUcnVlKG9uSW5pdC5jYWxsZWRPbmNlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgYWxlcnQgb24gbmV0d29yayBlcnJvclwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IChzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhcIm1vY2sgZXJyb3JcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSkpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluaXQgPSBzaW5vbi5zcHkoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KE1hbmFnZVZlaGljbGVDb3Jlcywge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgICAgICBvbkluaXRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmlzVHJ1ZSh3cmFwcGVyLnZtLmlzSW5pdEVycm9yKTtcclxuICAgICAgICAgICAgYXNzZXJ0LmlzTm90RW1wdHkod3JhcHBlci52bS5pbml0RXJyb3JNZXNzYWdlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgY2FsbCBvbkluaXQgb24gbmV0d29yayBlcnJvclwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgY29uc3QgYXBpQ2xpZW50ID0ge1xyXG4gICAgICAgICAgICAgICAgZ2V0Q29uZmlndXJhdGlvbkRhdGE6IChzaW5vbi5zdHViKCkucmV0dXJucyh7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhlbjogc2lub24uc3R1YigpLnJldHVybnMoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaDogKGNhbGxiYWNrKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhcIm1vY2sgZXJyb3JcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfSkpXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBvbkluaXQgPSBzaW5vbi5zcHkoKTtcclxuXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gc2hhbGxvd01vdW50KE1hbmFnZVZlaGljbGVDb3Jlcywge1xyXG4gICAgICAgICAgICAgICAgcHJvdmlkZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGFwaUNsaWVudCxcclxuICAgICAgICAgICAgICAgICAgICBvbkluaXRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmlzVHJ1ZShvbkluaXQuY2FsbGVkT25jZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcblxyXG59KTtcclxuIiwiLy8gRGVidWdnaW5nIGRvZXMgbm90IHdvcmsgaWYgbW9jaGEgaXMgcmVxdWlyZWQuXHJcbi8vIHZhciBtb2NoYSA9IHJlcXVpcmUoXCJtb2NoYVwiKTtcclxuLy8gdmFyIGRlc2NyaWJlID0gbW9jaGEuZGVzY3JpYmU7XHJcbi8vIHZhciBpdCA9IG1vY2hhLml0O1xyXG5cclxudmFyIGNoYWkgPSByZXF1aXJlKFwiY2hhaVwiKTtcclxudmFyIGFzc2VydCA9IGNoYWkuYXNzZXJ0O1xyXG5cclxuaW1wb3J0IFZ1ZSBmcm9tICd2dWUnO1xyXG5pbXBvcnQgeyBtb3VudCB9IGZyb20gXCJAdnVlL3Rlc3QtdXRpbHNcIjtcclxuaW1wb3J0IFZlaGljbGVDb3JlIGZyb20gXCIuLi8uLi9zcmMvdmlld21vZGVscy9WZWhpY2xlQ29yZS52dWVcIjtcclxuXHJcbmRlc2NyaWJlKFwiVmVoaWNsZUNvcmUudnVlXCIsICgpID0+IHtcclxuICAgIGl0KFwic2hvdWxkIGJlIGFibGUgdG8gbW91bnRcIiwgKCkgPT4ge1xyXG4gICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICBsZXQgd3JhcHBlciA9IG1vdW50KFZlaGljbGVDb3JlLCB7XHJcbiAgICAgICAgICAgIHByb3BzRGF0YToge1xyXG4gICAgICAgICAgICAgICAgY29yZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlzRXh0cmFjdGVkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiAxNSxcclxuICAgICAgICAgICAgICAgICAgICBwcmljZVBlclVuaXQ6IDEwLjAwLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiYXNkYXNkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiYWRhc2Rhc2RcIlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgIGFzc2VydC5pc05vdE51bGwod3JhcHBlci52bSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBkZXNjcmliZShcImRhdGE6IGlzRXh0cmFjdGVkXCIsICgpID0+IHtcclxuICAgICAgICBpdChcInNob3VsZCBiZSB5ZXMgd2hlbiBpbmNvbWluZyBwcm9wIGlzIHRydWVcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gbW91bnQoVmVoaWNsZUNvcmUsIHtcclxuICAgICAgICAgICAgICAgIHByb3BzRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvcmU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNFeHRyYWN0ZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiAxNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpY2VQZXJVbml0OiAxMC4wMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJhc2Rhc2RcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiYWRhc2Rhc2RcIlxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0uaXNFeHRyYWN0ZWQsIFwieWVzXCIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpdChcInNob3VsZCBiZSBubyB3aGVuIGluY29taW5nIHByb3AgaXMgZmFsc2VcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gbW91bnQoVmVoaWNsZUNvcmUsIHtcclxuICAgICAgICAgICAgICAgIHByb3BzRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvcmU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNFeHRyYWN0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWFudGl0eTogMTUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaWNlUGVyVW5pdDogMTAuMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiYXNkYXNkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcImFkYXNkYXNkXCJcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0ICYgQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLmlzRXh0cmFjdGVkLCBcIm5vXCIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpdChcInNob3VsZCBiZSBubyB3aGVuIGluY29taW5nIHByb3AgaXMgbnVsbFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBtb3VudChWZWhpY2xlQ29yZSwge1xyXG4gICAgICAgICAgICAgICAgcHJvcHNEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29yZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0V4dHJhY3RlZDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IDE1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmljZVBlclVuaXQ6IDEwLjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcImFzZGFzZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJhZGFzZGFzZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS5pc0V4dHJhY3RlZCwgXCJub1wiKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuICAgIGRlc2NyaWJlKFwiY29tcHV0ZWQ6IHRvdGFsUHJpY2VcIiwgKCkgPT4ge1xyXG4gICAgICAgIGl0KFwic2hvdWxkIGJlIGNhbGN1bGF0ZWQgaW5pdGlhbGx5XCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IG1vdW50KFZlaGljbGVDb3JlLCB7XHJcbiAgICAgICAgICAgICAgICBwcm9wc0RhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBjb3JlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRXh0cmFjdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IDE1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmljZVBlclVuaXQ6IDEwLjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcImFzZGFzZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJhZGFzZGFzZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIFxyXG4gICAgICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0udG90YWxQcmljZSwgMTUwKTtcclxuICAgICAgICB9KTtcclxuICAgIFxyXG4gICAgICAgIGl0KFwic2hvdWxkIGJlIGNhbGN1bGF0ZWQgcmVhY3RpdmVseVwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBtb3VudChWZWhpY2xlQ29yZSwge1xyXG4gICAgICAgICAgICAgICAgcHJvcHNEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29yZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0V4dHJhY3RlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiAxNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpY2VQZXJVbml0OiAxMC4wMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJhc2Rhc2RcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiYWRhc2Rhc2RcIlxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICBcclxuICAgICAgICAgICAgLy8gQWN0XHJcbiAgICAgICAgICAgIHdyYXBwZXIuc2V0UHJvcHMoe1xyXG4gICAgICAgICAgICAgICAgY29yZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlzRXh0cmFjdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBxdWFudGl0eTogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpY2VQZXJVbml0OiAxMC4wMCxcclxuICAgICAgICAgICAgICAgICAgICBjb2RlOiBcImFzZGFzZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcImFkYXNkYXNkXCJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICBcclxuICAgICAgICAgICAgLy8gQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLnRvdGFsUHJpY2UsIDEwMCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGl0KFwic2hvdWxkIGJlIHplcm8gd2hlbiBxdWFudGl0eSBtaXNzaW5nXCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IG1vdW50KFZlaGljbGVDb3JlLCB7XHJcbiAgICAgICAgICAgICAgICBwcm9wc0RhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBjb3JlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRXh0cmFjdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaWNlUGVyVW5pdDogMTAuMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiYXNkYXNkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcImFkYXNkYXNkXCJcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgXHJcbiAgICAgICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS50b3RhbFByaWNlLCAwLjAwKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgYmUgemVybyB3aGVuIHByaWNlUGVyVW5pdCBtaXNzaW5nXCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IG1vdW50KFZlaGljbGVDb3JlLCB7XHJcbiAgICAgICAgICAgICAgICBwcm9wc0RhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBjb3JlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRXh0cmFjdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IDEwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmljZVBlclVuaXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiYXNkYXNkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcImFkYXNkYXNkXCJcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgXHJcbiAgICAgICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS50b3RhbFByaWNlLCAwLjAwKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgYmUgemVybyB3aGVuIHF1YW50aXR5IGFuZCBwcmljZVBlclVuaXQgbWlzc2luZ1wiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBtb3VudChWZWhpY2xlQ29yZSwge1xyXG4gICAgICAgICAgICAgICAgcHJvcHNEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29yZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0V4dHJhY3RlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiAxMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpY2VQZXJVbml0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcImFzZGFzZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJhZGFzZGFzZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIFxyXG4gICAgICAgICAgICAvLyBBY3QgJiBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0udG90YWxQcmljZSwgMC4wMCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBkZXNjcmliZShcImNvbXB1dGVkOiB0b3RhbFByaWNlRGlzcGxheVwiLCAoKSA9PiB7XHJcbiAgICAgICAgaXQoXCJzaG91bGQgYmUgY2FsY3VsYXRlZCBpbml0aWFsbHlcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gbW91bnQoVmVoaWNsZUNvcmUsIHtcclxuICAgICAgICAgICAgICAgIHByb3BzRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvcmU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNFeHRyYWN0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWFudGl0eTogMTUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaWNlUGVyVW5pdDogMTAuMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiYXNkYXNkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcImFkYXNkYXNkXCJcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgXHJcbiAgICAgICAgICAgIC8vIEFjdCAmIEFzc2VydFxyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci52bS50b3RhbFByaWNlRGlzcGxheSwgXCIxNTAuMDBcIik7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGl0KFwic2hvdWxkIGJlIGNhbGN1bGF0ZWQgcmVhY3RpdmVseVwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBtb3VudChWZWhpY2xlQ29yZSwge1xyXG4gICAgICAgICAgICAgICAgcHJvcHNEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29yZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0V4dHJhY3RlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiAxNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpY2VQZXJVbml0OiAxMC4wMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJhc2Rhc2RcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiYWRhc2Rhc2RcIlxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICBcclxuICAgICAgICAgICAgLy8gQWN0XHJcbiAgICAgICAgICAgIHdyYXBwZXIuc2V0UHJvcHMoe1xyXG4gICAgICAgICAgICAgICAgY29yZToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlzRXh0cmFjdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBxdWFudGl0eTogMTAsXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpY2VQZXJVbml0OiAxMC4wMCxcclxuICAgICAgICAgICAgICAgICAgICBjb2RlOiBcImFzZGFzZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcImFkYXNkYXNkXCJcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICBcclxuICAgICAgICAgICAgLy8gQXNzZXJ0XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLnZtLnRvdGFsUHJpY2VEaXNwbGF5LCBcIjEwMC4wMFwiKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaXQoXCJzaG91bGQgYmUgemVybyB3aGVuIG5vIHRvdGFsUHJpY2VcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gbW91bnQoVmVoaWNsZUNvcmUsIHtcclxuICAgICAgICAgICAgICAgIHByb3BzRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvcmU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNFeHRyYWN0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWFudGl0eTogMTUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaWNlUGVyVW5pdDogMTAuMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiYXNkYXNkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcImFkYXNkYXNkXCJcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgXHJcbiAgICAgICAgICAgIC8vIEFjdFxyXG4gICAgICAgICAgICB3cmFwcGVyLnNldFByb3BzKHtcclxuICAgICAgICAgICAgICAgIGNvcmU6IHtcclxuICAgICAgICAgICAgICAgICAgICBpc0V4dHJhY3RlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgcHJpY2VQZXJVbml0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiYXNkYXNkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiYWRhc2Rhc2RcIlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIFxyXG4gICAgICAgICAgICAvLyBBc3NlcnRcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIudm0udG90YWxQcmljZURpc3BsYXksIFwiMC4wMFwiKTtcclxuICAgICAgICB9KTtcclxuICAgIH0pO1xyXG5cclxuICAgIGRlc2NyaWJlKFwibWV0aG9kOiBpc0V4dHJhY3RlZFllc09uQ2xpY2tcIiwgKCkgPT4ge1xyXG4gICAgICAgIGl0KFwic2hvdWxkIGVtaXQgY2hhbmdlLWlzLWV4dHJhY3RlZCB3aXRoIGEgdHJ1ZSBhcmd1bWVudFwiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIEFycmFuZ2VcclxuICAgICAgICAgICAgbGV0IHdyYXBwZXIgPSBtb3VudChWZWhpY2xlQ29yZSwge1xyXG4gICAgICAgICAgICAgICAgcHJvcHNEYXRhOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29yZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0V4dHJhY3RlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1YW50aXR5OiAxNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJpY2VQZXJVbml0OiAxMC4wMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29kZTogXCJhc2Rhc2RcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiYWRhc2Rhc2RcIlxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBY3RcclxuICAgICAgICAgICAgd3JhcHBlci52bS5pc0V4dHJhY3RlZFllc09uQ2xpY2soKTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFzc2VydFxyXG4gICAgICAgICAgICAvLyB0aGUgZXZlbnQgd2FzIGVtaXR0ZWRcclxuICAgICAgICAgICAgYXNzZXJ0LmV4aXN0cyh3cmFwcGVyLmVtaXR0ZWQoKVtcImNoYW5nZS1pcy1leHRyYWN0ZWRcIl0pOyBcclxuICAgICAgICAgICAgLy8gdGltZXMgdGhhdCB0aGUgZXZlbnQgd2FzIGVtaXR0ZWRcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIuZW1pdHRlZCgpW1wiY2hhbmdlLWlzLWV4dHJhY3RlZFwiXS5sZW5ndGgsIDEpOyBcclxuXHJcbiAgICAgICAgICAgIC8vIHBsYXlsb2FkIHdpdGggd2hpY2ggdGhlIGV2ZW50IHdhcyBlbWl0dGVkXHJcbiAgICAgICAgICAgIGxldCBmaXJzdENhbGwgPSAwO1xyXG4gICAgICAgICAgICBsZXQgZmlyc3RQYXlsb2FkQXJnID0gMDtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIuZW1pdHRlZCgpW1wiY2hhbmdlLWlzLWV4dHJhY3RlZFwiXVtmaXJzdENhbGxdW2ZpcnN0UGF5bG9hZEFyZ10sIHRydWUpOyBcclxuXHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBkZXNjcmliZShcIm1ldGhvZDogaXNFeHRyYWN0ZWROb09uQ2xpY2tcIiwgKCkgPT4ge1xyXG4gICAgICAgIGl0KFwic2hvdWxkIGVtaXQgY2hhbmdlLWlzLWV4dHJhY3RlZCB3aXRoIGEgZmFsc2UgYXJndW1lbnRcIiwgKCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBBcnJhbmdlXHJcbiAgICAgICAgICAgIGxldCB3cmFwcGVyID0gbW91bnQoVmVoaWNsZUNvcmUsIHtcclxuICAgICAgICAgICAgICAgIHByb3BzRGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvcmU6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNFeHRyYWN0ZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWFudGl0eTogMTUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHByaWNlUGVyVW5pdDogMTAuMDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvZGU6IFwiYXNkYXNkXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBcImFkYXNkYXNkXCJcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQWN0XHJcbiAgICAgICAgICAgIHdyYXBwZXIudm0uaXNFeHRyYWN0ZWROb09uQ2xpY2soKTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFzc2VydFxyXG4gICAgICAgICAgICAvLyB0aGUgZXZlbnQgd2FzIGVtaXR0ZWRcclxuICAgICAgICAgICAgYXNzZXJ0LmV4aXN0cyh3cmFwcGVyLmVtaXR0ZWQoKVtcImNoYW5nZS1pcy1leHRyYWN0ZWRcIl0pOyBcclxuICAgICAgICAgICAgLy8gdGltZXMgdGhhdCB0aGUgZXZlbnQgd2FzIGVtaXR0ZWRcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIuZW1pdHRlZCgpW1wiY2hhbmdlLWlzLWV4dHJhY3RlZFwiXS5sZW5ndGgsIDEpOyBcclxuXHJcbiAgICAgICAgICAgIC8vIHBsYXlsb2FkIHdpdGggd2hpY2ggdGhlIGV2ZW50IHdhcyBlbWl0dGVkXHJcbiAgICAgICAgICAgIGxldCBmaXJzdENhbGwgPSAwO1xyXG4gICAgICAgICAgICBsZXQgZmlyc3RQYXlsb2FkQXJnID0gMDtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIuZW1pdHRlZCgpW1wiY2hhbmdlLWlzLWV4dHJhY3RlZFwiXVtmaXJzdENhbGxdW2ZpcnN0UGF5bG9hZEFyZ10sIGZhbHNlKTsgXHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgZGVzY3JpYmUoXCJtZXRob2Q6IHF1YW50aXR5T25JbnB1dFwiLCAoKSA9PiB7XHJcbiAgICAgICAgaXQoXCJzaG91bGQgZW1pdCBpbnB1dC1xdWFudGl0eSB3aXRoIGdpdmVuIGFyZ3VtZW50XCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IG1vdW50KFZlaGljbGVDb3JlLCB7XHJcbiAgICAgICAgICAgICAgICBwcm9wc0RhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBjb3JlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRXh0cmFjdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IDE1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmljZVBlclVuaXQ6IDEwLjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcImFzZGFzZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJhZGFzZGFzZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdFxyXG4gICAgICAgICAgICB3cmFwcGVyLnZtLnF1YW50aXR5T25JbnB1dCh7XHJcbiAgICAgICAgICAgICAgICB0YXJnZXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogMTBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBBc3NlcnRcclxuICAgICAgICAgICAgLy8gdGhlIGV2ZW50IHdhcyBlbWl0dGVkXHJcbiAgICAgICAgICAgIGFzc2VydC5leGlzdHMod3JhcHBlci5lbWl0dGVkKClbXCJpbnB1dC1xdWFudGl0eVwiXSk7IFxyXG4gICAgICAgICAgICAvLyB0aW1lcyB0aGF0IHRoZSBldmVudCB3YXMgZW1pdHRlZFxyXG4gICAgICAgICAgICBhc3NlcnQuZXF1YWwod3JhcHBlci5lbWl0dGVkKClbXCJpbnB1dC1xdWFudGl0eVwiXS5sZW5ndGgsIDEpOyBcclxuXHJcbiAgICAgICAgICAgIC8vIHBsYXlsb2FkIHdpdGggd2hpY2ggdGhlIGV2ZW50IHdhcyBlbWl0dGVkXHJcbiAgICAgICAgICAgIGxldCBmaXJzdENhbGwgPSAwO1xyXG4gICAgICAgICAgICBsZXQgZmlyc3RQYXlsb2FkQXJnID0gMDtcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIuZW1pdHRlZCgpW1wiaW5wdXQtcXVhbnRpdHlcIl1bZmlyc3RDYWxsXVtmaXJzdFBheWxvYWRBcmddLCAxMCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBkZXNjcmliZShcIm1ldGhvZDogcHJpY2VQZXJVbml0T25JbnB1dFwiLCAoKSA9PiB7XHJcbiAgICAgICAgaXQoXCJzaG91bGQgZW1pdCBpbnB1dC1wcmljZS1wZXItdW5pdCB3aXRoIGdpdmVuIGFyZ3VtZW50XCIsICgpID0+IHtcclxuICAgICAgICAgICAgLy8gQXJyYW5nZVxyXG4gICAgICAgICAgICBsZXQgd3JhcHBlciA9IG1vdW50KFZlaGljbGVDb3JlLCB7XHJcbiAgICAgICAgICAgICAgICBwcm9wc0RhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBjb3JlOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRXh0cmFjdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IDE1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwcmljZVBlclVuaXQ6IDEwLjAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2RlOiBcImFzZGFzZFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXNjcmlwdGlvbjogXCJhZGFzZGFzZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFjdFxyXG4gICAgICAgICAgICB3cmFwcGVyLnZtLnByaWNlUGVyVW5pdE9uSW5wdXQoe1xyXG4gICAgICAgICAgICAgICAgdGFyZ2V0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IDEwLjAwXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQXNzZXJ0XHJcbiAgICAgICAgICAgIC8vIHRoZSBldmVudCB3YXMgZW1pdHRlZFxyXG4gICAgICAgICAgICBhc3NlcnQuZXhpc3RzKHdyYXBwZXIuZW1pdHRlZCgpW1wiaW5wdXQtcHJpY2UtcGVyLXVuaXRcIl0pOyBcclxuICAgICAgICAgICAgLy8gdGltZXMgdGhhdCB0aGUgZXZlbnQgd2FzIGVtaXR0ZWRcclxuICAgICAgICAgICAgYXNzZXJ0LmVxdWFsKHdyYXBwZXIuZW1pdHRlZCgpW1wiaW5wdXQtcHJpY2UtcGVyLXVuaXRcIl0ubGVuZ3RoLCAxKTsgXHJcblxyXG4gICAgICAgICAgICAvLyBwbGF5bG9hZCB3aXRoIHdoaWNoIHRoZSBldmVudCB3YXMgZW1pdHRlZFxyXG4gICAgICAgICAgICBsZXQgZmlyc3RDYWxsID0gMDtcclxuICAgICAgICAgICAgbGV0IGZpcnN0UGF5bG9hZEFyZyA9IDA7XHJcbiAgICAgICAgICAgIGFzc2VydC5lcXVhbCh3cmFwcGVyLmVtaXR0ZWQoKVtcImlucHV0LXByaWNlLXBlci11bml0XCJdW2ZpcnN0Q2FsbF1bZmlyc3RQYXlsb2FkQXJnXSwgMTAuMDApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcbn0pO1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAdnVlL3Rlc3QtdXRpbHNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiY2hhaVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJqcXVlcnlcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwianNkb20tZ2xvYmFsXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInNpbm9uXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInZ1ZVwiKTsiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEJBO0FBQ0E7OztBQUFBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQUNBO0FBWUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUlBO0FBRUE7QUFLQTtBQUVBO0FBS0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBakRBO0FBdkhBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzQkE7QUFDQTs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFMQTtBQWFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQTtBQUVBO0FBS0E7QUFFQTtBQUtBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQXZEQTtBQXhCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNDQTtBQUNBOzs7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE3Q0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMyQkE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFsQkE7QUFDQTtBQW9CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVpBO0FBakNBOzs7Ozs7Ozs7Ozs7QUNwRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDckpBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDcElBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN6RUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUMzS0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQzVGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFpQkE7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUN0Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBaUJBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDdENBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQWlCQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ3RDQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFpQkE7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUN0Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDSkE7QUFDQTs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7O0FBYkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQU5BO0FBQ0E7QUFZQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBQ0E7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBTkE7QUFDQTtBQVlBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBTkE7QUFDQTtBQVlBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFLQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBTkE7QUFDQTtBQVlBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFHQTtBQVZBO0FBWUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUhBO0FBQ0E7QUFZQTtBQUNBO0FBREE7QUFHQTtBQWxCQTtBQWJBO0FBQ0E7QUFrQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUhBO0FBQ0E7QUFXQTtBQUNBO0FBREE7QUFHQTtBQWpCQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBSEE7QUFDQTtBQVlBO0FBQ0E7QUFEQTtBQUdBO0FBbEJBO0FBcEJBO0FBQ0E7QUF5Q0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUhBO0FBQ0E7QUFXQTtBQUNBO0FBREE7QUFHQTtBQWpCQTtBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBSEE7QUFDQTtBQVlBO0FBQ0E7QUFEQTtBQUdBO0FBbEJBO0FBcEJBO0FBQ0E7QUF5Q0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFEQTtBQUdBO0FBVkE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBVkE7QUFDQTtBQW1CQTtBQUNBO0FBREE7QUFHQTtBQXpCQTtBQWJBO0FBQ0E7QUF5Q0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUpBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQVRBO0FBQ0E7QUFpQkE7QUFDQTtBQURBO0FBR0E7QUF2QkE7QUF5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQVZBO0FBQ0E7QUFtQkE7QUFDQTtBQURBO0FBR0E7QUF6QkE7QUExQkE7QUFDQTtBQXNEQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBQ0E7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFEQTtBQUdBO0FBVkE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBVkE7QUFDQTtBQW1CQTtBQUNBO0FBREE7QUFHQTtBQXpCQTtBQWJBO0FBQ0E7QUF5Q0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFIQTtBQUNBO0FBWUE7QUFDQTtBQURBO0FBR0E7QUFsQkE7QUFvQkE7QUFDQTtBQUNBO0FBREE7QUFEQTtBQWpDQTtBQUNBO0FBdUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFIQTtBQUNBO0FBWUE7QUFDQTtBQURBO0FBR0E7QUFsQkE7QUFvQkE7QUFDQTtBQUNBO0FBREE7QUFEQTtBQWpDQTtBQUNBO0FBdUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFIQTtBQUNBO0FBWUE7QUFDQTtBQURBO0FBR0E7QUFsQkE7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFUQTtBQWpDQTtBQUNBO0FBNkNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFIQTtBQUNBO0FBWUE7QUFDQTtBQURBO0FBR0E7QUFsQkE7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFUQTtBQWpDQTtBQUNBO0FBNkNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFIQTtBQUNBO0FBWUE7QUFDQTtBQURBO0FBR0E7QUFsQkE7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFUQTtBQWpDQTtBQUNBO0FBNkNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFWQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFIQTtBQUNBO0FBWUE7QUFDQTtBQURBO0FBR0E7QUFsQkE7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFEQTtBQWpDQTtBQUNBO0FBMENBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7O0FDN21DQTtBQUNBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7QUFiQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBTkE7QUFDQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFEQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBTkE7QUFDQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFEQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBTkE7QUFDQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBREE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFEQTtBQURBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFOQTtBQUNBO0FBZ0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBVEE7QUFEQTtBQUNBO0FBYUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBREE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQU5BO0FBQ0E7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBVEE7QUFEQTtBQUNBO0FBYUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBREE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQU5BO0FBQ0E7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBVEE7QUFEQTtBQUNBO0FBYUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQURBO0FBREE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQU5BO0FBQ0E7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFQQTtBQURBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFEQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBTkE7QUFDQTtBQWdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUM1WEE7QUFDQTs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7O0FBYkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFEQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFEQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBUUE7QUFDQTtBQURBO0FBR0E7QUFiQTtBQURBO0FBQ0E7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQUNBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFSQTtBQURBO0FBQ0E7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQVJBO0FBREE7QUFDQTtBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBREE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFSQTtBQURBO0FBQ0E7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBREE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFEQTtBQURBO0FBQ0E7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQURBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUdBOzs7Ozs7Ozs7Ozs7OztBQ3pPQTtBQUNBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7QUFYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFEQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQURBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQURBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQURBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFEQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFEQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBREE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBREE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBREE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQURBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQURBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUNBO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFEQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQURBO0FBQ0E7QUFXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBREE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFEQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBREE7QUFDQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDalpBOzs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7OztBQ0FBOzs7O0EiLCJzb3VyY2VSb290IjoiIn0=