// Debugging does not work if mocha is required.
// var mocha = require("mocha");
// var describe = mocha.describe;
// var it = mocha.it;

var chai = require("chai");
var assert = chai.assert;

var sinon = require("sinon");

import Vue from 'vue';
import { shallowMount } from "@vue/test-utils";
import ManageVehicleCores from "../../src/viewmodels/ManageVehicleCores.vue";

describe("ManageVehicleCores.vue", () => {
    it("should be able to mount", () => {
        // Arrange
        const apiClient = {
            getConfigurationData: sinon.stub().returns({
                then: sinon.stub().returns({
                    catch: sinon.spy()
                })
            })
        };

        const onInit = sinon.spy();

        let wrapper = shallowMount(ManageVehicleCores, {
            provide: {
                apiClient,
                onInit
            }
        });

        // Act & Assert
        assert.isNotNull(wrapper.vm);
    });

    describe("data", () => {
        it("should initialize correctly", () => {
            // Arrange
            const apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };
    
            const onInit = sinon.spy();
    
            let wrapper = shallowMount(ManageVehicleCores, {
                provide: {
                    apiClient,
                    onInit
                }
            });
    
            // Act & Assert
            assert.isFalse(wrapper.vm.isInitError);
            assert.isEmpty(wrapper.vm.initErrorMessage);
            assert.isFalse(wrapper.vm.isLoadingRequest);
            assert.isEmpty(wrapper.vm.cores);
        });
    });

    describe("created", () => {
        it("should assign cores on response 200", () => {
            // Arrange
            const apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                { code: "a" },
                                { code: "b" },
                                { code: "c" }
                            ]
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(ManageVehicleCores, {
                provide: {
                    apiClient,
                    onInit
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.cores.length, 3);
            assert.equal(wrapper.vm.cores[0].code, "a");
            assert.equal(wrapper.vm.cores[1].code, "b");
            assert.equal(wrapper.vm.cores[2].code, "c");
        });

        it("should call onInit on response 200", () => {
            // Arrange
            const apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(ManageVehicleCores, {
                provide: {
                    apiClient,
                    onInit
                }
            });

            // Act & Assert
            assert.isTrue(onInit.calledOnce);
        });

        it("should alert on response not 200", () => {
            // Arrange
            const apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 400
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(ManageVehicleCores, {
                provide: {
                    apiClient,
                    onInit
                }
            });

            // Act & Assert
            assert.isTrue(wrapper.vm.isInitError);
            assert.isNotEmpty(wrapper.vm.initErrorMessage);
        });

        it("should call onInit on response not 200", () => {
            // Arrange
            const apiClient = {
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 400
                        });
                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(ManageVehicleCores, {
                provide: {
                    apiClient,
                    onInit
                }
            });

            // Act & Assert
            assert.isTrue(onInit.calledOnce);
        });

        it("should alert on network error", () => {
            // Arrange
            const apiClient = {
                getConfigurationData: (sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("mock error");
                        }
                    })
                }))
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(ManageVehicleCores, {
                provide: {
                    apiClient,
                    onInit
                }
            });

            // Act & Assert
            assert.isTrue(wrapper.vm.isInitError);
            assert.isNotEmpty(wrapper.vm.initErrorMessage);
        });

        it("should call onInit on network error", () => {
            // Arrange
            const apiClient = {
                getConfigurationData: (sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("mock error");
                        }
                    })
                }))
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(ManageVehicleCores, {
                provide: {
                    apiClient,
                    onInit
                }
            });

            // Act & Assert
            assert.isTrue(onInit.calledOnce);
        });
    });


});
