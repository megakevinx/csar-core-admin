// Debugging does not work if mocha is required.
// var mocha = require("mocha");
// var describe = mocha.describe;
// var it = mocha.it;

var chai = require("chai");
var assert = chai.assert;

var sinon = require("sinon");

import Vue from 'vue';
import { mount } from "@vue/test-utils";
import EditVehicleCore from "../../src/viewmodels/EditVehicleCore.vue";

describe("EditVehicleCore.vue", () => {
    it("should be able to mount", () => {
        // Arrange
        const alertsManager = {
            showSuccess: sinon.spy(),
            showDanger: sinon.spy()
        };

        const apiClient = {
            updateVehicleCore: sinon.spy()
        };

        const validatorFactory = {
            makeValidator: sinon.spy()
        };

        let wrapper = mount(EditVehicleCore, {
            provide: {
                alertsManager,
                apiClient,
                validatorFactory
            },
            propsData: {
                coreToEdit: {
                    id: 1,
                    code: "CODE",
                    description: "Code",
                    defaultQuantity: 10,
                    defaultPricePerUnit: 10.00
                }
            }
        });

        // Act & Assert
        assert.isNotNull(wrapper.vm);
    });

    describe("data: core", () => {
        it("should correctly be initialized", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                updateVehicleCore: sinon.spy()
            };

            const validatorFactory = {
                makeValidator: sinon.spy()
            };

            let wrapper = mount(EditVehicleCore, {
                provide: {
                    alertsManager,
                    apiClient,
                    validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.core.id, 1);
            assert.equal(wrapper.vm.core.code, "code");
            assert.equal(wrapper.vm.core.description, "Code");
            assert.equal(wrapper.vm.core.defaultQuantity, 10);
            assert.equal(wrapper.vm.core.defaultPricePerUnit, 10.00);
        });
    });

    describe("method: save", () => {
        it("should do nothing when not valid", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };
    
            const apiClient = {
                updateVehicleCore: sinon.spy()
            };

            const validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: () => false
                })
            };

            let wrapper = mount(EditVehicleCore, {
                provide: {
                    alertsManager,
                    apiClient,
                    validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.notCalled);
        });

        it("should call the api when valid", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            const validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: () => true
                })
            };

            let wrapper = mount(EditVehicleCore, {
                provide: {
                    alertsManager,
                    apiClient,
                    validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
        });

        it("should show success alert when response is 200", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: () => true
                })
            };

            let wrapper = mount(EditVehicleCore, {
                provide: {
                    alertsManager,
                    apiClient,
                    validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
            assert.isTrue(alertsManager.showSuccess.calledOnce);
            assert.isTrue(alertsManager.showDanger.notCalled);
        });

        it("should show danger alert when response is 400", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 400
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: () => true
                })
            };

            let wrapper = mount(EditVehicleCore, {
                provide: {
                    alertsManager,
                    apiClient,
                    validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
            assert.isTrue(alertsManager.showSuccess.notCalled);
            assert.isTrue(alertsManager.showDanger.calledOnce);
        });

        it("should show danger alert when response is 401", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 401
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: () => true
                })
            };

            let wrapper = mount(EditVehicleCore, {
                provide: {
                    alertsManager,
                    apiClient,
                    validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
            assert.isTrue(alertsManager.showSuccess.notCalled);
            assert.isTrue(alertsManager.showDanger.calledOnce);
        });

        it("should show danger alert when there's a fetch error", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                updateVehicleCore: sinon.stub().returns({
                    then: () => {
                        return {
                            catch: (callback) => {
                                callback("mock error");
                            }
                        };
                    }
                })
            };

            const validatorFactory = {
                makeValidator: sinon.stub().returns({
                    isValid: () => true
                })
            };

            let wrapper = mount(EditVehicleCore, {
                provide: {
                    alertsManager,
                    apiClient,
                    validatorFactory
                },
                propsData: {
                    coreToEdit: {
                        id: 1,
                        code: "CODE",
                        description: "Code",
                        defaultQuantity: 10,
                        defaultPricePerUnit: 10.00
                    }
                }
            });

            // Act
            wrapper.vm.save();

            // Assert
            assert.isTrue(apiClient.updateVehicleCore.calledOnce);
            assert.isTrue(alertsManager.showSuccess.notCalled);
            assert.isTrue(alertsManager.showDanger.calledOnce);
        });
    });
});
