// Debugging does not work if mocha is required.
// var mocha = require("mocha");
// var describe = mocha.describe;
// var it = mocha.it;

var chai = require("chai");
var assert = chai.assert;

var sinon = require("sinon");

import Vue from 'vue';
import { shallowMount } from "@vue/test-utils";
import CaptureDealVehicleCores from "../../src/viewmodels/CaptureDealVehicleCores.vue";

describe("CaptureDealVehicleCores.vue", () => {
    it("should be able to mount", () => {
        // Arrange
        const alertsManager = {
            showSuccess: sinon.spy(),
            showDanger: sinon.spy()
        };

        const apiClient = {
            getDealVehicleCores: sinon.stub().returns({
                then: sinon.stub().returns({
                    catch: sinon.spy()
                })
            }),
            getConfigurationData: sinon.stub().returns({
                then: sinon.stub().returns({
                    catch: sinon.spy()
                })
            })
        };

        const validator = {
            addValidationFor: sinon.spy(),
            isValid: sinon.spy()
        };

        const urlHelper = {
            getQueryStringParam: sinon.spy()
        };

        const onInit = sinon.spy();

        let wrapper = shallowMount(CaptureDealVehicleCores, {
            provide: {
                alertsManager,
                apiClient,
                validator,
                onInit,
                urlHelper
            }
        });

        // Act & Assert
        assert.isNotNull(wrapper.vm);
    });

    describe("created", () => {
        it("should get deal id from query string", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                }),
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            const urlHelper = {
                getQueryStringParam: sinon.spy()
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act & Assert
            assert.isTrue(urlHelper.getQueryStringParam.calledOnce);
        });

        it("should initialize correctly when deal id is given", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                }),
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.dealId, "12345");
        });

        it("should show error when no deal id is given", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                }),
                getConfigurationData: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns(null)
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act & Assert
            assert.isTrue(wrapper.vm.isInitError);
            assert.isNotEmpty(wrapper.vm.initErrorMessage);
        });

        it("should load core data correctly when response is 200", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE",
                                    description: "Description",
                                    defaultQuantity: 10,
                                    defaultPricePerUnit: 10.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act & Assert
            assert.isNotEmpty(wrapper.vm.cores);
            assert.equal(wrapper.vm.cores[0].vehicleCoreId, 1);
            assert.equal(wrapper.vm.cores[0].code, "code");
            assert.equal(wrapper.vm.cores[0].description, "Description");
            assert.isFalse(wrapper.vm.cores[0].isExtracted);
            assert.equal(wrapper.vm.cores[0].quantity, 10);
            assert.equal(wrapper.vm.cores[0].pricePerUnit, 10.00);
            assert.equal(wrapper.vm.cores[0].totalPrice, 0.00);
        });

        it("should use deal's core data if present", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: [
                                {
                                    vehicleCoreId: 1,
                                    quantity: 5,
                                    pricePerUnit: 5.00,
                                    totalPrice: 25.00,
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE",
                                    description: "Description",
                                    defaultQuantity: 10,
                                    defaultPricePerUnit: 10.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act & Assert
            assert.isNotEmpty(wrapper.vm.cores);
            assert.equal(wrapper.vm.cores[0].vehicleCoreId, 1);
            assert.equal(wrapper.vm.cores[0].code, "code");
            assert.equal(wrapper.vm.cores[0].description, "Description");
            assert.isTrue(wrapper.vm.cores[0].isExtracted);
            assert.equal(wrapper.vm.cores[0].quantity, 5);
            assert.equal(wrapper.vm.cores[0].pricePerUnit, 5.00);
            assert.equal(wrapper.vm.cores[0].totalPrice, 25.00);
        });

        it("should not use deal's core data if id mismatch", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: [
                                {
                                    vehicleCoreId: 2,
                                    quantity: 5,
                                    pricePerUnit: 5.00,
                                    totalPrice: 25.00,
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE",
                                    description: "Description",
                                    defaultQuantity: 10,
                                    defaultPricePerUnit: 10.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act & Assert
            assert.isNotEmpty(wrapper.vm.cores);
            assert.equal(wrapper.vm.cores[0].vehicleCoreId, 1);
            assert.equal(wrapper.vm.cores[0].code, "code");
            assert.equal(wrapper.vm.cores[0].description, "Description");
            assert.isFalse(wrapper.vm.cores[0].isExtracted);
            assert.equal(wrapper.vm.cores[0].quantity, 10);
            assert.equal(wrapper.vm.cores[0].pricePerUnit, 10.00);
            assert.equal(wrapper.vm.cores[0].totalPrice, 0.00);
        });

        it("should configure validator for the given cores", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE_1",
                                    description: "Description_1",
                                    defaultQuantity: 10,
                                    defaultPricePerUnit: 10.00
                                },
                                {
                                    id: 2,
                                    code: "CODE_2",
                                    description: "Description_2",
                                    defaultQuantity: 5,
                                    defaultPricePerUnit: 5.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act & Assert
            assert.isTrue(validator.addValidationFor.calledTwice);
        });
    });

    describe("computed: totalValue", () => {
        it("should be calculated correctly", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: [
                                {
                                    vehicleCoreId: 1,
                                    quantity: 10,
                                    pricePerUnit: 10.00,
                                    totalPrice: 100.00,
                                },
                                {
                                    vehicleCoreId: 2,
                                    quantity: 5,
                                    pricePerUnit: 5.00,
                                    totalPrice: 25.00,
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE_1",
                                    description: "Description_1",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                },
                                {
                                    id: 2,
                                    code: "CODE_2",
                                    description: "Description_2",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.totalValue, 125.00);
        });

        it("should be zero when no cores are extracted", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE_1",
                                    description: "Description_1",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                },
                                {
                                    id: 2,
                                    code: "CODE_2",
                                    description: "Description_2",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.spy()
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.totalValue, 0.00);
        });
    });

    describe("method: submit", () => {
        it("should call the api", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE_1",
                                    description: "Description_1",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(apiClient.submitDealVehicleCores.called);
        });

        it("should not call the api when data is not valid", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE_1",
                                    description: "Description_1",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: sinon.spy()
                    })
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(false)
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(apiClient.submitDealVehicleCores.notCalled);
        });

        it("should show a success alert when response is 200", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE_1",
                                    description: "Description_1",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(alertsManager.showSuccess.called);
        });

        it("should show a danger alert when response is 400", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE_1",
                                    description: "Description_1",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 400
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(alertsManager.showDanger.called);
        });

        it("should show a danger alert when response is 401", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE_1",
                                    description: "Description_1",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 401
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(alertsManager.showDanger.called);
        });

        it("should show a danger alert when network error", () => {
            // Arrange
            const alertsManager = {
                showSuccess: sinon.spy(),
                showDanger: sinon.spy()
            };

            const apiClient = {
                getDealVehicleCores: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            dealVehicleCores: []
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                getConfigurationData: sinon.stub().returns({
                    then: (callback) => {
                        callback({
                            status: 200,
                            cores: [
                                {
                                    id: 1,
                                    code: "CODE_1",
                                    description: "Description_1",
                                    defaultQuantity: 1,
                                    defaultPricePerUnit: 1.00
                                }
                            ]
                        });

                        return {
                            catch: sinon.spy()
                        };
                    }
                }),
                submitDealVehicleCores: sinon.stub().returns({
                    then: sinon.stub().returns({
                        catch: (callback) => {
                            callback("mock error");
                            return;
                        }
                    })
                })
            };

            const validator = {
                addValidationFor: sinon.spy(),
                isValid: sinon.stub().returns(true)
            };

            const urlHelper = {
                getQueryStringParam: sinon.stub().returns("12345")
            };

            const onInit = sinon.spy();

            let wrapper = shallowMount(CaptureDealVehicleCores, {
                provide: {
                    alertsManager,
                    apiClient,
                    validator,
                    onInit,
                    urlHelper
                }
            });

            // Act
            wrapper.vm.submit();

            // Assert
            assert.isTrue(alertsManager.showDanger.called);
        });
    });
});
