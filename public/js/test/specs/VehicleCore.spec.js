// Debugging does not work if mocha is required.
// var mocha = require("mocha");
// var describe = mocha.describe;
// var it = mocha.it;

var chai = require("chai");
var assert = chai.assert;

import Vue from 'vue';
import { mount } from "@vue/test-utils";
import VehicleCore from "../../src/viewmodels/VehicleCore.vue";

describe("VehicleCore.vue", () => {
    it("should be able to mount", () => {
        // Arrange
        let wrapper = mount(VehicleCore, {
            propsData: {
                core: {
                    isExtracted: true,
                    quantity: 15,
                    pricePerUnit: 10.00,
                    code: "asdasd",
                    description: "adasdasd"
                }
            }
        });

        // Act & Assert
        assert.isNotNull(wrapper.vm);
    });

    describe("data: isExtracted", () => {
        it("should be yes when incoming prop is true", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: true,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.isExtracted, "yes");
        });

        it("should be no when incoming prop is false", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.isExtracted, "no");
        });

        it("should be no when incoming prop is null", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: null,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act & Assert
            assert.equal(wrapper.vm.isExtracted, "no");
        });
    });

    describe("computed: totalPrice", () => {
        it("should be calculated initially", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });
    
            // Act & Assert
            assert.equal(wrapper.vm.totalPrice, 150);
        });
    
        it("should be calculated reactively", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });
    
            // Act
            wrapper.setProps({
                core: {
                    isExtracted: false,
                    quantity: 10,
                    pricePerUnit: 10.00,
                    code: "asdasd",
                    description: "adasdasd"
                }
            });
    
            // Assert
            assert.equal(wrapper.vm.totalPrice, 100);
        });

        it("should be zero when quantity missing", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: null,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });
    
            // Act & Assert
            assert.equal(wrapper.vm.totalPrice, 0.00);
        });

        it("should be zero when pricePerUnit missing", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 10,
                        pricePerUnit: null,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });
    
            // Act & Assert
            assert.equal(wrapper.vm.totalPrice, 0.00);
        });

        it("should be zero when quantity and pricePerUnit missing", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 10,
                        pricePerUnit: null,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });
    
            // Act & Assert
            assert.equal(wrapper.vm.totalPrice, 0.00);
        });
    });

    describe("computed: totalPriceDisplay", () => {
        it("should be calculated initially", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });
    
            // Act & Assert
            assert.equal(wrapper.vm.totalPriceDisplay, "150.00");
        });

        it("should be calculated reactively", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });
    
            // Act
            wrapper.setProps({
                core: {
                    isExtracted: false,
                    quantity: 10,
                    pricePerUnit: 10.00,
                    code: "asdasd",
                    description: "adasdasd"
                }
            });
    
            // Assert
            assert.equal(wrapper.vm.totalPriceDisplay, "100.00");
        });

        it("should be zero when no totalPrice", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });
    
            // Act
            wrapper.setProps({
                core: {
                    isExtracted: false,
                    quantity: null,
                    pricePerUnit: null,
                    code: "asdasd",
                    description: "adasdasd"
                }
            });
    
            // Assert
            assert.equal(wrapper.vm.totalPriceDisplay, "0.00");
        });
    });

    describe("method: isExtractedYesOnClick", () => {
        it("should emit change-is-extracted with a true argument", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.vm.isExtractedYesOnClick();

            // Assert
            // the event was emitted
            assert.exists(wrapper.emitted()["change-is-extracted"]); 
            // times that the event was emitted
            assert.equal(wrapper.emitted()["change-is-extracted"].length, 1); 

            // playload with which the event was emitted
            let firstCall = 0;
            let firstPayloadArg = 0;
            assert.equal(wrapper.emitted()["change-is-extracted"][firstCall][firstPayloadArg], true); 

        });
    });

    describe("method: isExtractedNoOnClick", () => {
        it("should emit change-is-extracted with a false argument", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.vm.isExtractedNoOnClick();

            // Assert
            // the event was emitted
            assert.exists(wrapper.emitted()["change-is-extracted"]); 
            // times that the event was emitted
            assert.equal(wrapper.emitted()["change-is-extracted"].length, 1); 

            // playload with which the event was emitted
            let firstCall = 0;
            let firstPayloadArg = 0;
            assert.equal(wrapper.emitted()["change-is-extracted"][firstCall][firstPayloadArg], false); 

        });
    });

    describe("method: quantityOnInput", () => {
        it("should emit input-quantity with given argument", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.vm.quantityOnInput({
                target: {
                    value: 10
                }
            });

            // Assert
            // the event was emitted
            assert.exists(wrapper.emitted()["input-quantity"]); 
            // times that the event was emitted
            assert.equal(wrapper.emitted()["input-quantity"].length, 1); 

            // playload with which the event was emitted
            let firstCall = 0;
            let firstPayloadArg = 0;
            assert.equal(wrapper.emitted()["input-quantity"][firstCall][firstPayloadArg], 10);
        });
    });

    describe("method: pricePerUnitOnInput", () => {
        it("should emit input-price-per-unit with given argument", () => {
            // Arrange
            let wrapper = mount(VehicleCore, {
                propsData: {
                    core: {
                        isExtracted: false,
                        quantity: 15,
                        pricePerUnit: 10.00,
                        code: "asdasd",
                        description: "adasdasd"
                    }
                }
            });

            // Act
            wrapper.vm.pricePerUnitOnInput({
                target: {
                    value: 10.00
                }
            });

            // Assert
            // the event was emitted
            assert.exists(wrapper.emitted()["input-price-per-unit"]); 
            // times that the event was emitted
            assert.equal(wrapper.emitted()["input-price-per-unit"].length, 1); 

            // playload with which the event was emitted
            let firstCall = 0;
            let firstPayloadArg = 0;
            assert.equal(wrapper.emitted()["input-price-per-unit"][firstCall][firstPayloadArg], 10.00);
        });
    });
});
