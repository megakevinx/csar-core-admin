<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [

            // Auth
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'admin' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/admin',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'admin',
                    ],
                ],
            ],
            // 'application' => [
            //     'type'    => Segment::class,
            //     'options' => [
            //         'route'    => '/application[/:action]',
            //         'defaults' => [
            //             'controller' => Controller\IndexController::class,
            //             'action'     => 'index',
            //         ],
            //     ],
            // ],

            // Entry
            'capture-deal-vehicle-cores' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/capture-deal-vehicle-cores',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'captureDealVehicleCores',
                    ],
                ],
            ],
            'manage-vehicle-cores' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/manage-vehicle-cores',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'manageVehicleCores',
                    ],
                ],
            ],

            // Configuration
            'configuration-enter-cores' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/configuration/enter-cores',
                    'defaults' => [
                        'controller' => Controller\ConfigurationController::class,
                        'action' => 'enterCores'
                    ],
                ],
            ],

            // API
            'deal-vehicle-core' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/deal-vehicle-core[/:id]',
                    'defaults' => [
                        'controller' => Controller\DealVehicleCoreController::class,
                    ],
                ],
            ],
            'vehicle-core' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/api/vehicle-core/:id',
                    'defaults' => [
                        'controller' => Controller\VehicleCoreController::class,
                    ],
                ],
            ],

        ],
    ],
    'controllers' => [
        'factories' => [
            //Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
