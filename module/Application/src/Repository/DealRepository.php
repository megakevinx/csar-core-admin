<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;

use Application\Model\Deal;

class DealRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getById(string $dealId)
    {
        $resultSet = $this->tableGateway->select(['Inspection_ID' => $dealId]);

        $row = $resultSet->current();

        return $row;
    }
}