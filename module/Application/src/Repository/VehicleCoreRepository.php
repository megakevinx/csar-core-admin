<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\VehicleCore;

class VehicleCoreRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getAll()
    {
        $resultSet = $this->tableGateway->select();
        $rows = [];

        foreach ($resultSet as $row) {
            $rows[] = $row;
        }

        return $rows;
    }

    public function getById(string $vehicleCoreId)
    {
        $resultSet = $this->tableGateway->select(['VehicleCore_ID' => $vehicleCoreId]);

        $row = $resultSet->current();

        return $row;
    }

    public function update(VehicleCore $vehicleCore)
    {
        $dataToUpdate = $vehicleCore->toDbArray();

        $this->tableGateway->update($dataToUpdate, [ "VehicleCore_ID" => $vehicleCore->id ]);

        return $vehicleCore;
    }
}