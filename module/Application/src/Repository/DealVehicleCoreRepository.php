<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\DealVehicleCore;

class DealVehicleCoreRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getByDealId(string $dealId)
    {
        $resultSet = $this->tableGateway->select([ "Inspection_ID" => $dealId ]);
        $rows = [];

        foreach ($resultSet as $row) {
            $rows[] = $row;
        }

        return $rows;
    }

    public function getByDealAndVehicleCore(string $dealId, string $vehicleCoreId)
    {
        $resultSet = $this->tableGateway->select([ "Inspection_ID" => $dealId, "VehicleCore_ID" => $vehicleCoreId ]);
        return $resultSet->current();
    }

    public function save(DealVehicleCore $dealVehicleCore)
    {
        $dataToSave = $dealVehicleCore->toDbArray();

        $this->tableGateway->insert($dataToSave);
        $dealVehicleCore->id = $this->tableGateway->lastInsertValue;

        return $dealVehicleCore;
    }

    public function update(DealVehicleCore $dealVehicleCore)
    {
        $dataToUpdate = $dealVehicleCore->toDbArray();

        $this->tableGateway->update($dataToUpdate, [ "Inspection_VehicleCore_ID" => $dealVehicleCore->id ]);

        return $dealVehicleCore;
    }

    public function delete(DealVehicleCore $dealVehicleCore)
    {
        $this->tableGateway->delete([ "Inspection_VehicleCore_ID" => $dealVehicleCore->id ]);

        return $dealVehicleCore;
    }
}