<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\User;

class UserRepository
{
    const BUYER_USER_ROLE = 'Buyer';
    const ADMIN_USER_ROLE = 'Administrator';

    private $dbAdapter;

    public function __construct(Adapter $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    public function getByName(string $name)
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select()
            ->from('Buyer')->columns(['Buyer_ID', 'BuyerEmail', 'PassWd'])
            ->where(['Buyer.BuyerName' => $name, 'Buyer.Active' => 1]);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $userData = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($userData)
        {
            $user = new User();
            $user->id = $userData[0]['Buyer_ID'];
            $user->password = $userData[0]['PassWd'];
            $user->email = $userData[0]['BuyerEmail'];

            return $user;
        }
        else {
            return null;
        }
    }
}
