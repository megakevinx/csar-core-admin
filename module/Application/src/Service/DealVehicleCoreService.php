<?php

namespace Application\Service;

use Application\Repository\DealVehicleCoreRepository;
use Application\Repository\DealRepository;

use Application\Model\VehicleCore;
use Application\Model\DealVehicleCore;
use Application\Model\Deal;

class DealVehicleCoreService
{
    private $dealVehicleCorerepository;
    private $dealRepository;

    public function __construct(DealVehicleCoreRepository $dealVehicleCorerepository, DealRepository $dealRepository)
    {
        $this->dealVehicleCoreRepository = $dealVehicleCorerepository;
        $this->dealRepository = $dealRepository;
    }

    public function getVehicleCoresByDealId(string $dealId)
    {
        $deal = $this->dealRepository->getById($dealId);

        if (!$deal) {
            return (object)[
                "isSuccess" => false
            ];
        }

        $dealVehicleCores = $this->dealVehicleCoreRepository->getByDealId($dealId);

        return (object)[
            "isSuccess" => true,
            "dealVehicleCores" => $dealVehicleCores
        ];
    }

    public function updateDealVehicleCores(array $data)
    {
        $dealVehicleCoresToSave = $data["cores"];

        $savedDealVehicleCores = [];
        $deletedDealVehicleCores = [];

        foreach ($dealVehicleCoresToSave as $core) {

            $existingDealVehicleCore = $this->dealVehicleCoreRepository->getByDealAndVehicleCore($data["dealId"], $core["vehicleCoreId"]);

            if ($core["isExtracted"] == true) {
                if ($existingDealVehicleCore) {
                    $existingDealVehicleCore->quantity = $core["quantity"];
                    $existingDealVehicleCore->pricePerUnit = $core["pricePerUnit"];
                    $existingDealVehicleCore->totalPrice = $core["totalPrice"];

                    $savedDealVehicleCores[] = $this->dealVehicleCoreRepository->update($existingDealVehicleCore);
                }
                else {
                    $newDealVehicleCore = new DealVehicleCore();

                    // TODO: remove this casting
                    $newDealVehicleCore->dealId = (int)$data["dealId"];
                    $newDealVehicleCore->vehicleCoreId = $core["vehicleCoreId"];
                    $newDealVehicleCore->quantity = $core["quantity"];
                    $newDealVehicleCore->pricePerUnit = $core["pricePerUnit"];
                    $newDealVehicleCore->totalPrice = $core["totalPrice"];

                    $savedDealVehicleCores[] = $this->dealVehicleCoreRepository->save($newDealVehicleCore);
                }
            }
            else {
                if ($existingDealVehicleCore) {
                    $deletedDealVehicleCores[] = $this->dealVehicleCoreRepository->delete($existingDealVehicleCore);
                }
            }
        }

        return (object)[
            "isSuccess" => true,
            "saved" => $savedDealVehicleCores,
            "deleted" => $deletedDealVehicleCores
        ];
    }
}