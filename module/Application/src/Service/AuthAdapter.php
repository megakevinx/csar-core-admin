<?php

namespace Application\Service;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
//use Zend\Crypt\Password\Bcrypt;

use Application\Repository\UserRepository;

/**
 * Adapter used for authenticating user. It takes login and password on input
 * and checks the database if there is a user with such login (email) and password.
 * If such user exists, the service returns its identity (email). The identity
 * is saved to session and can be retrieved later with Identity view helper provided
 * by ZF3.
 */
class AuthAdapter implements AdapterInterface
{
    private $userRepository;

    /**
     * User email.
     * @var string
     */
    private $email;

    /**
     * Password
     * @var string
     */
    private $password;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Sets user email.
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Sets password.
     */
    public function setPassword($password)
    {
        $this->password = (string)$password;
    }

    /**
     * Performs an authentication attempt.
     */
    public function authenticate()
    {
        $user = $this->userRepository->getByName($this->email);

        if (! $user) {
            return new Result(
                Result::FAILURE_IDENTITY_NOT_FOUND,
                null,
                ['Invalid credentials.']
            );
        }

        if ($this->password === $user->password) {
            return new Result(
                Result::SUCCESS,
                $user,
                ['Authenticated successfully.']
            );

            // if ($user->role) {
            //     return new Result(
            //         Result::SUCCESS,
            //         $user,
            //         ['Authenticated successfully.']
            //     );
            // } else {
            //     return new Result(
            //         Result::FAILURE,
            //         null,
            //         ['No role configured.']
            //     );
            // }
        } else {
            return new Result(
                Result::FAILURE_CREDENTIAL_INVALID,
                null,
                ['Invalid credentials.']
            );
        }
    }
}
