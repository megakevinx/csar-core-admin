<?php

namespace Application\Service;

use Zend\Authentication\AuthenticationService;
use Zend\Session\SessionManager;
use Zend\Authentication\Result;
use Zend\Math\Rand;

use Application\Service\AuthAdapter;

use Application\Model\User;

/**
 * The AuthService service is responsible for user's login/logout and simple access
 * filtering. The access filtering feature checks whether the current visitor
 * is allowed to see the given page or not.
 */
class AuthService
{
    /**
     * Authentication service.
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authProvider;

    /**
     * Session manager.
     * @var Zend\Session\SessionManager
     */
    private $sessionManager;

    public function __construct(AuthenticationService $authProvider, SessionManager $sessionManager)
    {
        $this->authProvider = $authProvider;
        $this->sessionManager = $sessionManager;
    }

    /**
     * Performs a login attempt. If $rememberMe argument is true, it forces the session
     * to last for one month (otherwise the session expires on one hour).
     */
    public function login($email, $password)
    {
        $isSuccess = false;

        // Check if user has already logged in. If so, do not allow to log in
        // twice.
        if ($this->authProvider->getIdentity() != null) {
            //throw new \Exception('Already logged in');
            // Remove identity from session.
            $this->authProvider->clearIdentity();
        }

        // Authenticate with login/password.
        $authAdapter = $this->authProvider->getAdapter();
        $authAdapter->setEmail($email);
        $authAdapter->setPassword($password);

        $result = $this->authProvider->authenticate();

        if ($result->getCode() == Result::SUCCESS) {
            $isSuccess = true;
            $this->sessionManager->rememberMe(60 * 60 * 24 * 30);
        }

        return (object)[
            'isSuccess' => $isSuccess,
            'identity' => $this->authProvider->getIdentity()
        ];
    }

    // /**
    //  * Performs user logout.
    //  */
    // public function logout()
    // {
    //     // Allow to log out only when user is logged in.
    //     if ($this->authProvider->getIdentity() == null) {
    //         throw new \Exception('The user is not logged in');
    //     }

    //     // Remove identity from session.
    //     $this->authProvider->clearIdentity();
    // }
}
