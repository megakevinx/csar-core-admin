<?php

namespace Application\Service;

use Application\Repository\VehicleCoreRepository;

use Application\Model\VehicleCore;

class VehicleCoreService
{
    private $repository;

    public function __construct(VehicleCoreRepository $repository)
    {
        $this->repository = $repository;
    }

    public function updateVehicleCore(array $data)
    {
        $vehicleCoreToUpdate = $this->repository->getById($data["id"]);

        if (!$vehicleCoreToUpdate) {
            return (object)[
                "isSuccess" => false
            ];
        }

        $vehicleCoreToUpdate->defaultQuantity = $data["defaultQuantity"];
        $vehicleCoreToUpdate->defaultPricePerUnit = $data["defaultPricePerUnit"];

        $updatedVehicleCore = $this->repository->update($vehicleCoreToUpdate);

        return (object)[
            "isSuccess" => true,
            "updated" => $updatedVehicleCore
        ];
    }
}