<?php

namespace Application\Model;

class VehicleCore
{
    public $id;
    public $code;
    public $description;
    public $defaultQuantity;
    public $defaultPricePerUnit;

    public function exchangeArray(array $data)
    {
        $this->id = $data['VehicleCore_ID'] ?? null;
        $this->code = $data['Code'] ?? null;
        $this->description = $data['Description'] ?? null;
        $this->defaultQuantity = $data['DefaultQuantity'] ?? null;
        $this->defaultPricePerUnit = $data['DefaultPricePerUnit'] ?? null;
    }

    public function toDbArray()
    {
        return [
            'Code' => $this->code,
            'Description' => $this->description,
            'DefaultQuantity' => $this->defaultQuantity,
            'DefaultPricePerUnit' => $this->defaultPricePerUnit,
        ];
    }
}