<?php

namespace Application\Model;

class User
{
    public $id;

    public $email;
    public $password;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['User_ID']) ? $data['User_ID'] : null;

        $this->email = !empty($data['Email']) ? $data['Email'] : null;
        $this->password = !empty($data['Password']) ? $data['Password'] : null;
    }
}