<?php

namespace Application\Model;

class DealVehicleCore
{
    public $id;
    public $dealId;
    public $vehicleCoreId;
    public $quantity;
    public $pricePerUnit;
    public $totalPrice;

    public function exchangeArray(array $data)
    {
        $this->id = $data['Inspection_VehicleCore_ID'] ?? null;
        $this->dealId = $data['Inspection_ID'] ?? null;
        $this->vehicleCoreId = $data['VehicleCore_ID'] ?? null;
        $this->quantity = $data['Quantity'] ?? null;
        $this->pricePerUnit = $data['PricePerUnit'] ?? null;
        $this->totalPrice = $data['TotalPrice'] ?? null;
    }

    public function toDbArray()
    {
        return [
            'Inspection_ID' => $this->dealId,
            'VehicleCore_ID' => $this->vehicleCoreId,
            'Quantity' => $this->quantity,
            'PricePerUnit' => $this->pricePerUnit,
            'TotalPrice' => $this->totalPrice,
        ];
    }
}