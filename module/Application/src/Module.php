<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Zend\Session\SessionManager;
use Zend\Session\AbstractManager;
use Zend\Authentication\Storage\Session as SessionStorage;

use Zend\Authentication\AuthenticationService;

class Module implements ConfigProviderInterface
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        $sessionManager = $e->getApplication()->getServiceManager()->get('Zend\Session\SessionManager');
        $this->forgetInvalidSession($sessionManager);
    }

    protected function forgetInvalidSession(AbstractManager $sessionManager)
    {
        try {
            $sessionManager->start();
            return;
        } catch (\Exception $e) { }
        /**
         * Session validation failed: toast it and carry on.
         */
        // @codeCoverageIgnoreStart
        session_unset();
        // @codeCoverageIgnoreEnd
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                Controller\IndexController::class => function($container) {
                    return new Controller\IndexController(
                        $container->get(Service\AuthService::class)
                    );
                },

                Controller\ConfigurationController::class => function($container) {
                    return new Controller\ConfigurationController(
                        $container->get(Repository\VehicleCoreRepository::class)
                    );
                },

                Controller\DealVehicleCoreController::class => function($container) {
                    return new Controller\DealVehicleCoreController(
                        $container->get(Service\DealVehicleCoreService::class)
                    );
                },

                Controller\VehicleCoreController::class => function($container) {
                    return new Controller\VehicleCoreController(
                        $container->get(Service\VehicleCoreService::class)
                    );
                },
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                // Auth
                Service\AuthService::class => function ($container) {
                    return new Service\AuthService(
                        $container->get(\Zend\Authentication\AuthenticationService::class),
                        $container->get(\Zend\Session\SessionManager::class)
                    );
                },
                \Zend\Authentication\AuthenticationService::class => function ($container) {
                    return new AuthenticationService(
                        new SessionStorage('Zend_Auth', 'session', $container->get(\Zend\Session\SessionManager::class)),
                        $container->get(Service\AuthAdapter::class)
                    );
                },
                Service\AuthAdapter::class => function ($container) {
                    return new Service\AuthAdapter(
                        $container->get(Repository\UserRepository::class)
                    );
                },

                Service\DealVehicleCoreService::class => function($container) {
                    return new Service\DealVehicleCoreService(
                        $container->get(Repository\DealVehicleCoreRepository::class),
                        $container->get(Repository\DealRepository::class)
                    );
                },

                Service\VehicleCoreService::class => function($container) {
                    return new Service\VehicleCoreService(
                        $container->get(Repository\VehicleCoreRepository::class)
                    );
                },

                // Repositories
                Repository\UserRepository::class => function ($container) {
                    return new Repository\UserRepository(
                        $container->get(AdapterInterface::class)
                    );
                },

                Repository\DealVehicleCoreRepository::class => function($container) {
                    return new Repository\DealVehicleCoreRepository(
                        $container->get(Model\DealVehicleCoreTableGateway::class)
                    );
                },

                Repository\VehicleCoreRepository::class => function($container) {
                    return new Repository\VehicleCoreRepository(
                        $container->get(Model\VehicleCoreTableGateway::class)
                    );
                },

                Repository\DealRepository::class => function($container) {
                    return new Repository\DealRepository(
                        $container->get(Model\DealTableGateway::class)
                    );
                },

                // Table Gateways
                Model\DealVehicleCoreTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\DealVehicleCore());

                    return new TableGateway('Inspection_VehicleCore', $dbAdapter, null, $resultSetPrototype);
                },

                Model\VehicleCoreTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\VehicleCore());

                    return new TableGateway('VehicleCore', $dbAdapter, null, $resultSetPrototype);
                },

                Model\DealTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Deal());

                    return new TableGateway('Inspection', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }
}
