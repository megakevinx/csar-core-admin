<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

use Application\Controller\BaseRestfulController;

use Application\Controller\Form\UpdateVehicleCoreForm;

use Application\Service\VehicleCoreService;

class VehicleCoreController extends BaseRestfulController
{
    private $service;

    public function __construct(VehicleCoreService $service)
    {
        $this->service = $service;
    }

    // PUT api/vehicle-core/{vehicleCoreId}
    public function update($id, $data)
    {
        if (! $this->identity()) {
            return $this->notAuthorized();
        }

        $form = new UpdateVehicleCoreForm();
        $form->setData($data);

        if (!$form->isValid()) {
            return $this->badRequest([
                'errorCode' => "VALIDATION_ERROR",
                'errorMessage' => "The incoming data is invalid."
            ]);
        }

        $data = $form->getData();

        $result = $this->service->updateVehicleCore($data);

        if (!$result->isSuccess) {
            return $this->badRequest([
                'errorCode' => "NOT_FOUND",
                'errorMessage' => "The specified Vehicle Core does not exist."
            ]);
        }

        return $this->ok([
            'updated' => $result->updated
        ]);
    }
}
