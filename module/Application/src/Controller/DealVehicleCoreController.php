<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

use Application\Controller\BaseRestfulController;

use Application\Controller\Form\UpdateDealVehicleCoresForm;

use Application\Service\DealVehicleCoreService;

class DealVehicleCoreController extends BaseRestfulController
{
    private $service;

    public function __construct(DealVehicleCoreService $service)
    {
        $this->service = $service;
    }

    // GET api/deal-vehicle-cores/{dealId}
    public function get($dealId)
    {
        if (! $this->identity()) {
            return $this->notAuthorized();
        }

        $result = $this->service->getVehicleCoresByDealId($dealId);

        if (!$result->isSuccess) {
            return $this->badRequest([
                'errorCode' => "NOT_FOUND",
                'errorMessage' => "There is no Deal with the given number.",
                'result' => $result
            ]);
        }

        return $this->ok([
            'dealVehicleCores' => $result->dealVehicleCores,
        ]);
    }

    // POST api/deal-vehicle-cores
    public function create($data)
    {
        if (! $this->identity()) {
            return $this->notAuthorized();
        }

        $form = new UpdateDealVehicleCoresForm();
        $form->setData($data);

        if (!$form->isValid()) {
            return $this->badRequest([
                'errorCode' => "VALIDATION_ERROR",
                'errorMessage' => "The incoming data is invalid."
            ]);
        }

        $data = $form->getData();

        $result = $this->service->updateDealVehicleCores($data);

        return $this->ok([
            'saved' => $result->saved,
            'deleted' => $result->deleted
        ]);
    }
}
