<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class BaseRestfulController extends AbstractRestfulController
{
    protected function userIsInRole($role)
    {
        $user = $this->identity();
        return $user && $user->role == $role;
    }

    protected function notAuthorized()
    {
        $this->getResponse()->setStatusCode(401);
        return new JsonModel([
            'status' => 401
        ]);
    }

    protected function badRequest($result)
    {
        $this->getResponse()->setStatusCode(400);
        $result['status'] = 400;
        return new JsonModel($result);
    }

    protected function methodNotAllowed()
    {
        $this->getResponse()->setStatusCode(405);
        return new JsonModel([
            'status' => 405
        ]);
    }

    protected function ok($result)
    {
        $this->getResponse()->setStatusCode(200);
        $result['status'] = 200;
        return new JsonModel($result);
    }
}
