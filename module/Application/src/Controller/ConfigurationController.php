<?php

namespace Application\Controller;

use Zend\View\Model\JsonModel;

use Application\Controller\BaseRestfulController;

use Application\Repository\VehicleCoreRepository;

class ConfigurationController extends BaseRestfulController
{
    private $vehicleCoreRepository;

    public function __construct(VehicleCoreRepository $vehicleCoreRepository)
    {
        $this->vehicleCoreRepository = $vehicleCoreRepository;
    }

    // GET api/configuration/enter-cores
    public function enterCoresAction()
    {
        if ($this->getRequest()->isGet()) {
            return $this->ok([
                "cores" => $this->vehicleCoreRepository->getAll()
            ]);
        } else {
            return $this->methodNotAllowed();
        }
    }
}
