<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Service\AuthService;

use Application\Controller\Form\LoginForm;
use Application\Controller\Form\AdminLoginForm;

class IndexController extends AbstractActionController
{
    private $service;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function indexAction()
    {
        if ($this->getRequest()->isPost())
        {
            $data = $this->params()->fromPost();

            $form = new LoginForm();
            $form->setData($data);

            if ($form->isValid())
            {
                $data = $form->getData();

                if ($this->identity())
                {
                    return $this->redirectToCaptureAction($data['dealId']);
                }

                $result = $this->authService->login($data['username'], $data['password']);

                if ($result->isSuccess)
                {
                    return $this->redirectToCaptureAction($data['dealId']);
                }
            }
        }

        return new ViewModel();
    }

    public function adminAction()
    {
        if ($this->getRequest()->isPost())
        {
            $data = $this->params()->fromPost();

            $form = new AdminLoginForm();
            $form->setData($data);

            if ($form->isValid())
            {
                $data = $form->getData();

                if ($this->identity())
                {
                    return $this->redirect()->toRoute('manage-vehicle-cores');
                }

                $result = $this->authService->login($data['username'], $data['password']);

                if ($result->isSuccess)
                {
                    return $this->redirect()->toRoute('manage-vehicle-cores');
                }
            }
        }

        return new ViewModel();
    }

    private function redirectToCaptureAction($dealId)
    {
        return $this->redirect()->toRoute(
            'capture-deal-vehicle-cores', [], [
                "query" => [ "dealId" => $dealId ]
            ]
        );
    }

    public function captureDealVehicleCoresAction()
    {
        if (! $this->identity()) {
            return $this->redirect()->toRoute('home');
        }

        return new ViewModel();
    }

    public function manageVehicleCoresAction()
    {
        if (! $this->identity()) {
            return $this->redirect()->toRoute('admin');
        }

        return new ViewModel();
    }
}
