<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

/**
 * This form is used to collect user's login, password and 'Remember Me' flag.
 */
class UpdateVehicleCoreForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('update-vehicle-core-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        $this->add(['name' => 'id']);
        $this->add(['name' => 'defaultQuantity']);
        $this->add(['name' => 'defaultPricePerUnit']);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                [ 'name' => 'StringTrim' ],
            ],
            'validators' => [
                [ 'name' => 'Digits' ]
            ]
        ]);

        $inputFilter->add([
            'name' => 'defaultQuantity',
            'required' => true,
            'filters' => [
                [ 'name' => 'StringTrim' ],
            ],
            'validators' => [
                [ 'name' => 'Digits' ],
                [
                    'name' => 'GreaterThan',
                    'options' => [ 'min' => 0, 'inclusive' => true ],
                ]
            ]
        ]);

        $inputFilter->add([
            'name' => 'defaultPricePerUnit',
            'required' => true,
            'filters' => [
                [ 'name' => 'StringTrim' ],
            ],
            'validators' => [
                [
                    'name' => 'IsFloat',
                    'options' => ['locale' => 'en'],
                ],
                [
                    'name' => 'GreaterThan',
                    'options' => [ 'min' => 0, 'inclusive' => true ],
                ],
            ]
        ]);
    }
}
