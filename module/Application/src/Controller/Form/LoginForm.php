<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

/**
 * This form is used to collect user's login, password and 'Remember Me' flag.
 */
class LoginForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('login-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        $this->add(['name' => 'username']);
        $this->add(['name' => 'password']);
        $this->add(['name' => 'dealId']);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'username',
            'required' => true,
            'filters'  => [
                [ 'name' => 'StringTrim' ],
            ]
        ]);

        $inputFilter->add([
            'name'     => 'password',
            'required' => true,
            'filters'  => [
                [ 'name' => 'StringTrim' ],
            ]
        ]);

        $inputFilter->add([
            'name' => 'dealId',
            'required' => true,
            'filters' => [
                [ 'name' => 'StringTrim' ],
            ],
            'validators' => [
                [ 'name' => 'Digits' ]
            ]
        ]);
    }
}

