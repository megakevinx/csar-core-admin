<?php

namespace Application\Controller\Form;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

/**
 * This form is used to collect user's login, password and 'Remember Me' flag.
 */
class UpdateDealVehicleCoresForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('update-deal-vehicle-cores-form');
     
        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        $this->add(['name' => 'dealId']);
        $this->add(['name' => 'cores']);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'dealId',
            'required' => true,
            'filters' => [
                [ 'name' => 'StringTrim' ],
            ],
            'validators' => [
                [ 'name' => 'Digits' ]
            ]
        ]);

        $inputFilter->add([
            'name' => 'cores',
            'required' => true,
            'validators' => [
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function ($value, $formValues) {

                            $notEmptyArray = new \Zend\Validator\NotEmpty(\Zend\Validator\NotEmpty::EMPTY_ARRAY);
                            $isDigitsOnly = new \Zend\Validator\Digits();
                            $isFloat = new \Zend\I18n\Validator\IsFloat(['locale' => 'en']);
                            $isPositive = new \Zend\Validator\GreaterThan(['min' => 0, 'inclusive' => true]);
                            $isGreaterThanZero = new \Zend\Validator\GreaterThan(['min' => 0, 'inclusive' => false]);

                            // Cannot be empty
                            if (!$notEmptyArray->isValid($value)) {
                                return false;
                            }

                            // Every element needs to have all fileds set
                            foreach ($value as $core) {
                                if (!isset($core["isExtracted"]) || !is_bool($core["isExtracted"])) {
                                    return false;
                                }

                                if ($core["isExtracted"] === true) {
                                    if (!isset($core["vehicleCoreId"]) ||
                                        !$isDigitsOnly->isValid($core["vehicleCoreId"]) ||
                                        !$isGreaterThanZero->isValid($core["vehicleCoreId"])) {
                                        return false;
                                    }

                                    if (!isset($core["quantity"]) ||
                                        !$isDigitsOnly->isValid($core["quantity"]) ||
                                        !$isPositive->isValid($core["quantity"])) {
                                        return false;
                                    }

                                    if (!isset($core["pricePerUnit"]) ||
                                        !$isFloat->isValid($core["pricePerUnit"]) ||
                                        !$isPositive->isValid($core["pricePerUnit"])) {
                                        return false;
                                    }

                                    if (!isset($core["totalPrice"]) ||
                                        !$isFloat->isValid($core["totalPrice"]) ||
                                        !$isPositive->isValid($core["totalPrice"])) {
                                        return false;
                                    }
                                }
                            }

                            return true;
                        }
                    ]
                ]
            ]
        ]);
    }
}
