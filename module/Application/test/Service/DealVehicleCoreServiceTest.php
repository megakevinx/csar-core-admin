<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Application\Service\DealVehicleCoreService;

use Application\Repository\DealVehicleCoreRepository;
use Application\Repository\DealRepository;

use Application\Model\VehicleCore;
use Application\Model\DealVehicleCore;
use Application\Model\Deal;

class DealVehicleCoreServiceTest extends TestCase
{
    public function testGetFailsWhenDealNotFound()
    {
        // Arrange
        $dealId = "12345";

        $mockDealVehicleCoreRepository = $this->createMock(DealVehicleCoreRepository::class);

        $mockDealRepository = $this->createMock(DealRepository::class);
        $mockDealRepository
            ->expects($this->once())
            ->method("getById")
            ->with($this->equalTo($dealId))
            ->willReturn(null);

        $service = new DealVehicleCoreService($mockDealVehicleCoreRepository, $mockDealRepository);

        // Act
        $result = $service->getVehicleCoresByDealId($dealId);

        // Assert
        $this->assertFalse($result->isSuccess);
    }

    public function testGetReturnsCorrectData()
    {
        // Arrange
        $dealId = "12345";

        $mockDealVehicleCoreRepository = $this->createMock(DealVehicleCoreRepository::class);
        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("getByDealId")
            ->with($this->equalTo($dealId))
            // This can be whatever  as no class is dependent on the specific data
            // that DealVehicleCoreRepository's getByDealId returns
            ->willReturn([1, 2, 3]);

        $mockDealRepository = $this->createMock(DealRepository::class);
        $mockDealRepository
            ->expects($this->once())
            ->method("getById")
            ->with($this->equalTo($dealId))
            ->willReturn(new Deal());

        $service = new DealVehicleCoreService($mockDealVehicleCoreRepository, $mockDealRepository);

        // Act
        $result = $service->getVehicleCoresByDealId($dealId);

        // Assert
        $this->assertTrue($result->isSuccess);
        $this->assertNotNull($result->dealVehicleCores);
        $this->assertCount(3, $result->dealVehicleCores);
    }

    public function testUpdateCreatesNewDealVehicleCoreWhenNoPreviouslyExistingOneFound()
    {
        // Arrange
        $param = [
            "dealId" => 12345,
            "cores" => [
                [
                    "isExtracted" => true,
                    "vehicleCoreId" => 1,
                    "quantity" => 2,
                    "pricePerUnit" => 10.00,
                    "totalPrice" => 10.00
                ]
            ]
        ];

        $mockDealVehicleCoreRepository = $this->createMock(DealVehicleCoreRepository::class);
        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("getByDealAndVehicleCore")
            ->with($this->equalTo($param["dealId"]), $this->equalTo($param["cores"][0]["vehicleCoreId"]))
            ->willReturn(null);

        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("save")
            ->will($this->returnArgument(0));

        $mockDealRepository = $this->createMock(DealRepository::class);

        $service = new DealVehicleCoreService($mockDealVehicleCoreRepository, $mockDealRepository);

        // Act
        $result = $service->updateDealVehicleCores($param);

        // Assert
        $this->assertTrue($result->isSuccess);
        $this->assertCount(1, $result->saved);
        $this->assertCount(0, $result->deleted);
    }

    public function testUpdateSavesExistingDealVehicleCoreWhenPreviouslyExistingOneIsFound()
    {
        // Arrange
        $param = [
            "dealId" => 12345,
            "cores" => [
                [
                    "isExtracted" => true,
                    "vehicleCoreId" => 1,
                    "quantity" => 2,
                    "pricePerUnit" => 10.00,
                    "totalPrice" => 10.00
                ]
            ]
        ];

        $existingDealVehicleCore = new DealVehicleCore();

        $mockDealVehicleCoreRepository = $this->createMock(DealVehicleCoreRepository::class);
        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("getByDealAndVehicleCore")
            ->with($this->equalTo($param["dealId"]), $this->equalTo($param["cores"][0]["vehicleCoreId"]))
            ->willReturn($existingDealVehicleCore);

        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("update")
            ->will($this->returnArgument(0));

        $mockDealRepository = $this->createMock(DealRepository::class);

        $service = new DealVehicleCoreService($mockDealVehicleCoreRepository, $mockDealRepository);

        // Act
        $result = $service->updateDealVehicleCores($param);

        // Assert
        $this->assertTrue($result->isSuccess);
        $this->assertCount(1, $result->saved);
        $this->assertCount(0, $result->deleted);
    }

    public function testUpdateDeletesExistingDealVehicleCoreWhenPreviouslyExistingOneIsFound()
    {
        // Arrange
        $param = [
            "dealId" => 12345,
            "cores" => [
                [
                    "isExtracted" => false,
                    "vehicleCoreId" => 1,
                    "quantity" => 2,
                    "pricePerUnit" => 10.00,
                    "totalPrice" => 10.00
                ]
            ]
        ];

        $existingDealVehicleCore = new DealVehicleCore();

        $mockDealVehicleCoreRepository = $this->createMock(DealVehicleCoreRepository::class);
        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("getByDealAndVehicleCore")
            ->with($this->equalTo($param["dealId"]), $this->equalTo($param["cores"][0]["vehicleCoreId"]))
            ->willReturn($existingDealVehicleCore);

        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("delete")
            ->will($this->returnArgument(0));

        $mockDealRepository = $this->createMock(DealRepository::class);

        $service = new DealVehicleCoreService($mockDealVehicleCoreRepository, $mockDealRepository);

        // Act
        $result = $service->updateDealVehicleCores($param);

        // Assert
        $this->assertTrue($result->isSuccess);
        $this->assertCount(0, $result->saved);
        $this->assertCount(1, $result->deleted);
    }

    public function testUpdateDoesNothingWhenIncomingCoreIsNotExtractedAndPreviouslyExistingOneIsNotFound()
    {
        // Arrange
        $param = [
            "dealId" => 12345,
            "cores" => [
                [
                    "isExtracted" => false,
                    "vehicleCoreId" => 1,
                    "quantity" => 2,
                    "pricePerUnit" => 10.00,
                    "totalPrice" => 10.00
                ]
            ]
        ];

        $existingDealVehicleCore = new DealVehicleCore();

        $mockDealVehicleCoreRepository = $this->createMock(DealVehicleCoreRepository::class);
        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("getByDealAndVehicleCore")
            ->with($this->equalTo($param["dealId"]), $this->equalTo($param["cores"][0]["vehicleCoreId"]))
            ->willReturn(null);

        $mockDealVehicleCoreRepository
            ->expects($this->exactly(0))
            ->method("save");

        $mockDealVehicleCoreRepository
            ->expects($this->exactly(0))
            ->method("update");

        $mockDealVehicleCoreRepository
            ->expects($this->exactly(0))
            ->method("delete");


        $mockDealRepository = $this->createMock(DealRepository::class);

        $service = new DealVehicleCoreService($mockDealVehicleCoreRepository, $mockDealRepository);

        // Act
        $result = $service->updateDealVehicleCores($param);

        // Assert
        $this->assertTrue($result->isSuccess);
        $this->assertCount(0, $result->saved);
        $this->assertCount(0, $result->deleted);
    }

    public function testUpdateActsAccordinglyWiththeVariousChangeconditions()
    {
        // Arrange
        $param = [
            "dealId" => 12345,
            "cores" => [
                // This one will be updated
                [
                    "isExtracted" => true,
                    "vehicleCoreId" => 1,
                    "quantity" => 2,
                    "pricePerUnit" => 10.00,
                    "totalPrice" => 10.00
                ],
                // This one will be created
                [
                    "isExtracted" => true,
                    "vehicleCoreId" => 2,
                    "quantity" => 2,
                    "pricePerUnit" => 10.00,
                    "totalPrice" => 10.00
                ],
                // This one will be deleted
                [
                    "isExtracted" => false,
                    "vehicleCoreId" => 3,
                    "quantity" => 2,
                    "pricePerUnit" => 10.00,
                    "totalPrice" => 10.00
                ],
                // This one will be ignored
                [
                    "isExtracted" => false,
                    "vehicleCoreId" => 4,
                    "quantity" => 2,
                    "pricePerUnit" => 10.00,
                    "totalPrice" => 10.00
                ]
            ]
        ];

        $existingDealVehicleCore1 = new DealVehicleCore();
        $existingDealVehicleCore1->vehicleCoreId = 1;

        $existingDealVehicleCore3 = new DealVehicleCore();
        $existingDealVehicleCore1->vehicleCoreId = 3;

        $mockDealVehicleCoreRepository = $this->createMock(DealVehicleCoreRepository::class);
        $mockDealVehicleCoreRepository
            ->expects($this->exactly(4))
            ->method("getByDealAndVehicleCore")
            ->withConsecutive(
                [ $this->equalTo($param["dealId"]), $this->equalTo($param["cores"][0]["vehicleCoreId"]) ],
                [ $this->equalTo($param["dealId"]), $this->equalTo($param["cores"][1]["vehicleCoreId"]) ],
                [ $this->equalTo($param["dealId"]), $this->equalTo($param["cores"][2]["vehicleCoreId"]) ],
                [ $this->equalTo($param["dealId"]), $this->equalTo($param["cores"][3]["vehicleCoreId"]) ]
            )
            ->will($this->onConsecutiveCalls(
                $existingDealVehicleCore1,
                null,
                $existingDealVehicleCore3,
                null
            ));

        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("save")
            ->will($this->returnArgument(0));

        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("update")
            ->will($this->returnArgument(0));

        $mockDealVehicleCoreRepository
            ->expects($this->once())
            ->method("delete")
            ->will($this->returnArgument(0));

        $mockDealRepository = $this->createMock(DealRepository::class);

        $service = new DealVehicleCoreService($mockDealVehicleCoreRepository, $mockDealRepository);

        // Act
        $result = $service->updateDealVehicleCores($param);

        // Assert
        $this->assertTrue($result->isSuccess);
        $this->assertCount(2, $result->saved);
        $this->assertCount(1, $result->deleted);

        $this->assertEquals($result->saved[0]->vehicleCoreId, $existingDealVehicleCore1->vehicleCoreId);
        $this->assertEquals($result->saved[1]->vehicleCoreId, 2);
        $this->assertEquals($result->deleted[0]->vehicleCoreId, $existingDealVehicleCore3->vehicleCoreId);
    }

}
