<?php

namespace ApplicationTest\Service;

use \PHPUnit\Framework\TestCase;

use Application\Service\VehicleCoreService;
use Application\Repository\VehicleCoreRepository;
use Application\Model\VehicleCore;

class VehicleCoreServiceTest extends TestCase
{
    public function testUpdateFailsWhenVehicleCoreNotFound()
    {
        // Arrange
        $params = [
            "id" => "12345"
        ];

        $mockRepository = $this->createMock(VehicleCoreRepository::class);
        $mockRepository
            ->expects($this->once())
            ->method("getById")
            ->willReturn(null);

        $service = new VehicleCoreService($mockRepository);

        // Act
        $result = $service->updateVehicleCore($params);

        // Assert
        $this->assertFalse($result->isSuccess);
    }

    public function testUpdateAsignsIncomingValues()
    {
        // Arrange
        $params = [
            "id" => "12345",
            "defaultQuantity" => 111,
            "defaultPricePerUnit" => 222,
        ];

        $testVehicleCore = new VehicleCore();
        $testVehicleCore->defaultQuantity = 333;
        $testVehicleCore->defaultPricePerUnit = 333;

        $mockRepository = $this->createMock(VehicleCoreRepository::class);
        $mockRepository
            ->expects($this->once())
            ->method("getById")
            ->with($this->equalTo($params["id"]))
            ->willReturn($testVehicleCore);

        $mockRepository
            ->expects($this->once())
            ->method("update")
            ->will($this->returnArgument(0));

        $service = new VehicleCoreService($mockRepository);

        // Act
        $result = $service->updateVehicleCore($params);

        // Assert
        $this->assertTrue($result->isSuccess);
        $this->assertEquals($result->updated->defaultQuantity, $params["defaultQuantity"]);
        $this->assertEquals($result->updated->defaultPricePerUnit, $params["defaultPricePerUnit"]);
    }
}
