<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\UpdateVehicleCoreForm;

class UpdateVehicleCoreFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new UpdateVehicleCoreForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [ 'id' => 12345, 'defaultQuantity' => 10, 'defaultPricePerUnit' => 10.00 ]
            ],

            'Id not digits' => [
                false, [ 'id' => "asdadsad", 'defaultQuantity' => 10, 'defaultPricePerUnit' => 10.00 ]
            ],
            'Id null' => [
                false, [ 'id' => null, 'defaultQuantity' => 10, 'defaultPricePerUnit' => 10.00 ]
            ],
            'Id missing' => [
                false, [ 'defaultQuantity' => 10, 'defaultPricePerUnit' => 10.00 ]
            ],
            'Id empty' => [
                false, [ 'id' => "", 'defaultQuantity' => 10, 'defaultPricePerUnit' => 10.00 ]
            ],

            'defaultQuantity not digits' => [
                false, [ 'id' => 12345, 'defaultQuantity' => "asdsad", 'defaultPricePerUnit' => 10.00 ]
            ],
            'defaultQuantity negative' => [
                false, [ 'id' => 12345, 'defaultQuantity' => -10, 'defaultPricePerUnit' => 10.00 ]
            ],
            'defaultQuantity zero' => [
                true, [ 'id' => 12345, 'defaultQuantity' => 0, 'defaultPricePerUnit' => 10.00 ]
            ],
            'defaultQuantity null' => [
                false, [ 'id' => 12345, 'defaultQuantity' => null, 'defaultPricePerUnit' => 10.00 ]
            ],
            'defaultQuantity missing' => [
                false, [ 'id' => 12345, 'defaultPricePerUnit' => 10.00 ]
            ],
            'defaultQuantity empty' => [
                false, [ 'id' => 12345, 'defaultQuantity' => "", 'defaultPricePerUnit' => 10.00 ]
            ],

            'defaultPricePerUnit not number' => [
                false, [ 'id' => 12345, 'defaultQuantity' => 10, 'defaultPricePerUnit' => "asdasdsad" ]
            ],
            'defaultPricePerUnit not valid float' => [
                false, [ 'id' => 12345, 'defaultQuantity' => 10, 'defaultPricePerUnit' => "10.01.50" ]
            ],
            'defaultPricePerUnit negative' => [
                false, [ 'id' => 12345, 'defaultQuantity' => 10, 'defaultPricePerUnit' => -10.00 ]
            ],
            'defaultPricePerUnit zero' => [
                true, [ 'id' => 12345, 'defaultQuantity' => 10, 'defaultPricePerUnit' => 0 ]
            ],
            'defaultPricePerUnit null' => [
                false, [ 'id' => 12345, 'defaultQuantity' => 10, 'defaultPricePerUnit' => null ]
            ],
            'defaultPricePerUnit missing' => [
                false, [ 'id' => 12345, 'defaultQuantity' => 10 ]
            ],
            'defaultPricePerUnit empty' => [
                false, [ 'id' => 12345, 'defaultQuantity' => 10, 'defaultPricePerUnit' => "" ]
            ],
        ];
    }
}