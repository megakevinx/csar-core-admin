<?php

namespace ApplicationTest\Controller\Form;

use \PHPUnit\Framework\TestCase;

use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

use Application\Controller\Form\UpdateDealVehicleCoresForm;

class UpdateDealVehicleCoresFormTest extends BaseFormTestCase
{
    protected function getFormInstance()
    {
        return new UpdateDealVehicleCoresForm();
    }

    public function dataProvider_TestValidationIsCorrect()
    {
        return [
            'All data valid' => [
                true, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ],
                        [
                            "isExtracted" => false,
                            "vehicleCoreId" => 2,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],

            'cores empty' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [ ]
                ]
            ],

            'cores null' => [
                false, [
                    'dealId' => 12345,
                    'cores' => null
                ]
            ],

            'a core is extracted but does not have all data' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true
                        ]
                    ]
                ]
            ],

            'a core is not extracted and does not have all data' => [
                true, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => false
                        ]
                    ]
                ]
            ],

            'a core has isExtracted missing' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],

            'a core has isExtracted null' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => null,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],

            'a core has isExtracted empty' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => "",
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],

            'a core has isExtracted not a bool' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => 123,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],

            'a core has vehicleCoreId missing' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has vehicleCoreId empty' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => "",
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has vehicleCoreId null' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => null,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has vehicleCoreId not digits' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => "asdsad",
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has vehicleCoreId negative' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => -20,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has vehicleCoreId zero' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 0,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],

            'a core has quantity missing' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has quantity empty' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => "",
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has quantity null' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => null,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has quantity not digits' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => "asdsadsd",
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has quantity negative' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => -2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],

            'a core has pricePerUnit missing' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has pricePerUnit empty' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => "",
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has pricePerUnit null' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => null,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has pricePerUnit not digits' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => "asdasda",
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],
            'a core has pricePerUnit negative' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => -10.00,
                            "totalPrice" => 10.00
                        ]
                    ]
                ]
            ],

            'a core has totalPrice missing' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => -10.00,
                        ]
                    ]
                ]
            ],
            'a core has totalPrice empty' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => ""
                        ]
                    ]
                ]
            ],
            'a core has totalPrice null' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => null
                        ]
                    ]
                ]
            ],
            'a core has totalPrice not digits' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => "asasdasd"
                        ]
                    ]
                ]
            ],
            'a core has totalPrice negative' => [
                false, [
                    'dealId' => 12345,
                    'cores' => [
                        [
                            "isExtracted" => true,
                            "vehicleCoreId" => 1,
                            "quantity" => 2,
                            "pricePerUnit" => 10.00,
                            "totalPrice" => -10.00
                        ]
                    ]
                ]
            ],
        ];
    }
}