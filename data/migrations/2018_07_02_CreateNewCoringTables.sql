CREATE TABLE `VehicleCore` (
  `VehicleCore_ID` INT NOT NULL AUTO_INCREMENT,
  `Code` VARCHAR(45) NOT NULL,
  `Description` VARCHAR(45) NOT NULL,
  `DefaultQuantity` INT NOT NULL,
  `DefaultPricePerUnit` DECIMAL(5,2) NOT NULL,
  PRIMARY KEY (`VehicleCore_ID`),
  UNIQUE INDEX `Code_UNIQUE` (`Code` ASC),
  UNIQUE INDEX `Description_UNIQUE` (`Description` ASC));


CREATE TABLE `Inspection_VehicleCore` (
  `Inspection_VehicleCore_ID` INT NOT NULL AUTO_INCREMENT,
  `Inspection_ID` INT NOT NULL,
  `VehicleCore_ID` INT NOT NULL,
  `Quantity` INT NOT NULL,
  `PricePerUnit` DECIMAL(5,2) NOT NULL,
  `TotalPrice` DECIMAL(5,2) NOT NULL,
  PRIMARY KEY (`Inspection_VehicleCore_ID`),
  UNIQUE INDEX `Inspection_VehicleCore_UNIQUE` (`Inspection_ID` ASC, `VehicleCore_ID` ASC));


-- Configuration data
INSERT INTO `VehicleCore` (`Code`,`Description`,`DefaultQuantity`,`DefaultPricePerUnit`) VALUES
('ALTERNATOR', 'Alternator', 1, 5.00),
('BATTERY', 'Battery', 1, 14.00),
('STARTER', 'Starter', 1, 2.00),
('RADIATOR', 'Radiator', 1, 3.00),
('CONDENSER', 'Condenser', 1, 2.00),
('AC_COMPRESSOR', 'AC Compressor', 1, 3.00),
('WIRING_HARNESS', 'Wiring Harness', 1, 15.00),
('ECM', 'ECM', 1, 5.00),
('ALUMINUM_WHEELS', 'Aluminum Wheels', 4, 11.00),
('CHROME_WHEELS', 'Chrome Wheels', 4, 8.00),
('TIRES', 'Tires', 1, 10.00),
('CATALYTIC_CONVERTER', 'Catalytic Converter', 1, 63.00);
